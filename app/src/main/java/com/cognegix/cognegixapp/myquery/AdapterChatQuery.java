package com.cognegix.cognegixapp.myquery;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.forum.DateObject;
import com.cognegix.cognegixapp.forum.ListObject;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterChatQuery extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;

    public AdapterChatQuery(Context context) {
        this.context = context;
    }

    private List<ListObject> listObjects = new ArrayList<>();
    private int loggedInUserId;
    private ItemOnClickListener itemOnClickListener;

    public void setOnItemListners(ItemOnClickListener itemOnClickListener) {
        this.itemOnClickListener = itemOnClickListener;
    }


    public void setDataChange(List<ListObject> asList) {
        this.listObjects.addAll(asList);
        notifyDataSetChanged();
    }


    public void setCleatList() {
        this.listObjects.clear();
        notifyDataSetChanged();
    }


    public List<ListObject> getListObjects() {
        return listObjects;
    }


    public void removeChat(int pos) {
        listObjects.remove(pos);
        notifyDataSetChanged();
    }




    public void setUser(int userId) {
        this.loggedInUserId = userId;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(context);
        switch (i) {
            case ListObject.TYPE_GENERAL_SELF:
                View otherUserView = LayoutInflater.from(context).inflate(R.layout.item_view_chat_user, viewGroup, false);
                viewHolder = new HolderSelf(otherUserView); // view holder for normal items
                break;
            case ListObject.TYPE_GENERAL_OTHERS:
                View currentUserView = LayoutInflater.from(context).inflate(R.layout.item_view_chat_forum, viewGroup, false);
                viewHolder = new HolderOthers(currentUserView); // view holder for normal items
                break;
            case ListObject.TYPE_DATE:
                View v2 = inflater.inflate(R.layout.item_view_my_query_chat, viewGroup, false);
                viewHolder = new HolderSendDate(v2);
                break;
        }

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        switch (viewHolder.getItemViewType()) {
            case ListObject.TYPE_GENERAL_SELF:
                ChatModelObjectQuery generalItemLeft = (ChatModelObjectQuery) listObjects.get(i);
                HolderSelf chatLeftViewHolder = (HolderSelf) viewHolder;
                //chatLeftViewHolder.bind(generalItemLeft.getChatModel());
                chatLeftViewHolder.tvQuestion.setText(generalItemLeft.getChatModel().getMessage());
                chatLeftViewHolder.tvQuationPersonName.setText(generalItemLeft.getChatModel().getSender_name());
                chatLeftViewHolder.tvTime.setText(formatedTime(generalItemLeft.getChatModel().getCreated_at()));
                chatLeftViewHolder.itemView.setTag(i);


                break;
            case ListObject.TYPE_GENERAL_OTHERS:
                ChatModelObjectQuery generalItem = (ChatModelObjectQuery) listObjects.get(i);
                HolderOthers chatViewHolder = (HolderOthers) viewHolder;
                // chatViewHolder.bind(generalItem.getChatModel());
                chatViewHolder.tvAnwserPersonName.setText(generalItem.getChatModel().getReceiver_name());
                chatViewHolder.tvTime.setText(formatedTime(generalItem.getChatModel().getCreated_at()));
                chatViewHolder.tvAnwser.setText(generalItem.getChatModel().getMessage());
                break;
            case ListObject.TYPE_DATE:
                DateObject dateItem = (DateObject) listObjects.get(i);
                HolderSendDate dateViewHolder = (HolderSendDate) viewHolder;
                //  dateViewHolder.bind(dateItem.getDate());
                dateViewHolder.tvDateQuestion.setText(dateItem.getDate());
                break;
        }


    }

    @Override
    public int getItemCount() {
        if (listObjects != null) {
            return listObjects.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return listObjects.get(position).getType(loggedInUserId);
    }


    class HolderSendDate extends RecyclerView.ViewHolder {
        @BindView(R.id.tvDateQuestion)
        CustomTextView tvDateQuestion;


        public HolderSendDate(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }


    class HolderSelf extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTime)
        CustomTextView tvTime;

        @BindView(R.id.tvQuationPersonName)
        CustomTextView tvQuationPersonName;
        @BindView(R.id.tvQuestion)
        CustomTextView tvQuestion;
        @BindView(R.id.cardUser)
        CardView cardUser;


        public HolderSelf(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    int pos = (int) view.getTag();
                    itemOnClickListener.setOnItemClickListener(pos);
                    return false;
                }
            });

        }


    }

    class HolderOthers extends RecyclerView.ViewHolder {
        @BindView(R.id.tvAnwser)
        CustomTextView tvAnwser;
        @BindView(R.id.tvAnwserPersonName)
        CustomTextView tvAnwserPersonName;
        @BindView(R.id.tvTime)
        CustomTextView tvTime;
        @BindView(R.id.cardForum)
        CardView cardForum;

        public HolderOthers(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    @SuppressLint("SimpleDateFormat")
    private String formatedTime(String date) {
        if (!date.equals("")) {
            String forDate = null;
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                java.util.Date date1 = formatter.parse(date);
                SimpleDateFormat format = new SimpleDateFormat("hh:mm a");

                forDate = format.format(date1);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            return forDate;
        }
        // 10th March 2018

        return null;
    }


}
