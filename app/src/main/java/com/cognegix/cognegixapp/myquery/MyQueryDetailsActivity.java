package com.cognegix.cognegixapp.myquery;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomEditTextView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.facilitor.activity.MyDocumentActivity;
import com.cognegix.cognegixapp.forum.DateObject;
import com.cognegix.cognegixapp.forum.ListObject;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.model.ForgotResponse;
import com.cognegix.cognegixapp.model.ThreadMessagesResponse;
import com.cognegix.cognegixapp.model.ThreadMessagesModel;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyQueryDetailsActivity extends BaseActivity implements View.OnClickListener, ItemOnClickListener {
    private static final String TAG = MyDocumentActivity.class.getSimpleName();
    public static final String KEYThread = "keyThread";
    public static final String KEY_PROGRAM = "keyProgram";

    @BindView(R.id.rvData)
    RecyclerView rvData;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.ibSend)
    ImageButton ibSend;
    @BindView(R.id.etQuery)
    CustomEditTextView etQuery;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    ApiInterface apiInterface;
    String threadID;
    boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int page = 1;
    LinearLayoutManager linearLayoutManager;
    String programName;
    AdapterChatQuery adapterChatQuery;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_query_details);
        init();
    }

    //Todo
    private void init() {
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        ibSend.setOnClickListener(this);
        // tvToolbarTitle.setText(getString(R.string.myQuery));
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayout.VERTICAL, false);
        // linearLayoutManager.setReverseLayout(true);
        rvData.setLayoutManager(linearLayoutManager);

        adapterChatQuery = new AdapterChatQuery(this);
        adapterChatQuery.setOnItemListners(this);
        adapterChatQuery.setUser(1);
        rvData.setAdapter(adapterChatQuery);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        programName = getIntent().getStringExtra(KEY_PROGRAM);
        threadID = getIntent().getStringExtra(KEYThread);
        tvToolbarTitle.setText(programName);
        loadScroll();
        loadData(page);
    }

    //todo manage  scroll last
    private void loadScroll() {
        rvData.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                Log.d(TAG, "onScrolled:   dx == " + dx);
                Log.d(TAG, "onScrolled:   dy == " + dy);

                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            Log.v(TAG, "Last Item Wow !");
                            //Do pagination.. i.e. fetch new data
                            page = page + 1;
                            //  loadData(page);
                        }
                    }


                }

            }
        });

    }

    //todo call api for my query chat 
    private void loadData(int page) {

        adapterChatQuery.setCleatList();

        Log.d(TAG, "loadData: " + threadID);
        showDialog(this);
        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();
        Call<ThreadMessagesResponse> responseCall = apiInterface.doFetchQueryThreadChat(accept,
                authorization,
                threadID);

        responseCall.enqueue(new Callback<ThreadMessagesResponse>() {

            @Override
            public void onResponse(@NonNull Call<ThreadMessagesResponse> call, @NonNull Response<ThreadMessagesResponse> response) {
                //  Log.d(TAG, "onResponse:    +++> " + response);
                hideDialog();


                if (response.code() == 200) {

                    Log.d(TAG, "onResponse: == >  " + new Gson().toJson(response));
                    if (response.body().getStatus().equals("true")) {
                        //  adapterMyQuery.setThreadMessagesModelList(response.body().getThreadMessagesModelList());
                        groupDataIntoHashMap(response.body().getThreadMessagesModelList());


                    } else {
                        showAlertDialog(MyQueryDetailsActivity.this, "", response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ThreadMessagesResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;
            case R.id.ibSend:

                if (!etQuery.getText().toString().equals("")) {
                    doSendQuery();
                }
                break;


        }
    }

    private void doSendQuery() {
        showDialog(this);
        Log.d(TAG, "doSendQuery:threadID  " + threadID);

        Log.d(TAG, "doSendQuery:threadIDtext  " + etQuery.getText().toString());

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();
        Call<ForgotResponse> responseCall = apiInterface.doASkchQueryThreadChat(accept,
                authorization,
                threadID,
                etQuery.getText().toString(),
                SharePref.getProgramUserId(this));

        Log.d(TAG, " :   " + new Gson().toJson(responseCall.request().body()));

        responseCall.enqueue(new Callback<ForgotResponse>() {

            @Override
            public void onResponse(@NonNull Call<ForgotResponse> call, @NonNull Response<ForgotResponse> response) {
                //    Log.d(TAG, "onResponse:    +++> " + response.body().getStatus());
                hideDialog();


                if (response.code() == 200) {
                    if (response.body().getStatus().equals("true")) {
                        //showDataAfterMsg();
                        etQuery.setText("");
                        loadData(page);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ForgotResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });

    }

    @SuppressLint("SimpleDateFormat")
    private void showDataAfterMsg() {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c);
        ThreadMessagesModel threadMessagesModel = new ThreadMessagesModel();
        threadMessagesModel.setMessage(etQuery.getText().toString().trim());
        // forumModel.setUser_id(SharePref.getLoginResponse(getApplicationContext()).getUserDetail().getId());
        //forumModel.setPost_by("You");
        threadMessagesModel.setCreated_at(formattedDate);
        threadMessagesModel.setSender_name("You");
        //  modelChatForumList.add(threadMessagesModel);
        // adapterMyQuery.notifyDataSetChanged();
        etQuery.setText("");
    }

    @Override
    public void setOnItemClickListener(final int position) {
        ChatModelObjectQuery chatModelObjectForum = (ChatModelObjectQuery) adapterChatQuery.getListObjects().get(position);

        if (chatModelObjectForum.getChatModel().getIs_from_sender().equals("1")) {
            View view = getLayoutInflater().inflate(R.layout.dailog_delete, null);
            final BottomSheetDialog dialog = new BottomSheetDialog(this);
            dialog.setContentView(view);
            dialog.show();
            CustomButtonView bYes = dialog.findViewById(R.id.bYes);
            CustomButtonView bNo = dialog.findViewById(R.id.bNo);


            assert bYes != null;
            bYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    doDeletePostMsg(position);
//        //       Log.d(TAG, "onClick: " + forumModelList.get(position).getId());
                    //      Log.d(TAG, "loadData: program_id == " + program_id);
                    //     Log.d(TAG, "loadData: getForum_id == " + forumInterModel.getForum_id());
                    //     Log.d(TAG, "loadData: getId == " + forumInterModel.getId());
                    //     Log.d(TAG, "loadData: msg id == " + forumModelList.get(position).getId());


                }
            });


            assert bNo != null;
            bNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }

    }

    private void doDeletePostMsg(final int pos) {
        showDialog(this);
        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();

        ChatModelObjectQuery chatModelObjectForum = (ChatModelObjectQuery) adapterChatQuery.getListObjects().get(pos);
        String chatId = chatModelObjectForum.getChatModel().getId();
        Call<ForgotResponse> responseCall = apiInterface.doDeleteQueryThread(accept, authorization,
                threadID,
                chatId);

        Log.d(TAG, "onResponse:=>> " + new Gson().toJson(responseCall.request().body()));


        responseCall.enqueue(new Callback<ForgotResponse>() {

            @Override
            public void onResponse(@NonNull Call<ForgotResponse> call, @NonNull Response<ForgotResponse> response) {
                //   Log.d(TAG, "onResponse:    +++> " + response);
                hideDialog();

                //  Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));

                if (response.code() == 200) {
                    Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));

                    if (response.body().getStatus().equalsIgnoreCase("true")) {
                        Log.d(TAG, "onResponse: " + response.body().getStatus());
                        showAlertDialog(MyQueryDetailsActivity.this, "", response.body().getMessage());

                        adapterChatQuery.removeChat(pos);

                    } else {

                        showAlertDialog(MyQueryDetailsActivity.this, "", response.body().getMessage());
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<ForgotResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });


    }

    private void groupDataIntoHashMap(List<ThreadMessagesModel> threadMessagesModels) {

        LinkedHashMap<String, Set<ThreadMessagesModel>> groupedHashMap = new LinkedHashMap<>();
        Set<ThreadMessagesModel> list = null;
        for (ThreadMessagesModel chatModel : threadMessagesModels) {
            //Log.d(TAG, travelActivityDTO.toString());
            String hashMapKey = formatedDate(chatModel.getCreated_at());
            //Log.d(TAG, "start date: " + DateParser.convertDateToString(travelActivityDTO.getStartDate()));
            if (groupedHashMap.containsKey(hashMapKey)) {
                // The key is already in the HashMap; add the pojo object
                // against the existing key.
                groupedHashMap.get(hashMapKey).add(chatModel);
            } else {
                // The key is not there in the HashMap; create a new key-value pair
                list = new LinkedHashSet<>();
                list.add(chatModel);
                groupedHashMap.put(hashMapKey, list);
            }
        }

        //Generate list from map
        generateListFromMap(groupedHashMap);

    }

    private List<ListObject> generateListFromMap(LinkedHashMap<String, Set<ThreadMessagesModel>> groupedHashMap) {

        List<ListObject> consolidatedList = new ArrayList<>();
        for (String date : groupedHashMap.keySet()) {
            DateObject dateItem = new DateObject();
            dateItem.setDate(date);
            consolidatedList.add(dateItem);
            for (ThreadMessagesModel chatModel : groupedHashMap.get(date)) {
                ChatModelObjectQuery generalItem = new ChatModelObjectQuery();
                generalItem.setChatModel(chatModel);
                consolidatedList.add(generalItem);
            }
        }

        adapterChatQuery.setDataChange(consolidatedList);
        rvData.smoothScrollToPosition(adapterChatQuery.getListObjects().size() - 1);

        return consolidatedList;
    }


    @SuppressLint("SimpleDateFormat")
    private String formatedDate(String date) {
        if (!date.equals("")) {
            String forDate = null;
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                java.util.Date date1 = formatter.parse(date);
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");

                forDate = format.format(date1);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            return forDate;
        }
        // 10th March 2018

        return null;
    }


}
