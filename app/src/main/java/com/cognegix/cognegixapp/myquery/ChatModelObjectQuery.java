package com.cognegix.cognegixapp.myquery;


import com.cognegix.cognegixapp.forum.ListObject;
import com.cognegix.cognegixapp.model.ThreadMessagesModel;

public class ChatModelObjectQuery extends ListObject {
    private ThreadMessagesModel chatModel;


    public ThreadMessagesModel getChatModel() {
        return chatModel;
    }

    public void setChatModel(ThreadMessagesModel chatModel) {
        this.chatModel = chatModel;
    }

    @Override
    public int getType(int userId) {

        int id = Integer.parseInt(this.chatModel.getIs_from_sender());

        if (id == userId) {
            return TYPE_GENERAL_SELF;
        } else
            return TYPE_GENERAL_OTHERS;
    }


}
