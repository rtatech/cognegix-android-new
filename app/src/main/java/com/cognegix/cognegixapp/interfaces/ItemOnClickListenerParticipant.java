package com.cognegix.cognegixapp.interfaces;


import com.cognegix.cognegixapp.model.ModelParticipant;

public interface ItemOnClickListenerParticipant {
    void setOnItemClickListener(ModelParticipant modelParticipant);

    void setOnItemClick(int pos);


}

