package com.cognegix.cognegixapp.interfaces;

import com.cognegix.cognegixapp.model.ModelNotification;
import com.cognegix.cognegixapp.model.document.DocumentModel;

public interface OnClickNotification {
    void onClickView(ModelNotification modelNotification);
}
