package com.cognegix.cognegixapp.interfaces;

import com.cognegix.cognegixapp.model.document.DocumentModel;
import com.cognegix.cognegixapp.model.program.UpComingModel;

public interface OnClickDocument {
    void onClickView(DocumentModel documentModel);

    void onClickDownload(DocumentModel documentModel);

}
