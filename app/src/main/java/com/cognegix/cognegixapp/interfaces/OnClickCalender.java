package com.cognegix.cognegixapp.interfaces;

import com.cognegix.cognegixapp.model.program.UpComingModel;

public interface OnClickCalender {
    void onClickItemCalender(UpComingModel upComingModel);
}
