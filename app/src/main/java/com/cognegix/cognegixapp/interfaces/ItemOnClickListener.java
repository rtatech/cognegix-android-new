package com.cognegix.cognegixapp.interfaces;


public interface ItemOnClickListener {
    void setOnItemClickListener(int position);
}
