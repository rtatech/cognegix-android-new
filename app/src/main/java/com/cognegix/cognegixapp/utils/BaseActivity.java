package com.cognegix.cognegixapp.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.model.program.ProgramModel;
import com.cognegix.cognegixapp.utils.network_connectivity.ConnectivityReceiver;
import com.cognegix.cognegixapp.utils.network_connectivity.MyApplication;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;
import java.util.Objects;

public class BaseActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    private static final String TAG = BaseActivity.class.getSimpleName();
    ConnectivityReceiver connectivityReceiver = new ConnectivityReceiver();

    //dialog
    private static Dialog dialog;
    private static AVLoadingIndicatorView avi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkConnection();
    }

    //Todo Show progress dialog

    public void showDialog(Activity activity) {
        if (dialog == null) {
            dialog = new Dialog(BaseActivity.this);
        }
        if (!dialog.isShowing()) {
            // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dailog_view_indicator);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            avi = dialog.findViewById(R.id.avi);
            avi.show();
            dialog.setCancelable(false);
            dialog.show();
        }

    }

    //Todo hide dialog
    public static void hideDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;

            if (avi != null) {
                avi.hide();
            }
        }
    }


    public void showAlertDialog(Activity activity, String title, String msg) {
        if (dialog == null) {
            dialog = new Dialog(BaseActivity.this);
        }
        if (!dialog.isShowing()) {
            // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dailog_view_alert);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            CustomTextView tvTitle = dialog.findViewById(R.id.tvTitle);
            CustomTextView tvMessage = dialog.findViewById(R.id.tvMessage);
            CustomButtonView bOk = dialog.findViewById(R.id.bOk);
            tvTitle.setText(title);
            tvMessage.setText(msg);

            bOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //  dialog.dismiss();
                    hideDialog();
                    doneDialog();
                }
            });
            dialog.setCancelable(false);
            dialog.show();
        }

    }


    public void doneDialog() {

    }


    public void selectDropDownValue(List<ProgramModel> modelProgramList, String value) {


        for (int i = 0; i < modelProgramList.size(); i++) {
            //   Log.d(TAG, "selectValue: pid  " + modelProgramList.get(i).getId());
            if (modelProgramList.get(i).getId() != null && modelProgramList.get(i).getId().equals(value)) {
                selectedDropDownValue(i);
                break;
            }
        }
    }

    public void selectedDropDownValue(int position) {

    }


    // check internet  connection
    public boolean checkConnectionIsConnected() {
        return ConnectivityReceiver.isConnected();
    }


    @Override
    protected void onStart() {
        super.onStart();
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(connectivityReceiver, intentFilter);
        MyApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //  MyApplication.getInstance().setConnectivityListener(this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(connectivityReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "onResume: ");
        // register connection status listener
        //  this.startService(new Intent(this, ConnectivityReceiver.class));
        MyApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Log.d(TAG, "onNetworkConnectionChanged: ");
        showSnack(isConnected);
    }


    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;
            Log.d(TAG, "showSnack: " + message);
        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
            this.showAlertDialogInternet("Check Connection", "No Internet");
            Log.d(TAG, "showSnack: " + message);

        }


    }

    public void showAlertDialogInternet(String title, String msg) {
        if (dialog == null) {
            dialog = new Dialog(BaseActivity.this);
        }
        if (!dialog.isShowing()) {
            // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dailog_view_alert_internet);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            CustomTextView tvTitle = dialog.findViewById(R.id.tvTitle);
            CustomTextView tvMessage = dialog.findViewById(R.id.tvMessage);
            CustomButtonView bOk = dialog.findViewById(R.id.bOk);
            tvTitle.setText(title);
            tvMessage.setText(msg);

            bOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //  dialog.dismiss();
                    if (checkConnectionIsConnected()) {
                        hideDialog();

                    }
                }
            });
            dialog.setCancelable(false);
            dialog.show();
        }

    }


}
