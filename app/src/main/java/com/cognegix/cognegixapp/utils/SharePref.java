package com.cognegix.cognegixapp.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.RemoteAction;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.model.UserProfileResponse;
import com.cognegix.cognegixapp.model.loginresponse.LoginReponces;
import com.cognegix.cognegixapp.model.program.AllProgramResponse;
import com.cognegix.cognegixapp.model.program.ProgramModel;
import com.cognegix.cognegixapp.model.program.ProgramResponse;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

/**
 * Created by ALPHA-08 on 4/21/2017.
 */

public class SharePref {


    private static final String FILE_NAME = "cognegix";
    private static final String API_TOKEN = "deviceToken";
    private static final String RESPONSE_LOGIN = "loginResponse";
    private static final String RESPONSE_PROFILE = "profileResponse";
    private static final String PROGRAM_MODEL = "programId";
    private static final String NOTIFICATION_COUNT = "notification";
    private static final String PROGRAM_RESPONSE = "responseProgram";
    private static final String PROFILE_TYPE = "profileType";

    private static final String REMEAINING_BADGES = "badgesRemaining";

    private static final String PROGRAM_USER_ID = "programUserID";


    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }


    public static String getApiToken(Context context) {
        return getPreferences(context).getString(API_TOKEN, null);
    }


    public static boolean setApiToken(Context context, String apiToken) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(API_TOKEN, apiToken);
        return editor.commit();
    }


    public static String getNotificationCount(Context context) {
        return getPreferences(context).getString(NOTIFICATION_COUNT, null);
    }


    public static boolean setNotificationCount(Context context, String notificationCount) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(NOTIFICATION_COUNT, notificationCount);
        return editor.commit();
    }


    /*public static boolean setLoginResponse(Context context, String responseJson) {
        return setString(context, RESPONSE_LOGIN, responseJson);
    }*/

    public static LoginReponces getLoginResponse(Context context) {
        String responseJson = getPreferences(context).getString(RESPONSE_LOGIN, null);
        return responseJson == null ? null : new Gson().fromJson(responseJson, LoginReponces.class);
    }

    public static boolean setLoginResponse(Context context, LoginReponces loginReponces) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        String json = new Gson().toJson(loginReponces);
        editor.putString(RESPONSE_LOGIN, json);
        return editor.commit();
    }


    public static UserProfileResponse getProfileResponse(Context context) {
        String responseJson = getPreferences(context).getString(RESPONSE_PROFILE, null);
        return responseJson == null ? null : new Gson().fromJson(responseJson, UserProfileResponse.class);
    }

    public static boolean setProfileResponse(Context context, UserProfileResponse userProfileResponse) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        String json = new Gson().toJson(userProfileResponse);
        editor.putString(RESPONSE_PROFILE, json);
        return editor.commit();
    }


    public static AllProgramResponse getProgramResponse(Context context) {
        String responseJson = getPreferences(context).getString(PROGRAM_RESPONSE, null);
        return responseJson == null ? null : new Gson().fromJson(responseJson, AllProgramResponse.class);
    }

    public static boolean setProgramResponse(Context context, AllProgramResponse programResponse) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        String json = new Gson().toJson(programResponse);
        editor.putString(PROGRAM_RESPONSE, json);
        return editor.commit();
    }


    public static ProgramModel getProgramModel(Context context) {
        String responseJson = getPreferences(context).getString(PROGRAM_MODEL, null);
        return responseJson == null ? null : new Gson().fromJson(responseJson, ProgramModel.class);
    }

    public static boolean setProgramModel(Context context, ProgramModel programModel) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        String json = new Gson().toJson(programModel);
        editor.putString(PROGRAM_MODEL, json);
        return editor.commit();
    }


    public static String getProfileType(Context context) {
        return getPreferences(context).getString(PROFILE_TYPE, null);
    }


    public static boolean setProfileType(Context context, String profileType) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(PROFILE_TYPE, profileType);
        return editor.commit();
    }

    public static String getRemeainingBadges(Context context) {
        return getPreferences(context).getString(REMEAINING_BADGES, null);
    }


    public static boolean setRemeainingBadges(Context context, String remeainingBadges) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(REMEAINING_BADGES, remeainingBadges);
        return editor.commit();
    }

    public static String getProgramUserId(Context context) {
        return getPreferences(context).getString(PROGRAM_USER_ID, null);
    }


    public static boolean setProgramUserId(Context context, String programUserId) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(PROGRAM_USER_ID, programUserId);
        return editor.commit();
    }

}
