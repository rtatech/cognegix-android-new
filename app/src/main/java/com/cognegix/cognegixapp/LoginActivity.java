package com.cognegix.cognegixapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomEditTextView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.facilitor.activity.ActivityForgotPassword;
import com.cognegix.cognegixapp.facilitor.activity.HelpDeskMessageActivity;
import com.cognegix.cognegixapp.facilitor.activity.HomeActivity;
import com.cognegix.cognegixapp.facilitor.activity.TermsAndConditionActivity;
import com.cognegix.cognegixapp.model.ContactResponseModel;
import com.cognegix.cognegixapp.model.loginresponse.LoginReponces;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final String CALL_PERMISION = Manifest.permission.CALL_PHONE;
    private static final String RECORD_AUDIO_PERMISSION = Manifest.permission.RECORD_AUDIO;
    private static final String MODIFY_AUDIO_SETTINGS_PERMISSION = Manifest.permission.MODIFY_AUDIO_SETTINGS;
    private static final String ACCESS_STORAGE = android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private static final String CAMERA = android.Manifest.permission.CAMERA;

    private static final int PERMISSION = 3;


    @BindView(R.id.bLogin)
    CustomButtonView bLogin;

    @BindView(R.id.etUserName)
    CustomEditTextView etUserName;
    @BindView(R.id.etPassword)
    CustomEditTextView etPassword;

    @BindView(R.id.tvForgot)
    CustomTextView tvForgot;

    @BindView(R.id.rlTerm)
    RelativeLayout rlTerm;


    @BindView(R.id.rlEmail)
    RelativeLayout rlEmail;
    @BindView(R.id.rlMsg)
    RelativeLayout rlMsg;
    @BindView(R.id.rlCall)
    RelativeLayout rlCall;


    //Todo create variable
    ApiInterface apiInterface;
    String userName;
    String password;

    String number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        inti();
        //  showDialog();
    }

    private void inti() {
        Log.d(TAG, "inti: ");
        ButterKnife.bind(this);
        bLogin.setOnClickListener(this);
        tvForgot.setOnClickListener(this);
        rlTerm.setOnClickListener(this);
        rlEmail.setOnClickListener(this);
        rlMsg.setOnClickListener(this);
        rlCall.setOnClickListener(this);


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        // etUserName.setText("tagsab2000@gmail.com");
        //  etPassword.setText("pass@1234");
        //etUserName.setText("dhananjay.katre@gmail.com");
        // etUserName.setText("vivek.chudgar@vnnogile.com");
        //  etUserName.setText("millind.moravekar@vnnogile.com");
        etUserName.setText("bahar.kanade@vnnogile.com");
        //    etUserName.setText("sabera@cognegix.in");
        etPassword.setText("pass@123");

        doGetPhoneNumber();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!checkPermission()) {
                requestPermission();
            }
        }
    }

    @SuppressLint("ResourceType")
    @Override
    public void onClick(View view) {


        switch (view.getId()) {

            case R.id.bLogin:
/*
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(
                        etPassword.getWindowToken(), 0);
*/

                if (isValidate()) {

                    doLogin();

                }


               /* startActivity(new Intent(this, HomeActivity.class));
                finish();*/
                break;

            case R.id.tvForgot:
                startActivity(new Intent(this, ActivityForgotPassword.class));
                //finish();
                break;

            case R.id.rlTerm:
                startActivity(new Intent(this, TermsAndConditionActivity.class));
                //finish();
                break;

            case R.id.rlEmail:
                Intent intent = new Intent(this, HelpDeskMessageActivity.class);
                intent.putExtra("help_type", "mail");
                startActivity(intent);
                break;

            case R.id.rlMsg:
                Intent intent1 = new Intent(this, HelpDeskMessageActivity.class);
                intent1.putExtra("help_type", "sms");
                startActivity(intent1);
                break;

            case R.id.rlCall:

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkPermission()) {
                        if (!number.equals("")) {
                            Intent call = new Intent(Intent.ACTION_CALL);
                            call.setData(Uri.parse("tel:" + number));
                            startActivity(call);
                        }
                    } else {
                        requestPermission();
                    }
                } else {
                    if (!number.equals("")) {
                        Intent call = new Intent(Intent.ACTION_CALL);
                        call.setData(Uri.parse("tel:" + number));
                        startActivity(call);
                    }
                }

                break;

        }


    }


    //Todo call api for login
    private void doLogin() {

        Log.d(TAG, "doLogin: ");
        showDialog(this);
        Call<LoginReponces> responseCall = apiInterface.doLogin(userName, password, "A");
        responseCall.enqueue(new Callback<LoginReponces>() {
            @Override
            public void onResponse(@NonNull Call<LoginReponces> call, @NonNull Response<LoginReponces> response) {
                //  Log.d(TAG, "onResponse:     " + response.body().getStatus() + "===kffkdkb=====" + response.body().getMessage());
                hideDialog();
                // SharePref.hideDialog();
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (response.body().getStatus().equals("true")) {
                        //  Log.d(TAG, "onResponse: " + response.body().getUserDetail().getApi_token());
                        SharePref.setLoginResponse(getApplicationContext(), response.body());

                        SharePref.setProfileType(getApplicationContext(), response.body().getUserProfileDetail().getIs_private());
                        Log.d(TAG, "onResponse: shasre kfbkb kbfkb  " + SharePref.getLoginResponse(getApplicationContext()).getStatus());
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                        //  startActivity(new Intent(getApplicationContext(), TestActivity.class));
                        finish();
                    } else {
                        showAlertDialog(LoginActivity.this, "Login", response.body().getMessage());
                        // showAlertDialog(LoginActivity.this,"d;fmf","ncn");
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginReponces> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
                //  SharePref.hideDialog();
            }
        });


    }


    void initVariable() {
        userName = etUserName.getText().toString().trim();
        password = etPassword.getText().toString().trim();
    }

    boolean isValidate() {
        boolean isValidate = true;
        initVariable();
        if (userName.equals("")) {
            etUserName.setError("Enter Username");
            isValidate = false;
        }

        if (password.equals("")) {
            etPassword.setError("Enter Password");
            isValidate = false;
        }
        return isValidate;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{CALL_PERMISION,
                MODIFY_AUDIO_SETTINGS_PERMISSION,
                RECORD_AUDIO_PERMISSION, ACCESS_STORAGE, CAMERA}, PERMISSION);
    }

    private boolean checkPermission() {
        int call = ContextCompat.checkSelfPermission(this, CALL_PERMISION);
        int audio = ContextCompat.checkSelfPermission(this, RECORD_AUDIO_PERMISSION);
        int mic_setting = ContextCompat.checkSelfPermission(this, MODIFY_AUDIO_SETTINGS_PERMISSION);
        int accessStorage = ContextCompat.checkSelfPermission(this, ACCESS_STORAGE);
        int camera = ContextCompat.checkSelfPermission(this, CAMERA);

        return call == PackageManager.PERMISSION_GRANTED &&
                audio == PackageManager.PERMISSION_GRANTED &&
                mic_setting == PackageManager.PERMISSION_GRANTED &&
                accessStorage == PackageManager.PERMISSION_GRANTED &&
                camera == PackageManager.PERMISSION_GRANTED;
    }


    private void doGetPhoneNumber() {

        Log.d(TAG, "doLogin: ");
        showDialog(this);
        Call<ContactResponseModel> responseCall = apiInterface.doGetContactResponseModel();
        responseCall.enqueue(new Callback<ContactResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<ContactResponseModel> call, @NonNull Response<ContactResponseModel> response) {
                // Log.d(TAG, "onResponse:     " + response.body().getStatus() + "===kffkdkb=====" + response.body().getMessage());
                hideDialog();
                // SharePref.hideDialog();
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (response.body().getStatus().equals("true")) {
                        number = response.body().getHelp_desk_phone();
                        Log.d(TAG, "onResponse:  " + number);
                    } else {
                        showAlertDialog(LoginActivity.this, "Login", response.body().getMessage());
                        // showAlertDialog(LoginActivity.this,"d;fmf","ncn");
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ContactResponseModel> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
                //  SharePref.hideDialog();
            }
        });


    }

}
