package com.cognegix.cognegixapp.forum;

public abstract class ListObject {

    public static final int TYPE_DATE = 0;
    public static final int TYPE_GENERAL_SELF = 1;
    public static final int TYPE_GENERAL_OTHERS = 2;

    abstract public int getType(int userId);

}
