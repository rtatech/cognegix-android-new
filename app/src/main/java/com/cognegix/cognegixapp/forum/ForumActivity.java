package com.cognegix.cognegixapp.forum;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomEditTextView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.facilitor.activity.ForumInterMediatorActivity;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.model.ForgotResponse;
import com.cognegix.cognegixapp.model.ForumAddResponse;
import com.cognegix.cognegixapp.model.ForumInterModel;
import com.cognegix.cognegixapp.model.ForumModel;
import com.cognegix.cognegixapp.model.ForumResponse;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForumActivity extends BaseActivity implements View.OnClickListener, ItemOnClickListener {

    private static final String TAG = ForumActivity.class.getSimpleName();
    public static final String Key = "keyForBundle";

    @BindView(R.id.rvData)
    RecyclerView rvData;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.ibSend)
    ImageButton ibSend;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    @BindView(R.id.etQuery)
    CustomEditTextView etQuery;

    //Todo create varible
    ApiInterface apiInterface;
    String program_id;
    ForumInterModel forumInterModel;

    LinearLayoutManager linearLayoutManager;
    boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int page = 1;

    AdapterChatForum adapterForumTest;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_query_details);
        init();
    }

    //Todo
    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        ibSend.setOnClickListener(this);
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayout.VERTICAL, false);
        rvData.setLayoutManager(linearLayoutManager);
        doSetScrollistner();


        int uId = Integer.parseInt(SharePref.getLoginResponse(this).getUserDetail().getId());

        adapterForumTest = new AdapterChatForum(this);
        adapterForumTest.setOnItemListners(this);
        adapterForumTest.setUser(uId);
        rvData.setAdapter(adapterForumTest);


        forumInterModel = (ForumInterModel) getIntent().getSerializableExtra(Key);
        tvToolbarTitle.setText(forumInterModel.getSubject());

        Log.d(TAG, "init: forum id === > " + forumInterModel.getForum_id() + "Th===>" + forumInterModel.getId());

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        program_id = SharePref.getProgramModel(this).getId();

        loadData(page);
    }


    private void doSetScrollistner() {
        rvData.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                Log.d(TAG, "onScrolled:   dx == " + dx);
                Log.d(TAG, "onScrolled:   dy == " + dy);

                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            Log.v(TAG, "Last Item Wow !");
                            //Do pagination.. i.e. fetch new data
                            page = page + 1;
                            loadData(page);
                        }
                    }
                }

            }
        });


    }


    private void loadData(int page) {
        loading = true;

        showDialog(this);
        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();
        //   Log.d(TAG, "loadData: program_id == " + program_id);
        Call<ForumResponse> responseCall = apiInterface.doFetchFoeumList(accept, authorization,
                program_id,
                String.valueOf(page),
                forumInterModel.getForum_id(),
                forumInterModel.getId()
        );

        responseCall.enqueue(new Callback<ForumResponse>() {

            @Override
            public void onResponse(@NonNull Call<ForumResponse> call, @NonNull Response<ForumResponse> response) {
                //   Log.d(TAG, "onResponse:    +++> " + response);
                hideDialog();


                if (response.code() == 200) {

                    if (response.body().getStatus().equalsIgnoreCase("true")) {
                        //  Log.d(TAG, "onResponse: " + response.body().getStatus());
                        //  Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));

                       /* forumModelList.addAll(response.body().getForum_messages());
                        adapterForumChat.notifyDataSetChanged();*/
                        groupDataIntoHashMap(response.body().getForum_messages());

                    } else {

                        showAlertDialog(ForumActivity.this, "", response.body().getMessage());
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<ForumResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;

            case R.id.ibSend:

                if (!etQuery.getText().toString().equals("")) {
                    //do call api
                    doAddPostForum();
                    //l showDataAfterMsg();
                } else {
                    etQuery.setError("Enter you message");

                }
                break;


        }
    }

    //Todo call api for add forum
    private void doAddPostForum() {
       // Log.d(TAG, "loadData: program_id == " + program_id);

        showDialog(this);
        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();

      //  Log.d(TAG, "doAddPostForum:=>     " + authorization);
        Call<ForumAddResponse> responseCall = apiInterface.doAddForumPost(accept,
                authorization,
                program_id,
                etQuery.getText().toString().trim(),
                forumInterModel.getForum_id(),
                forumInterModel.getId());

        Log.d(TAG, "doAddPostForum:    " + new Gson().toJson(responseCall.request().body()));

        responseCall.enqueue(new Callback<ForumAddResponse>() {

            @Override
            public void onResponse(@NonNull Call<ForumAddResponse> call, @NonNull Response<ForumAddResponse> response) {
                // Log.d(TAG, "onResponse:     " + response.body().getStatus() + "===kffkdkb=====" + response.body().getMessage());


                hideDialog();

                if (response.code() == 200) {
                    // groupDataIntoHashMap(response.body().getThread_post());

                    adapterForumTest.addNewChatItem(response.body().getThread_post().get(0));
                    etQuery.setText("");

                  rvData.smoothScrollToPosition(adapterForumTest.getListObjects().size() - 1);

                }
            }

            @Override
            public void onFailure(@NonNull Call<ForumAddResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, ForumInterMediatorActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void setOnItemClickListener(final int position) {
        Log.d(TAG, "setOnItemClickListener:  " + position);

        ChatModelObjectForum chatModelObjectForum = (ChatModelObjectForum) adapterForumTest.getListObjects().get(position);
        if (chatModelObjectForum.getChatModel().getUser_id().equals(SharePref.getLoginResponse(this).getUserDetail().getId())) {
            View view = getLayoutInflater().inflate(R.layout.dailog_delete, null);
            final BottomSheetDialog dialog = new BottomSheetDialog(this);
            dialog.setContentView(view);
            dialog.show();
            CustomButtonView bYes = dialog.findViewById(R.id.bYes);
            CustomButtonView bNo = dialog.findViewById(R.id.bNo);


            assert bYes != null;
            bYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    doDeletePostMsg(position);
//                Log.d(TAG, "onClick: " + forumModelList.get(position).getId());
                    //      Log.d(TAG, "loadData: program_id == " + program_id);
                    //    Log.d(TAG, "loadData: getForum_id == " + forumInterModel.getForum_id());
                    //     Log.d(TAG, "loadData: getId == " + forumInterModel.getId());


                }
            });


            assert bNo != null;
            bNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }


    }


    private void doDeletePostMsg(final int pos) {
        showDialog(this);
        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();

        //forumModelList.get(pos).getId()
        // Log.d(TAG, "loadData: program_id == " + program_id);
        // Log.d(TAG, "loadData: getForum_id == " + forumInterModel.getForum_id());
        // Log.d(TAG, "loadData: getId == " + forumInterModel.getId());
        //Log.d(TAG, "loadData: program_id == " + forumInterModel.());

        ChatModelObjectForum chatModelObjectForum = (ChatModelObjectForum) adapterForumTest.getListObjects().get(pos);

        Call<ForgotResponse> responseCall = apiInterface.doDeletePost(accept, authorization,
                program_id,
                chatModelObjectForum.getChatModel().getId(),
                forumInterModel.getForum_id(),
                forumInterModel.getId()
        );

        responseCall.enqueue(new Callback<ForgotResponse>() {

            @Override
            public void onResponse(@NonNull Call<ForgotResponse> call, @NonNull Response<ForgotResponse> response) {
                //   Log.d(TAG, "onResponse:    +++> " + response);
                hideDialog();


                if (response.code() == 200) {

                    if (response.body().getStatus().equalsIgnoreCase("true")) {
                        Log.d(TAG, "onResponse: " + response.body().getStatus());
                        Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));
                        showAlertDialog(ForumActivity.this, "", response.body().getMessage());
                       /* forumModelList.remove(pos);
                        adapterForumChat.notifyDataSetChanged();*/
                        adapterForumTest.removeChat(pos);

                    } else {

                        showAlertDialog(ForumActivity.this, "", response.body().getMessage());
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<ForgotResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });


    }


    private void groupDataIntoHashMap(List<ForumModel> forumModelList) {

        LinkedHashMap<String, Set<ForumModel>> groupedHashMap = new LinkedHashMap<>();
        Set<ForumModel> list = null;
        for (ForumModel chatModel : forumModelList) {
            //Log.d(TAG, travelActivityDTO.toString());
            String hashMapKey = formatedDate(chatModel.getUpdated_at());
            //Log.d(TAG, "start date: " + DateParser.convertDateToString(travelActivityDTO.getStartDate()));
            if (groupedHashMap.containsKey(hashMapKey)) {
                // The key is already in the HashMap; add the pojo object
                // against the existing key.
                groupedHashMap.get(hashMapKey).add(chatModel);
            } else {
                // The key is not there in the HashMap; create a new key-value pair
                list = new LinkedHashSet<>();
                list.add(chatModel);
                groupedHashMap.put(hashMapKey, list);
            }
        }

        //Generate list from map
        generateListFromMap(groupedHashMap);

    }

    private List<ListObject> generateListFromMap(LinkedHashMap<String, Set<ForumModel>> groupedHashMap) {

        List<ListObject> consolidatedList = new ArrayList<>();
        for (String date : groupedHashMap.keySet()) {
            DateObject dateItem = new DateObject();
            dateItem.setDate(date);
            consolidatedList.add(dateItem);
            for (ForumModel chatModel : groupedHashMap.get(date)) {
                ChatModelObjectForum generalItem = new ChatModelObjectForum();
                generalItem.setChatModel(chatModel);
                consolidatedList.add(generalItem);
            }
        }

        adapterForumTest.setDataChange(consolidatedList);

        return consolidatedList;
    }


    @SuppressLint("SimpleDateFormat")
    private String formatedDate(String date) {
        if (!date.equals("")) {
            String forDate = null;
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                java.util.Date date1 = formatter.parse(date);
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");

                forDate = format.format(date1);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            return forDate;
        }
        // 10th March 2018

        return null;
    }


}
