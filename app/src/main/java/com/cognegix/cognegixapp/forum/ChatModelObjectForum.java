package com.cognegix.cognegixapp.forum;


import com.cognegix.cognegixapp.model.ForumModel;

public class ChatModelObjectForum extends ListObject {

    private ForumModel chatModel;


    public ForumModel getChatModel() {
        return chatModel;
    }

    public void setChatModel(ForumModel chatModel) {
        this.chatModel = chatModel;
    }

    @Override
    public int getType(int userId) {

        int id = Integer.parseInt(this.chatModel.getUser_id());

        if ( id == userId) {
            return TYPE_GENERAL_SELF;
        } else
            return TYPE_GENERAL_OTHERS;
    }


}
