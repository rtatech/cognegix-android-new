package com.cognegix.cognegixapp.apihelper;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {

    // private static final String BASE_URL = "http://cogvc.testursites.info/cgx-front/public/index.php/api/";
    private static final String BASE_URL = " http://182.59.4.193:8010/api/";
    private static Retrofit retrofit = null;
    //public static final String LEARNING_TREE_LINK = "http://182.59.4.193:8010/api/program/learning/tree?tree_id";
    public static final String LEARNING_TREE_LINK_ADD = "http://182.59.4.193:8010/svg?tree_id";


    public static final String LEARNING_TREE_LINK = "http://182.59.4.193:8010/artefact/open?";



    public static Retrofit getClient() {
        if (retrofit == null) {
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(15, TimeUnit.SECONDS)
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;
    }

}
