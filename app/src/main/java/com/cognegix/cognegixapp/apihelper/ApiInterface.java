package com.cognegix.cognegixapp.apihelper;


import com.cognegix.cognegixapp.model.AssingBadgesResponse;
import com.cognegix.cognegixapp.model.ContactResponseModel;
import com.cognegix.cognegixapp.model.FAQResponse;
import com.cognegix.cognegixapp.model.ForumAddResponse;
import com.cognegix.cognegixapp.model.ProgarmTopicResponse;
import com.cognegix.cognegixapp.model.ProgramParticipantResponse;
import com.cognegix.cognegixapp.model.SearchResponse;
import com.cognegix.cognegixapp.model.VideoCallResponse;
import com.cognegix.cognegixapp.model.badges.BadgesLederResponse;
import com.cognegix.cognegixapp.model.badges.BadgesResponse;
import com.cognegix.cognegixapp.model.ForgotResponse;
import com.cognegix.cognegixapp.model.ForumInterResponse;
import com.cognegix.cognegixapp.model.ForumResponse;
import com.cognegix.cognegixapp.model.MainResponseMessageBoard;
import com.cognegix.cognegixapp.model.MyQueryThreadResponse;
import com.cognegix.cognegixapp.model.ThreadMessagesResponse;
import com.cognegix.cognegixapp.model.UserProfileResponse;
import com.cognegix.cognegixapp.model.badges.ProfileBadgesResponse;
import com.cognegix.cognegixapp.model.document.ResponseDocument;
import com.cognegix.cognegixapp.model.loginresponse.LoginReponces;
import com.cognegix.cognegixapp.model.NotificationReponseModel;
import com.cognegix.cognegixapp.model.program.AllProgramResponse;
import com.cognegix.cognegixapp.model.program.ProgramDetailsResponse;
import com.cognegix.cognegixapp.model.program.ProgramResponse;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {


    //For login
    @FormUrlEncoded
    @POST("user/authenticate")
    Call<LoginReponces> doLogin(@Field("username") String username, @Field("password") String password, @Field("device_type") String device_type);


    //for forgot password
    @FormUrlEncoded
    @POST("user/password/forget")
    Call<ForgotResponse> doForgotPassword(@Field("email") String email);

    //for forgot opt verification
    @FormUrlEncoded
    @POST("user/verify-otp")
    Call<ForgotResponse> doForgotPasswordOtpVerification(@Field("email") String email,
                                                         @Field("otp") String otp,
                                                         @Field("password") String password,
                                                         @Field("password_confirmation") String password_confirmation);


    //for change passsword

    @FormUrlEncoded
    @POST("user/password/reset")
    Call<ForgotResponse> doChangePassword(@Header("Accept") String accept, @Header("Authorization") String authorization, @Field("old_password") String old_password, @Field("password") String password, @Field("password_confirmation") String password_confirmation);


    //for get list of  notiification
    @FormUrlEncoded
    @POST("user/notification")
    Call<NotificationReponseModel> doNotiifcationList(@Header("Accept") String accept, @Header("Authorization") String authorization, @Field("notification_is_read") String notification_is_read);


    //For Help desk as gest
/*
    @FormUrlEncoded
    @POST("guest/help/raise")
    Call<ForgotResponse> doSendDataAsGeast(@Header("Accept") String accept, @Header("Authorization") String authorization, @Field("help_type") String help_type, @Field("help_content") String help_content, @Field("first_name") String first_name, @Field("last_name") String last_name, @Field("organization_name") String organization_name, @Field("email") String email);
*/


    @FormUrlEncoded
    @POST("guest/help/raise")
    Call<ForgotResponse> doSendDataAsGeast(@Field("help_type") String help_type, @Field("help_content") String help_content, @Field("first_name") String first_name, @Field("last_name") String last_name, @Field("organization_name") String organization_name, @Field("email") String email);


    //For Help desk as gest
    @FormUrlEncoded
    @POST("user/update-detail")
    Call<LoginReponces> doEditProfile(@Header("Accept") String accept,
                                      @Header("Authorization") String authorization,
                                      @Field("first_name") String first_name,
                                      @Field("last_name") String last_name,
                                      @Field("gender") String gender,
                                      @Field("linkedin_profile") String linkedin_profile,
                                      @Field("facebook_id") String facebook_id,
                                      @Field("twitter_handle") String twitter_handle,
                                      @Field("google_profile") String google_profile,
                                      @Field("designation") String designation,
                                      @Field("personal_email") String personal_email,
                                      @Field("email") String email,
                                      @Field("work_phone_number") String work_phone_number,
                                      @Field("home_phone_number") String home_phone_number);


    //For program listing
    @GET("user/programs")
    Call<AllProgramResponse> doFeatchProgramList(@Header("Accept") String accept,
                                                 @Header("Authorization") String authorization);


    //For forum add post
    @FormUrlEncoded
    @POST("forum/thread/post/add")
    Call<ForumAddResponse> doAddForumPost(@Header("Accept") String accept, @Header("Authorization") String authorization,
                                          @Field("program_id") String program_id,
                                          @Field("message") String message,
                                          @Field("forum_id") String forum_id,
                                          @Field("thread_id") String thread_id);


    //For program listing
    @GET("user/profile")
    Call<UserProfileResponse> doViewProfile(@Header("Accept") String accept, @Header("Authorization") String authorization);

    //for image upload of profile photo
    @Multipart
    @POST("user/update/profile/picture")
    Call<LoginReponces> doUploadImage(@Header("Accept") String accept, @Header("Authorization") String authorization, @Part MultipartBody.Part file);


    //For profile type
    @FormUrlEncoded
    @POST("user/update/profile-type")
    Call<ForgotResponse> doUpdateProfileType(@Header("Accept") String accept, @Header("Authorization") String authorization, @Field("profile_type") String profile_type);


    //For forum thread posts
    @FormUrlEncoded
    @POST("forum/thread/posts")
    Call<ForumResponse> doFetchFoeumList(@Header("Accept") String accept,
                                         @Header("Authorization") String authorization,
                                         @Field("program_id") String program_id,
                                         @Field("page") String page,
                                         @Field("forum_id") String forum_id,
                                         @Field("thread_id") String thread_id);
    //  @Field("program_user_id ") String program_user_id );

    //Help Desk: Ask
    @FormUrlEncoded
    @POST("user/help/raise")
    Call<ForgotResponse> doHelpASkAsUser(@Header("Accept") String accept, @Header("Authorization") String authorization, @Field("help_type") String help_type, @Field("help_content") String help_content);


    //For Forum inter mediate Listing
    @FormUrlEncoded
    @POST("forum/threads")
    Call<ForumInterResponse> doFetchForumInterList(@Header("Accept") String accept, @Header("Authorization") String authorization, @Field("program_id") String program_id, @Field("page") String page);


    //For Forum inter mediate Listing
    @FormUrlEncoded
    @POST("forum/thread/add")
    Call<ForgotResponse> doAddForumThread(@Header("Accept") String accept, @Header("Authorization") String authorization,
                                          @Field("program_id") String program_id,
                                          @Field("subject") String subject,
                                          @Field("description") String description);


    //For progarm listing
    @GET("load/dashboard")
    Call<ProgramResponse> doProgarmListMain(@Header("Accept") String accept,
                                            @Header("Authorization") String authorization);


    //for set flag is notification read
    @FormUrlEncoded
    @POST("user/notification/mark/read")
    Call<ForgotResponse> doSetNotificationRead(@Header("Accept") String accept,
                                               @Header("Authorization") String authorization,
                                               @Field("notification_id") String notification_id);


    //for thread lists
    @FormUrlEncoded
    @POST("my-query/thread/list")
    Call<MyQueryThreadResponse> doFetchQueryThread(@Header("Accept") String accept,
                                                   @Header("Authorization") String authorization,
                                                   @Field("program_id") String programId,
                                                   @Field("page") String page,
                                                   @Field("program_user_id") String program_user_id);

    //for thread chat listr
    @FormUrlEncoded
    @POST("my-query/thread/message/list")
    Call<ThreadMessagesResponse> doFetchQueryThreadChat(@Header("Accept") String accept,
                                                        @Header("Authorization") String authorization,
                                                        @Field("query_thread_id") String query_thread_id);


    //for thread add chat
    @FormUrlEncoded
    @POST("my-query/thread/message")
    Call<ForgotResponse> doASkchQueryThreadChat(@Header("Accept") String accept,
                                                @Header("Authorization") String authorization,
                                                @Field("query_thread_id") String query_thread_id,
                                                @Field("query") String query,
                                                @Field("program_user_id") String program_user_id);


    //for user badges progarm wise
    @FormUrlEncoded
    @POST("user/program/badges")
    Call<BadgesResponse> doGetBadgesListByProgarm(@Header("Accept") String accept,
                                                  @Header("Authorization") String authorization,
                                                  @Field("programId") String programId,
                                                  @Field("userId") String userId);

    //for get leader badges
    @FormUrlEncoded
    @POST("program/badge/leaders")
    Call<BadgesLederResponse> doGetLeaderBadgesListByProgarm(@Header("Accept") String accept,
                                                             @Header("Authorization") String authorization,
                                                             @Field("programId") String programId);


    //get user profile badges
    @GET("user/badges")
    Call<ProfileBadgesResponse> doGetBagdesForProfile(@Header("Accept") String accept,
                                                      @Header("Authorization") String authorization);

    //get user profile badges
    @FormUrlEncoded
    @POST("program/topics")
    Call<ProgarmTopicResponse> doGetProgramTopicForQuery(@Header("Accept") String accept,
                                                         @Header("Authorization") String authorization,
                                                         @Field("program_id") String program_id);


    //get user profile badges
    @FormUrlEncoded
    @POST("my-query/check/thread")
    Call<ForgotResponse> doCreateQueryThread(@Header("Accept") String accept,
                                             @Header("Authorization") String authorization,
                                             @Field("program_id") String program_id,
                                             @Field("topic_id") String topic_id,
                                             @Field("receiver_user_id") String receiver_user_id,
                                             @Field("query") String query,
                                             @Field("program_user_id") String program_user_id,
                                             @Field("receiver_program_user_id ") String receiver_program_user_id );


    //For program participant list
    @FormUrlEncoded
    @POST("program/users")
    Call<ProgramParticipantResponse> doGetParticipantList(@Header("Accept") String accept,
                                                          @Header("Authorization") String authorization,
                                                          @Field("program_id") String program_id);


    //For program participant  Distribute Badge  Participant to Participant Facilitator to Participan
    @FormUrlEncoded
    @POST("badge/distribute")
    Call<AssingBadgesResponse> doDistributeBadgeForPartcipant(@Header("Accept") String accept,
                                                              @Header("Authorization") String authorization,
                                                              @Field("program_id") String program_id,
                                                              @Field("badge_count") String badge_count,
                                                              @Field("recipient_user_id") String recipient_user_id);


    //get Faq Quation list
    @GET("help/faq/list")
    Call<FAQResponse> doGetFAQList(@Header("Accept") String accept,
                                   @Header("Authorization") String authorization);


    //for search program topic
    @FormUrlEncoded
    @POST("program/learning/tree/search")
    Call<SearchResponse> doSearch(@Header("Accept") String accept,
                                  @Header("Authorization") String authorization,
                                  @Field("search") String search);


    //For pasr program listing
    @GET("user/past/programs")
    Call<ProgramResponse> getPastProgram(@Header("Accept") String accept,
                                         @Header("Authorization") String authorization);


    //for delete thread msg
    @FormUrlEncoded
    @POST("forum/thread/post/delete")
    Call<ForgotResponse> doDeletePost(@Header("Accept") String accept,
                                      @Header("Authorization") String authorization,
                                      @Field("program_id") String program_id,
                                      @Field("message_id") String message_id,
                                      @Field("forum_id") String forum_id,
                                      @Field("thread_id") String thread_id);

    //for progarm details
    @FormUrlEncoded
    @POST("program/detail")
    Call<ProgramDetailsResponse> getProgramDetails(@Header("Accept") String accept,
                                                   @Header("Authorization") String authorization,
                                                   @Field("program_id") String program_id);


    //for progarm details
    @FormUrlEncoded
    @POST("my-query/thread/message/delete")
    Call<ForgotResponse> doDeleteQueryThread(@Header("Accept") String accept,
                                             @Header("Authorization") String authorization,
                                             @Field("query_thread_id") String query_thread_id,
                                             @Field("message_id") String message_id);

    //for progarm details
    @FormUrlEncoded
    @POST("video-call/initiate")
    Call<VideoCallResponse> doInitiateCall(@Header("Accept") String accept,
                                           @Header("Authorization") String authorization,
                                           @Field("program_id") String program_id,
                                           @Field("participant_ids[]") ArrayList<String> participant_ids);


    //for progarm details
    @FormUrlEncoded
    @POST("user/program/documents")
    Call<ResponseDocument> doGetResponseDocument(@Header("Accept") String accept,
                                                 @Header("Authorization") String authorization,
                                                 @Field("program_id") String program_id);


    //for get contact number
    @GET("help/contact/detail")
    Call<ContactResponseModel> doGetContactResponseModel();

}
