package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ModelNotification implements Serializable {
   /* private String subject;
    private String postby;
    private String date;


    public ModelNotification(String subject, String postby, String date) {
        this.subject = subject;
        this.postby = postby;
        this.date = date;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPostby() {
        return postby;
    }

    public void setPostby(String postby) {
        this.postby = postby;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }*/


    @SerializedName("id")
    private String id;//": 2,
    @SerializedName("notification")
    private String notification;//": "Test notification 2",
    @SerializedName("tenant_party_id")
    private String tenant_party_id;//": 1,
    @SerializedName("party_id")
    private String party_id;//": 3,
    @SerializedName("is_read")
    private String is_read;//": 0,
    @SerializedName("created_at")
    private String created_at;//": "2018-02-17 10:41:53",
    @SerializedName("updated_at")
    private String updated_at;//": null,
    @SerializedName("deleted_at")
    private String deleted_at;//": null,
    @SerializedName("created_by")
    private String created_by;//": 0,
    @SerializedName("updated_by")
    private String updated_by;//": 0,
    @SerializedName("deleted_by")
    private String deleted_by;//": 0,
    @SerializedName("formatted_created_at")
    private String formatted_created_at;//": "3 months ago"

    @SerializedName("is_video_call")
    private String is_video_call;
    @SerializedName("video_call_url")
    private String video_call_url;


    public String getIs_video_call() {
        return is_video_call;
    }

    public void setIs_video_call(String is_video_call) {
        this.is_video_call = is_video_call;
    }

    public String getVideo_call_url() {
        return video_call_url;
    }

    public void setVideo_call_url(String video_call_url) {
        this.video_call_url = video_call_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getTenant_party_id() {
        return tenant_party_id;
    }

    public void setTenant_party_id(String tenant_party_id) {
        this.tenant_party_id = tenant_party_id;
    }

    public String getParty_id() {
        return party_id;
    }

    public void setParty_id(String party_id) {
        this.party_id = party_id;
    }

    public String getIs_read() {
        return is_read;
    }

    public void setIs_read(String is_read) {
        this.is_read = is_read;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getDeleted_by() {
        return deleted_by;
    }

    public void setDeleted_by(String deleted_by) {
        this.deleted_by = deleted_by;
    }

    public String getFormatted_created_at() {
        return formatted_created_at;
    }

    public void setFormatted_created_at(String formatted_created_at) {
        this.formatted_created_at = formatted_created_at;
    }
}
