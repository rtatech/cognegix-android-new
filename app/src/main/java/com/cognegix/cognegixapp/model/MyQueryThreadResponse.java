package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyQueryThreadResponse {

    @SerializedName("status")
    private String status;//": true,
    @SerializedName("message")
    private String message;//": "User authenticated successfully.",
    @SerializedName("queries")
    private List<MyQueryThreadModel> myQueryThreadModelList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<MyQueryThreadModel> getMyQueryThreadModelList() {
        return myQueryThreadModelList;
    }

    public void setMyQueryThreadModelList(List<MyQueryThreadModel> myQueryThreadModelList) {
        this.myQueryThreadModelList = myQueryThreadModelList;
    }
}
