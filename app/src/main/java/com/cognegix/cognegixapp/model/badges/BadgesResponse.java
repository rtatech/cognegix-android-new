package com.cognegix.cognegixapp.model.badges;

import com.google.gson.annotations.SerializedName;

public class BadgesResponse {
    @SerializedName("status")
    private String status;//": true,

    @SerializedName("user_badges_list")
    private BadgesModel badgesModel;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BadgesModel getBadgesModel() {
        return badgesModel;
    }

    public void setBadgesModel(BadgesModel badgesModel) {
        this.badgesModel = badgesModel;
    }
}
