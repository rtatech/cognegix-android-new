package com.cognegix.cognegixapp.model.badges;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProfileBadgeModel implements Serializable {
    private String title;
    private String count;
    private int imagePath;


    @SerializedName("fastestActivityBadge")
    private String fastestActivityBadge;//": 0,
    @SerializedName("contentRatingBadgeCount")
    private String contentRatingBadgeCount;//": 0,
    @SerializedName("quizCompletionBadgeCount")
    private String quizCompletionBadgeCount;//": 0,
    @SerializedName("facilitatorBadgeCount")
    private String facilitatorBadgeCount;//": "3",
    @SerializedName("participantBadgeCount")
    private String participantBadgeCount;//": 0


    public ProfileBadgeModel(String title, String count, int imagePath) {
        this.title = title;
        this.count = count;
        this.imagePath = imagePath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public int getImagePath() {
        return imagePath;
    }

    public void setImagePath(int imagePath) {
        this.imagePath = imagePath;
    }


    public String getFastestActivityBadge() {
        return fastestActivityBadge;
    }

    public void setFastestActivityBadge(String fastestActivityBadge) {
        this.fastestActivityBadge = fastestActivityBadge;
    }

    public String getContentRatingBadgeCount() {
        return contentRatingBadgeCount;
    }

    public void setContentRatingBadgeCount(String contentRatingBadgeCount) {
        this.contentRatingBadgeCount = contentRatingBadgeCount;
    }

    public String getQuizCompletionBadgeCount() {
        return quizCompletionBadgeCount;
    }

    public void setQuizCompletionBadgeCount(String quizCompletionBadgeCount) {
        this.quizCompletionBadgeCount = quizCompletionBadgeCount;
    }

    public String getFacilitatorBadgeCount() {
        return facilitatorBadgeCount;
    }

    public void setFacilitatorBadgeCount(String facilitatorBadgeCount) {
        this.facilitatorBadgeCount = facilitatorBadgeCount;
    }

    public String getParticipantBadgeCount() {
        return participantBadgeCount;
    }

    public void setParticipantBadgeCount(String participantBadgeCount) {
        this.participantBadgeCount = participantBadgeCount;
    }
}
