package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

public class ProgramPersonToAskModel {
    @SerializedName("id")
    private String id;//": 9,
    @SerializedName("name")
    private String name;//": "Shubhada Kularni (Program Director)"

    @SerializedName("program_user_id")
    private String program_user_id;//": 91


    public ProgramPersonToAskModel(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProgram_user_id() {
        return program_user_id;
    }

    public void setProgram_user_id(String program_user_id) {
        this.program_user_id = program_user_id;
    }
}
