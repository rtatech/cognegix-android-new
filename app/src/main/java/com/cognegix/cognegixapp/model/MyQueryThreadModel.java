package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

public class MyQueryThreadModel {
    @SerializedName("query_thread_id")
    private String query_thread_id;//": 14,
    @SerializedName("sender_user_id")
    private String sender_user_id;//": 3,
    @SerializedName("sender_name")
    private String sender_name;//": "Sabera Thyagrajan",
    @SerializedName("receiver_user_id")
    private String receiver_user_id;//": 10,
    @SerializedName("receiver_name")
    private String receiver_name;//": "Manisha Tambe",
    @SerializedName("program_id")
    private String program_id;//": 1,
    @SerializedName("program_name")
    private String program_name;//": "Negotiation",
    @SerializedName("topic_name")
    private String topic_name;//": null,
    @SerializedName("topic_id")
    private String topic_id;//": 11,
    @SerializedName("created_at")
    private String created_at;//": "2018-06-14 20:23:37",
    @SerializedName("query")
    private String query;//": "My Query 22",
    @SerializedName("formatted_created_at")
    private String formatted_created_at;//": "9 minutes ago"


    public String getQuery_thread_id() {
        return query_thread_id;
    }

    public void setQuery_thread_id(String query_thread_id) {
        this.query_thread_id = query_thread_id;
    }

    public String getSender_user_id() {
        return sender_user_id;
    }

    public void setSender_user_id(String sender_user_id) {
        this.sender_user_id = sender_user_id;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    public String getReceiver_user_id() {
        return receiver_user_id;
    }

    public void setReceiver_user_id(String receiver_user_id) {
        this.receiver_user_id = receiver_user_id;
    }

    public String getReceiver_name() {
        return receiver_name;
    }

    public void setReceiver_name(String receiver_name) {
        this.receiver_name = receiver_name;
    }

    public String getProgram_id() {
        return program_id;
    }

    public void setProgram_id(String program_id) {
        this.program_id = program_id;
    }

    public String getProgram_name() {
        return program_name;
    }

    public void setProgram_name(String program_name) {
        this.program_name = program_name;
    }

    public String getTopic_name() {
        return topic_name;
    }

    public void setTopic_name(String topic_name) {
        this.topic_name = topic_name;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getFormatted_created_at() {
        return formatted_created_at;
    }

    public void setFormatted_created_at(String formatted_created_at) {
        this.formatted_created_at = formatted_created_at;
    }
}
