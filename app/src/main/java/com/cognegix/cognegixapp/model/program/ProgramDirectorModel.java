package com.cognegix.cognegixapp.model.program;

import com.google.gson.annotations.SerializedName;

public class ProgramDirectorModel {

    @SerializedName("name")
    private String name;//": "Sabera Thyagrajan",
    @SerializedName("profile")
    private String profile;//": "Expert on presentation skills"

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }
}
