package com.cognegix.cognegixapp.model.badges;

import com.google.gson.annotations.SerializedName;

public class ContentRatingBadgeCount {
    @SerializedName("name")
    private String name;//": "Sabera Thyagrajan",
    @SerializedName("count")
    private String count;//": 0

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
