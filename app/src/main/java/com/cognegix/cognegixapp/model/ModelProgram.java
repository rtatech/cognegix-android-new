package com.cognegix.cognegixapp.model;


public class ModelProgram {

    private String title;
    private String desc;
    private int imagePath;


    public ModelProgram(String title, String desc, int imagePath) {
        this.title = title;
        this.desc = desc;
        this.imagePath = imagePath;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getImagePath() {
        return imagePath;
    }

    public void setImagePath(int imagePath) {
        this.imagePath = imagePath;
    }
}
