package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchResponse {
    @SerializedName("status")
    private String status;//": "Soft Skills",
    @SerializedName("message")
    private String message;//": "User authenticated successfully.",


    @SerializedName("search_result")
    private List<SearchModel> searchModelList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SearchModel> getSearchModelList() {
        return searchModelList;
    }

    public void setSearchModelList(List<SearchModel> searchModelList) {
        this.searchModelList = searchModelList;
    }
}
