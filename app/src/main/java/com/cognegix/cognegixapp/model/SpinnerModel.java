package com.cognegix.cognegixapp.model;


public class SpinnerModel {

    private String title;



    public SpinnerModel(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
