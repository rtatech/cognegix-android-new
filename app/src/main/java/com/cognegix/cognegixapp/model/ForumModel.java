package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

public class ForumModel {
    @SerializedName("id")
    private String id;//": 72,
    @SerializedName("forum_thread_id")
    private String forum_thread_id;//": 1,
    @SerializedName("program_user_id")
    private String program_user_id;//": 1,
    @SerializedName("message")
    private String message;//": "Hey there",
    @SerializedName("created_at")
    private String created_at;//": "2018-06-05 18:08:17",
    @SerializedName("updated_at")
    private String updated_at;//": "2018-06-05 18:08:17",
    @SerializedName("deleted_at")
    private String deleted_at;//": null,
    @SerializedName("created_by")
    private String created_by;//": 0,
    @SerializedName("updated_by")
    private String updated_by;//": 0,
    @SerializedName("deleted_by")
    private String deleted_by;//": 0,
    @SerializedName("post_by")
    private String post_by;//": "Manisha Tambe",
    @SerializedName("user_id")
    private String user_id;//": 10


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getForum_thread_id() {
        return forum_thread_id;
    }

    public void setForum_thread_id(String forum_thread_id) {
        this.forum_thread_id = forum_thread_id;
    }

    public String getProgram_user_id() {
        return program_user_id;
    }

    public void setProgram_user_id(String program_user_id) {
        this.program_user_id = program_user_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getDeleted_by() {
        return deleted_by;
    }

    public void setDeleted_by(String deleted_by) {
        this.deleted_by = deleted_by;
    }

    public String getPost_by() {
        return post_by;
    }

    public void setPost_by(String post_by) {
        this.post_by = post_by;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
