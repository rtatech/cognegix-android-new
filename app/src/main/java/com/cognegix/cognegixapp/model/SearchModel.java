package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

public class SearchModel {
    @SerializedName("program_name")
    private String program_name;//": "Soft Skills",
    @SerializedName("topic_name")
    private String topic_name;//": "First learning tree",
    @SerializedName("launch_url")
    private String launch_url;//": "http://182.59.4.193:8010/svg?tree_type=PLT&tree_id=6&parent_node_title=Soft Skills&parent_node_id=73&user_id=11"

    private int color = -1;

    public String getProgram_name() {
        return program_name;
    }

    public void setProgram_name(String program_name) {
        this.program_name = program_name;
    }

    public String getTopic_name() {
        return topic_name;
    }

    public void setTopic_name(String topic_name) {
        this.topic_name = topic_name;
    }

    public String getLaunch_url() {
        return launch_url;
    }

    public void setLaunch_url(String launch_url) {
        this.launch_url = launch_url;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
