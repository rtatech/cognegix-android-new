package com.cognegix.cognegixapp.model.program;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProgramDetailsResponse {
    @SerializedName("status")
    private String status;//": true,

    @SerializedName("program_detail")
    private ProgramModel programModel;


    @SerializedName("program_start_date")
    private String program_start_date;//": "23rd Jun 2018 12:00 AM",
    @SerializedName("program_end_date")
    private String program_end_date;//": "30th Jun 2018 12:00 AM"


    @SerializedName("program_director")
    private ProgramDirectorModel programDirectorModel;

    @SerializedName("program_coordinators")
    private List<ProgramCoordinatorsModel> programCoordinatorsModelList;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ProgramModel getProgramModel() {
        return programModel;
    }

    public void setProgramModel(ProgramModel programModel) {
        this.programModel = programModel;
    }

    public String getProgram_start_date() {
        return program_start_date;
    }

    public void setProgram_start_date(String program_start_date) {
        this.program_start_date = program_start_date;
    }

    public String getProgram_end_date() {
        return program_end_date;
    }

    public void setProgram_end_date(String program_end_date) {
        this.program_end_date = program_end_date;
    }


    public ProgramDirectorModel getProgramDirectorModel() {
        return programDirectorModel;
    }

    public void setProgramDirectorModel(ProgramDirectorModel programDirectorModel) {
        this.programDirectorModel = programDirectorModel;
    }

    public List<ProgramCoordinatorsModel> getProgramCoordinatorsModelList() {
        return programCoordinatorsModelList;
    }

    public void setProgramCoordinatorsModelList(List<ProgramCoordinatorsModel> programCoordinatorsModelList) {
        this.programCoordinatorsModelList = programCoordinatorsModelList;
    }
}
