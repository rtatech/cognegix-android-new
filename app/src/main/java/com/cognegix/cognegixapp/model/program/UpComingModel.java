package com.cognegix.cognegixapp.model.program;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpComingModel {
    @SerializedName("day")
    private String day;//": "Fri",
    @SerializedName("date")
    private String date;//": "15",
    @SerializedName("month")
    private String month;//": "Jun",
    @SerializedName("full_date")
    private String full_date;//": "09 Jul 2018",

    @SerializedName("activities")
    private List<UpComingEventModel> upComingEventModelList;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public List<UpComingEventModel> getUpComingEventModelList() {
        return upComingEventModelList;
    }

    public void setUpComingEventModelList(List<UpComingEventModel> upComingEventModelList) {
        this.upComingEventModelList = upComingEventModelList;
    }

    public String getFull_date() {
        return full_date;
    }

    public void setFull_date(String full_date) {
        this.full_date = full_date;
    }
}
