package com.cognegix.cognegixapp.model.badges;


import com.google.gson.annotations.SerializedName;

public class BadgesModel {

    private String title;
    private String count;
    private int imagePath;

    @SerializedName("program_id")
    private String program_id;//": 1,
    @SerializedName("user_id")
    private String user_id;//": 3,
    @SerializedName("id")
    private String id;//": 1,
    @SerializedName("max_distribution_badges")
    private String max_distribution_badges;//": 0,
    @SerializedName("current_distribution_badges")
    private String current_distribution_badges;//": 0,
    @SerializedName("content_rated_count")
    private String content_rated_count;//": 1,
    @SerializedName("quiz_completion_badge_count")
    private String quiz_completion_badge_count;//": 0,
    @SerializedName("fastest_activity_badge_count")
    private String fastest_activity_badge_count;//": 0,
    @SerializedName("content_rating_badge_count")
    private String content_rating_badge_count;//": 0,
    @SerializedName("facilitator_badge_count")
    private String facilitator_badge_count;//": 3,
    @SerializedName("participant_badge_count")
    private String participant_badge_count;//": 0


    public BadgesModel(String title, String count, int imagePath) {
        this.title = title;
        this.count = count;
        this.imagePath = imagePath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public int getImagePath() {
        return imagePath;
    }

    public void setImagePath(int imagePath) {
        this.imagePath = imagePath;
    }

    public String getProgram_id() {
        return program_id;
    }

    public void setProgram_id(String program_id) {
        this.program_id = program_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMax_distribution_badges() {
        return max_distribution_badges;
    }

    public void setMax_distribution_badges(String max_distribution_badges) {
        this.max_distribution_badges = max_distribution_badges;
    }

    public String getCurrent_distribution_badges() {
        return current_distribution_badges;
    }

    public void setCurrent_distribution_badges(String current_distribution_badges) {
        this.current_distribution_badges = current_distribution_badges;
    }

    public String getContent_rated_count() {
        return content_rated_count;
    }

    public void setContent_rated_count(String content_rated_count) {
        this.content_rated_count = content_rated_count;
    }

    public String getQuiz_completion_badge_count() {
        return quiz_completion_badge_count;
    }

    public void setQuiz_completion_badge_count(String quiz_completion_badge_count) {
        this.quiz_completion_badge_count = quiz_completion_badge_count;
    }

    public String getFastest_activity_badge_count() {
        return fastest_activity_badge_count;
    }

    public void setFastest_activity_badge_count(String fastest_activity_badge_count) {
        this.fastest_activity_badge_count = fastest_activity_badge_count;
    }

    public String getContent_rating_badge_count() {
        return content_rating_badge_count;
    }

    public void setContent_rating_badge_count(String content_rating_badge_count) {
        this.content_rating_badge_count = content_rating_badge_count;
    }

    public String getFacilitator_badge_count() {
        return facilitator_badge_count;
    }

    public void setFacilitator_badge_count(String facilitator_badge_count) {
        this.facilitator_badge_count = facilitator_badge_count;
    }

    public String getParticipant_badge_count() {
        return participant_badge_count;
    }

    public void setParticipant_badge_count(String participant_badge_count) {
        this.participant_badge_count = participant_badge_count;
    }
}
