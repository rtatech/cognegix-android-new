package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

public class VideoCallResponse {

    @SerializedName("status")
    private String status;//": true,
    @SerializedName("message")
    private String message;//": "Video call initiated successfully.",
    @SerializedName("join_url")
    private String join_url;//": "https://staging.cognegixlearning.com/bigbluebutton/api/join?meetingID=91_16_JXIGCWPR&fullName=Test+User&password=T94N69SO&redirect=true&joinViaHtml5=false&checksum=7b3ad2ff90fba07bc084a4ea71daea0ba5bb4aa6"

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getJoin_url() {
        return join_url;
    }

    public void setJoin_url(String join_url) {
        this.join_url = join_url;
    }
}
