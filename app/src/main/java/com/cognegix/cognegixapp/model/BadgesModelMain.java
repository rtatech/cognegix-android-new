package com.cognegix.cognegixapp.model;

public class BadgesModelMain {
    private String title;
    private String shortForm;

    private int color = -1;

    public BadgesModelMain(String title, String shortForm, int color) {
        this.title = title;
        this.shortForm = shortForm;
        this.color = color;
    }

    public String getShortForm() {
        return shortForm;
    }

    public void setShortForm(String shortForm) {
        this.shortForm = shortForm;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
