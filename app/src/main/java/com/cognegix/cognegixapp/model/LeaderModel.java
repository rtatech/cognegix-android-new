package com.cognegix.cognegixapp.model;


public class LeaderModel {

    private String activity;
    private String name;
    private String count;
    private int imagePath;


    public LeaderModel(String activity, String name, String count, int imagePath) {
        this.activity = activity;
        this.name = name;
        this.count = count;
        this.imagePath = imagePath;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImagePath() {
        return imagePath;
    }

    public void setImagePath(int imagePath) {
        this.imagePath = imagePath;
    }
}
