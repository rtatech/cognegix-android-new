package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ProgramParticipantResponse implements Serializable{
    @SerializedName("status")
    private String status;//": true,

    @SerializedName("user_list")
    private List<ModelParticipant> modelParticipantList;
    @SerializedName("message")
    private String message;//": "User authenticated successfully.",

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ModelParticipant> getModelParticipantList() {
        return modelParticipantList;
    }

    public void setModelParticipantList(List<ModelParticipant> modelParticipantList) {
        this.modelParticipantList = modelParticipantList;
    }
}
