package com.cognegix.cognegixapp.model.program;

import com.google.gson.annotations.SerializedName;

public class LearningTree {

    @SerializedName("programDirectorProfile")
    private String programDirectorProfile;//": "Expert on emotional intelligence",
    @SerializedName("programDirectorVideoId")
    private String programDirectorVideoId;//": "2",
    @SerializedName("programDirectorVideoUrl")
    private String programDirectorVideoUrl;//": "/storage/mediaserver/video/2/5af719853712b.mp4",
    @SerializedName("programDirectorVideoName")
    private String programDirectorVideoName;//": "VID-20180430-WA0002.mp4",
    @SerializedName("programDirectorVideoAvailableTo")
    private String programDirectorVideoAvailableTo;//": "29-09-2018",
    @SerializedName("programDirectorVideoAvailableFrom")
    private String programDirectorVideoAvailableFrom;//": "16-06-2018"

    public String getProgramDirectorProfile() {
        return programDirectorProfile;
    }

    public void setProgramDirectorProfile(String programDirectorProfile) {
        this.programDirectorProfile = programDirectorProfile;
    }

    public String getProgramDirectorVideoId() {
        return programDirectorVideoId;
    }

    public void setProgramDirectorVideoId(String programDirectorVideoId) {
        this.programDirectorVideoId = programDirectorVideoId;
    }

    public String getProgramDirectorVideoUrl() {
        return programDirectorVideoUrl;
    }

    public void setProgramDirectorVideoUrl(String programDirectorVideoUrl) {
        this.programDirectorVideoUrl = programDirectorVideoUrl;
    }

    public String getProgramDirectorVideoName() {
        return programDirectorVideoName;
    }

    public void setProgramDirectorVideoName(String programDirectorVideoName) {
        this.programDirectorVideoName = programDirectorVideoName;
    }

    public String getProgramDirectorVideoAvailableTo() {
        return programDirectorVideoAvailableTo;
    }

    public void setProgramDirectorVideoAvailableTo(String programDirectorVideoAvailableTo) {
        this.programDirectorVideoAvailableTo = programDirectorVideoAvailableTo;
    }

    public String getProgramDirectorVideoAvailableFrom() {
        return programDirectorVideoAvailableFrom;
    }

    public void setProgramDirectorVideoAvailableFrom(String programDirectorVideoAvailableFrom) {
        this.programDirectorVideoAvailableFrom = programDirectorVideoAvailableFrom;
    }
}
