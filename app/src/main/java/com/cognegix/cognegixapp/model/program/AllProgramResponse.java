package com.cognegix.cognegixapp.model.program;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllProgramResponse {
    @SerializedName("status")
    private String status;//": true,


    @SerializedName("program_list")
    private List<ProgramModel> program_list;



    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



    public List<ProgramModel> getProgram_list() {
        return program_list;
    }

    public void setProgram_list(List<ProgramModel> program_list) {
        this.program_list = program_list;
    }



}
