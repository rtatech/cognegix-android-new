package com.cognegix.cognegixapp.model;

import com.cognegix.cognegixapp.model.badges.ProfileBadgeModel;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ModelParticipant implements Serializable {
    @SerializedName("id")
    private String id;//": 5,
    @SerializedName("first_name")
    private String first_name;//": "sidhant",
    @SerializedName("last_name")
    private String last_name;//": "mohapatra",
    @SerializedName("email")
    private String email;//": "sid@gmail.com",
    @SerializedName("personal_email")
    private String personal_email;//": "sidhant@gmail.com",
    @SerializedName("gender")
    private String gender;//": "M",
    @SerializedName("employee_id")
    private String employee_id;//": null,
    @SerializedName("department")
    private String department;//": null,
    @SerializedName("designation")
    private String designation;//": "PM",
    @SerializedName("linkedin_profile")
    private String linkedin_profile;//": null,
    @SerializedName("facebook_id")
    private String facebook_id;//": null,
    @SerializedName("twitter_handle")
    private String twitter_handle;//": null,
    @SerializedName("google_profile")
    private String google_profile;//": null,
    @SerializedName("party_id")
    private String party_id;//": 7,
    @SerializedName("tenant_party_id")
    private String tenant_party_id;//": 1,
    @SerializedName("customer_party_id")
    private String customer_party_id;//": 6,
    @SerializedName("photo")
    private String photo;//": "http://182.59.4.193:8010/storagepublic//tenants/1/user/7/profile/5af6f89bc2dcd.png",
    @SerializedName("work_phone_number")
    private String work_phone_number;//": "9632587412",
    @SerializedName("home_phone_number")
    private String home_phone_number;//": null,
    @SerializedName("is_private")
    private String is_private;//": 0,
    @SerializedName("user_id")
    private String user_id;//": 6,
    @SerializedName("role_name")
    private String role_name;//": "Participant"
    @SerializedName("organization_name")
    private String organization_name;
    @SerializedName("remaining_distribution_badges")
    private String remaining_distribution_badges;
    @SerializedName("badges")
    private ProfileBadgeModel profileBadgeModel;

    private boolean isSelected = false;


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPersonal_email() {
        return personal_email;
    }

    public void setPersonal_email(String personal_email) {
        this.personal_email = personal_email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getLinkedin_profile() {
        return linkedin_profile;
    }

    public void setLinkedin_profile(String linkedin_profile) {
        this.linkedin_profile = linkedin_profile;
    }

    public String getFacebook_id() {
        return facebook_id;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }

    public String getTwitter_handle() {
        return twitter_handle;
    }

    public void setTwitter_handle(String twitter_handle) {
        this.twitter_handle = twitter_handle;
    }

    public String getGoogle_profile() {
        return google_profile;
    }

    public void setGoogle_profile(String google_profile) {
        this.google_profile = google_profile;
    }

    public String getParty_id() {
        return party_id;
    }

    public void setParty_id(String party_id) {
        this.party_id = party_id;
    }

    public String getTenant_party_id() {
        return tenant_party_id;
    }

    public void setTenant_party_id(String tenant_party_id) {
        this.tenant_party_id = tenant_party_id;
    }

    public String getCustomer_party_id() {
        return customer_party_id;
    }

    public void setCustomer_party_id(String customer_party_id) {
        this.customer_party_id = customer_party_id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getWork_phone_number() {
        return work_phone_number;
    }

    public void setWork_phone_number(String work_phone_number) {
        this.work_phone_number = work_phone_number;
    }

    public String getHome_phone_number() {
        return home_phone_number;
    }

    public void setHome_phone_number(String home_phone_number) {
        this.home_phone_number = home_phone_number;
    }

    public String getIs_private() {
        return is_private;
    }

    public void setIs_private(String is_private) {
        this.is_private = is_private;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

    public ProfileBadgeModel getProfileBadgeModel() {
        return profileBadgeModel;
    }

    public void setProfileBadgeModel(ProfileBadgeModel profileBadgeModel) {
        this.profileBadgeModel = profileBadgeModel;
    }

    public String getOrganization_name() {
        return organization_name;
    }

    public void setOrganization_name(String organization_name) {
        this.organization_name = organization_name;
    }

    public String getRemaining_distribution_badges() {
        return remaining_distribution_badges;
    }

    public void setRemaining_distribution_badges(String remaining_distribution_badges) {
        this.remaining_distribution_badges = remaining_distribution_badges;
    }
}


