package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ForumResponse {


    @SerializedName("status")
    private String status;//": true,
    @SerializedName("remaining")
    private String remaining;//": 52,
    @SerializedName("message")
    private String message;//": 52,


    @SerializedName("messages")
    private List<ForumModel> forum_messages;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemaining() {
        return remaining;
    }

    public void setRemaining(String remaining) {
        this.remaining = remaining;
    }

    public List<ForumModel> getForum_messages() {
        return forum_messages;
    }

    public void setForum_messages(List<ForumModel> forum_messages) {
        this.forum_messages = forum_messages;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
