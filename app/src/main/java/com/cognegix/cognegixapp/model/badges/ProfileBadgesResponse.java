package com.cognegix.cognegixapp.model.badges;

import com.google.gson.annotations.SerializedName;

public class ProfileBadgesResponse {
    @SerializedName("status")
    private String status;//": "true",

    @SerializedName("userBadgesList")
    private ProfileBadgeModel profileBadgeModel;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ProfileBadgeModel getProfileBadgeModel() {
        return profileBadgeModel;
    }

    public void setProfileBadgeModel(ProfileBadgeModel profileBadgeModel) {
        this.profileBadgeModel = profileBadgeModel;
    }
}
