package com.cognegix.cognegixapp.model.program;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProgramResponse {
    @SerializedName("status")
    private String status;//": true,

    @SerializedName("unread_notification_count")
    private String unread_notification_count;//": 1,

    @SerializedName("program_list")
    private List<ProgramModel> program_list;

    @SerializedName("board_messages")
    private List<BoardMessagesModel> board_messages;

    @SerializedName("critical_alerts")
    private List<String> critical_alerts;

    @SerializedName("upcoming_activities")
    private List<UpComingModel> upComingModelList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUnread_notification_count() {
        return unread_notification_count;
    }

    public void setUnread_notification_count(String unread_notification_count) {
        this.unread_notification_count = unread_notification_count;
    }

    public List<ProgramModel> getProgram_list() {
        return program_list;
    }

    public void setProgram_list(List<ProgramModel> program_list) {
        this.program_list = program_list;
    }

    public List<BoardMessagesModel> getBoard_messages() {
        return board_messages;
    }

    public void setBoard_messages(List<BoardMessagesModel> board_messages) {
        this.board_messages = board_messages;
    }

    public List<UpComingModel> getUpComingModelList() {
        return upComingModelList;
    }

    public void setUpComingModelList(List<UpComingModel> upComingModelList) {
        this.upComingModelList = upComingModelList;
    }

    public List<String> getCritical_alerts() {
        return critical_alerts;
    }

    public void setCritical_alerts(List<String> critical_alerts) {
        this.critical_alerts = critical_alerts;
    }

}
