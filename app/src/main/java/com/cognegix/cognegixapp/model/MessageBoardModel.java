package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

public class MessageBoardModel {

    @SerializedName("id")
    private String id;//": 1,
    @SerializedName("message_subject")
    private String message_subject;//": "Test message 1",
    @SerializedName("message_body")
    private String message_body;//": "<h2>Test message Boday 1</h2>",
    @SerializedName("message_for")
    private String message_for;//": "all",
    @SerializedName("tenant_party_id")
    private String tenant_party_id;//": 1,
    @SerializedName("customer_party_ids")
    private String customer_party_ids;//": "",
    @SerializedName("program_ids")
    private String program_ids;//": "",
    @SerializedName("start_date")
    private String start_date;//": "2018-02-11 00:00:00",
    @SerializedName("end_date")
    private String end_date;//": "2018-03-14 00:00:00",
    @SerializedName("is_active")
    private String is_active;//": 1


    public MessageBoardModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage_subject() {
        return message_subject;
    }

    public void setMessage_subject(String message_subject) {
        this.message_subject = message_subject;
    }

    public String getMessage_body() {
        return message_body;
    }

    public void setMessage_body(String message_body) {
        this.message_body = message_body;
    }

    public String getMessage_for() {
        return message_for;
    }

    public void setMessage_for(String message_for) {
        this.message_for = message_for;
    }

    public String getTenant_party_id() {
        return tenant_party_id;
    }

    public void setTenant_party_id(String tenant_party_id) {
        this.tenant_party_id = tenant_party_id;
    }

    public String getCustomer_party_ids() {
        return customer_party_ids;
    }

    public void setCustomer_party_ids(String customer_party_ids) {
        this.customer_party_ids = customer_party_ids;
    }

    public String getProgram_ids() {
        return program_ids;
    }

    public void setProgram_ids(String program_ids) {
        this.program_ids = program_ids;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

}
