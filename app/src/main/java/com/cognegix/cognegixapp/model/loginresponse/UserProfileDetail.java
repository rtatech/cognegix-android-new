package com.cognegix.cognegixapp.model.loginresponse;

import com.google.gson.annotations.SerializedName;

public class UserProfileDetail {
    @SerializedName("id")
    private String id;//": 2,
    @SerializedName("first_name")
    private String first_name;//": "AKshay",
    @SerializedName("last_name")
    private String last_name;//": "Deshmukh",
    @SerializedName("email")
    private String email;//": "tagsab2000@outlook.com",
    @SerializedName("personal_email")
    private String personal_email;//": null,
    @SerializedName("gender")
    private String gender;//": "F",
    @SerializedName("employee_id")
    private String employee_id;//": null,
    @SerializedName("department")
    private String department;//": null,
    @SerializedName("designation")
    private String designation;//": null,
    @SerializedName("linkedin_profile")
    private String linkedin_profile;//": "http://127.0.0.1:8000/api/user/update-detail",
    @SerializedName("facebook_id")
    private String facebook_id;//": "http://www.facebook.com",
    @SerializedName("twitter_handle")
    private String twitter_handle;//": "http://127.0.0.1:8000/api/user/update-detail",
    @SerializedName("google_profile")
    private String google_profile;//": "http://127.0.0.1:8000/api/user/update-detail",
    @SerializedName("photo")
    private String photo;//": "/storage/tenants/1/user/3/profiles/31528128663.jpeg",
    @SerializedName("original_photo")
    private String original_photo;//": "31528128663.jpeg",
    @SerializedName("party_id")
    private String party_id;//": 3,
    @SerializedName("tenant_party_id")
    private String tenant_party_id;//": 1,
    @SerializedName("customer_party_id")
    private String customer_party_id;//": null,
    @SerializedName("is_private")
    private String is_private;//": 1,
    @SerializedName("profile_completion_percentage")
    private String profile_completion_percentage;//": 69.23
    @SerializedName("work_phone_number")
    private String work_phone_number;//": "3652365239",
    @SerializedName("home_phone_number")
    private String home_phone_number;//": "1234567890",


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPersonal_email() {
        return personal_email;
    }

    public void setPersonal_email(String personal_email) {
        this.personal_email = personal_email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getLinkedin_profile() {
        return linkedin_profile;
    }

    public void setLinkedin_profile(String linkedin_profile) {
        this.linkedin_profile = linkedin_profile;
    }

    public String getFacebook_id() {
        return facebook_id;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }

    public String getTwitter_handle() {
        return twitter_handle;
    }

    public void setTwitter_handle(String twitter_handle) {
        this.twitter_handle = twitter_handle;
    }

    public String getGoogle_profile() {
        return google_profile;
    }

    public void setGoogle_profile(String google_profile) {
        this.google_profile = google_profile;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getOriginal_photo() {
        return original_photo;
    }

    public void setOriginal_photo(String original_photo) {
        this.original_photo = original_photo;
    }

    public String getParty_id() {
        return party_id;
    }

    public void setParty_id(String party_id) {
        this.party_id = party_id;
    }

    public String getTenant_party_id() {
        return tenant_party_id;
    }

    public void setTenant_party_id(String tenant_party_id) {
        this.tenant_party_id = tenant_party_id;
    }

    public String getCustomer_party_id() {
        return customer_party_id;
    }

    public void setCustomer_party_id(String customer_party_id) {
        this.customer_party_id = customer_party_id;
    }

    public String getIs_private() {
        return is_private;
    }

    public void setIs_private(String is_private) {
        this.is_private = is_private;
    }

    public String getProfile_completion_percentage() {
        return profile_completion_percentage;
    }

    public void setProfile_completion_percentage(String profile_completion_percentage) {
        this.profile_completion_percentage = profile_completion_percentage;
    }


    public String getWork_phone_number() {
        return work_phone_number;
    }

    public void setWork_phone_number(String work_phone_number) {
        this.work_phone_number = work_phone_number;
    }

    public String getHome_phone_number() {
        return home_phone_number;
    }

    public void setHome_phone_number(String home_phone_number) {
        this.home_phone_number = home_phone_number;
    }
}
