package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

public class FAQModel {

    @SerializedName("title")
    private String title;//": "How do I know if my order qualifies for free shipping?",
    @SerializedName("description")
    private String description;//": "We're happy to offer Free Ground Shipping on online orders to our customers who have a total purchase that exceeds our Free Shipping threshold of $99.00. Free shipping is only available to customers shipping Ground in the Continental US. Orders shipping 2 Day, Overnight or Express will not be eligible but can be upgraded at the checkout for an additional cost."
    private boolean isSelectde = false;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSelectde() {
        return isSelectde;
    }

    public void setSelectde(boolean selectde) {
        isSelectde = selectde;
    }
}
