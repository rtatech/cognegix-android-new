package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ForumAddResponse {


    @SerializedName("status")
    private String status;//": true,
    @SerializedName("message")
    private String message;//": 52,
    @SerializedName("thread_post")
    private List<ForumModel> thread_post;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ForumModel> getThread_post() {
        return thread_post;
    }

    public void setThread_post(List<ForumModel> thread_post) {
        this.thread_post = thread_post;
    }
}
