package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

public class  ForgotResponse {

    @SerializedName("status")
    private String status;//": true,
    @SerializedName("message")
    private String message;//": "We have mailed you an OTP. Please check."


    public ForgotResponse(String status, String message) {
        this.status = status;
        this.message = message;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
