package com.cognegix.cognegixapp.model.program;

import com.google.gson.annotations.SerializedName;

public class UpComingEventModel {
    @SerializedName("topic_name")
    private String topic_name;//": "BATNA/ZOPA",
    @SerializedName("topic_id")
    private String topic_id;//": 2,
    @SerializedName("date")
    private String date;//": "2018-06-16"
    @SerializedName("launch_url")
    private String launch_url;//": "http://127.0.0.1:8000/svg?tree_id=3&tree_type=PLT&user_id=23&parent_node_title=Negotiation Copy 2&parent_node_id=88",


    public String getTopic_name() {
        return topic_name;
    }

    public void setTopic_name(String topic_name) {
        this.topic_name = topic_name;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLaunch_url() {
        return launch_url;
    }

    public void setLaunch_url(String launch_url) {
        this.launch_url = launch_url;
    }
}
