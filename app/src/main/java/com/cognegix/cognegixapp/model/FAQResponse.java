package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FAQResponse {

    @SerializedName("status")
    private String status;//": true
    @SerializedName("faqs")
    private List<FAQModel> faqModelList;
    @SerializedName("message")
    private String message;//": "We have mailed you an OTP. Please check."


    @SerializedName("help_desk_phone")
    private String help_desk_phone;//": "9167572929"


    public String getHelp_desk_phone() {
        return help_desk_phone;
    }

    public void setHelp_desk_phone(String help_desk_phone) {
        this.help_desk_phone = help_desk_phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<FAQModel> getFaqModelList() {
        return faqModelList;
    }

    public void setFaqModelList(List<FAQModel> faqModelList) {
        this.faqModelList = faqModelList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
