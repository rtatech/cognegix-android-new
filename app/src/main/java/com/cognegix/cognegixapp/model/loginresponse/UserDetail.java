package com.cognegix.cognegixapp.model.loginresponse;

import com.google.gson.annotations.SerializedName;

public class UserDetail {
    @SerializedName("id")
    private String id;//": 3,
    @SerializedName("first_name")
    private String first_name;//": "Sabera",
    @SerializedName("last_name")
    private String last_name;//": "Thyagrajan",
    @SerializedName("username")
    private String username;//": "akshayD",
    @SerializedName("email")
    private String email;//": "tagsab2000@gmail.com",
    @SerializedName("personal_email")
    private String personal_email;//": "akshaydeshmukh@gmail.com",
    @SerializedName("is_active")
    private String is_active;//": 1,
    @SerializedName("party_id")
    private String party_id;//": 3,
    @SerializedName("api_token")
    private String api_token;//": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjI5MjRkZjhkMWYzMGJjZjgwMGE0YTUyM2MxZjBjYmM0NTdkOTlkMmQwZjQ2Y2IyYjUzZmFkNGMzZDEyMWJhN2UyY2FiOGQ5NDQxODBiOGM0In0.eyJhdWQiOiIxIiwianRpIjoiMjkyNGRmOGQxZjMwYmNmODAwYTRhNTIzYzFmMGNiYzQ1N2Q5OWQyZDBmNDZjYjJiNTNmYWQ0YzNkMTIxYmE3ZTJjYWI4ZDk0NDE4MGI4YzQiLCJpYXQiOjE1MjA5MzQxMDYsIm5iZiI6MTUyMDkzNDEwNiwiZXhwIjoxNTUyNDcwMTA2LCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.E0h2h17dbd0--mE8A0yXIw2SB4-YFKpW7_qqlfVwphHTKVEoG8NtSQGFoqk27nCPT9uloZC7KPB5ar-u-EDhrREHHJL4icQ3bhEj9lEcgEzO6alVFnn3kF4fAGGUHSNVmPDojK9hTJiyN3sGDpP3qTudvWrqa24AEPitkhI6VfqnfPKWSgDwZodZAUvotUTzUvEN0XWudDMMIjD-f5AMR3MMTt46ij8jEq7QwBiiE4fZYT4Z_T-T0O92PnpeQfjKUZ4JSYx7e7wR3D7FzATNwRxkxoJznyIMJlN2-SyMMbEJdhbN5FUNu8Z9CiwMVtIgUAFNX7baID5_3zX_NVvWghZmaqjtGFZFRWeRNGryFP0Ar040mLwSHMxf621o_7hHTJlnqGDz3yL_mhWmVAIVkcJo7mjV6Ra9TkTYjCowvOBC1K3ZRxJ-_aoPD75Io-Kzly8XsAusLdA8T3FIFgCe7_Eya41tfLek00ZvVLcH-KT7taNJO_O0y_0-K6vNOWiaJQe9eFLbdVvXz9dsIKC9sEUlrZZFxpLRssc4hmRqDv32xvXIY9Z57kGd7AAm3YZ35TYb-R6vpoC4j-J03wzFthPFLAOfTHqjkCajdzoK7QRTa82CCktSJezjy5VBg1FrleIPApBLupbzlWCf5D4_76ur4i7wjKD8tTOU_OszMmQ",
    @SerializedName("verification_otp")
    private String verification_otp;//": "132966",
    @SerializedName("created_at")
    private String created_at;//": "2018-01-27 11:52:56",
    @SerializedName("updated_at")
    private String updated_at;//": "2018-03-03 07:25:07",
    @SerializedName("deleted_at")
    private String deleted_at;//": null,
    @SerializedName("created_by")
    private String created_by;//": 1,
    @SerializedName("updated_by")
    private String updated_by;//": 1,
    @SerializedName("deleted_by")
    private String deleted_by;//": null

    @SerializedName("userId")
    private String userId;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPersonal_email() {
        return personal_email;
    }

    public void setPersonal_email(String personal_email) {
        this.personal_email = personal_email;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getParty_id() {
        return party_id;
    }

    public void setParty_id(String party_id) {
        this.party_id = party_id;
    }

    public String getApi_token() {
        return api_token;
    }

    public void setApi_token(String api_token) {
        this.api_token = api_token;
    }

    public String getVerification_otp() {
        return verification_otp;
    }

    public void setVerification_otp(String verification_otp) {
        this.verification_otp = verification_otp;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getDeleted_by() {
        return deleted_by;
    }

    public void setDeleted_by(String deleted_by) {
        this.deleted_by = deleted_by;
    }
}
