package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

public class ProgramTopicModel {

    @SerializedName("topic_name")
    private String topic_name;//": "Understanding a negotiation Situation",
    @SerializedName("topic_id")
    private String topic_id;//": 1

    public ProgramTopicModel(String topic_name) {
        this.topic_name = topic_name;
    }

    public String getTopic_name() {
        return topic_name;
    }

    public void setTopic_name(String topic_name) {
        this.topic_name = topic_name;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }
}
