package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MainResponseMessageBoard {

    @SerializedName("status")
    private String status;//": true,

    @SerializedName("board_messages")
    private List<MessageBoardModel> messageBoardModelList;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<MessageBoardModel> getMessageBoardModelList() {
        return messageBoardModelList;
    }

    public void setMessageBoardModelList(List<MessageBoardModel> messageBoardModelList) {
        this.messageBoardModelList = messageBoardModelList;
    }
}
