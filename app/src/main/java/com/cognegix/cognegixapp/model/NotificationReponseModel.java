package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationReponseModel {

    @SerializedName("status")
    private String status;//": true,
    @SerializedName("message")
    private String message;//": "No notification found."

    @SerializedName("notifications")
    private List<ModelNotification> modelNotificationList;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ModelNotification> getModelNotificationList() {
        return modelNotificationList;
    }

    public void setModelNotificationList(List<ModelNotification> modelNotificationList) {
        this.modelNotificationList = modelNotificationList;
    }
}
