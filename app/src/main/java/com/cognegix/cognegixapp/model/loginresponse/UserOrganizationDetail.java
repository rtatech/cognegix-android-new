package com.cognegix.cognegixapp.model.loginresponse;

import com.google.gson.annotations.SerializedName;

public class
UserOrganizationDetail {
    @SerializedName("id")
    private String id;//": 1,
    @SerializedName("name")
    private String name;//": "Cognegix LLP",
    @SerializedName("organization_code")
    private String organization_code;//": "CGX",
    @SerializedName("website_url")
    private String website_url;//": null,
    @SerializedName("logo")
    private String logo;//": "http://wwww.localhost.com",
    @SerializedName("original_logo")
    private String original_logo;//": null,
    @SerializedName("party_id")
    private String party_id;//": 1,
    @SerializedName("tenant_party_id")
    private String tenant_party_id;//": 1


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganization_code() {
        return organization_code;
    }

    public void setOrganization_code(String organization_code) {
        this.organization_code = organization_code;
    }

    public String getWebsite_url() {
        return website_url;
    }

    public void setWebsite_url(String website_url) {
        this.website_url = website_url;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getOriginal_logo() {
        return original_logo;
    }

    public void setOriginal_logo(String original_logo) {
        this.original_logo = original_logo;
    }

    public String getParty_id() {
        return party_id;
    }

    public void setParty_id(String party_id) {
        this.party_id = party_id;
    }

    public String getTenant_party_id() {
        return tenant_party_id;
    }

    public void setTenant_party_id(String tenant_party_id) {
        this.tenant_party_id = tenant_party_id;
    }
}
