package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

public class ContactResponseModel {
    @SerializedName("status")
    private String status;//": true,
    @SerializedName("help_desk_phone")
    private String help_desk_phone;//": "9167572929"

    @SerializedName("message")
    private String message;//": "User authenticated successfully.",

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHelp_desk_phone() {
        return help_desk_phone;
    }

    public void setHelp_desk_phone(String help_desk_phone) {
        this.help_desk_phone = help_desk_phone;
    }
}
