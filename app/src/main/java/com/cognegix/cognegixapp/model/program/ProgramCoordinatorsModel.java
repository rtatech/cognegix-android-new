package com.cognegix.cognegixapp.model.program;

import com.google.gson.annotations.SerializedName;

public class ProgramCoordinatorsModel {

    @SerializedName("name")
    private String name;//": "Sabera Thyagrajan",
    @SerializedName("email")
    private String email;//": "sabera@cognegix.in",
    @SerializedName("contact")
    private String contact;//": "9820132503"

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
