package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ThreadMessagesResponse {
    @SerializedName("status")
    private String status;//": true,
    @SerializedName("message")
    private String message;//": "User authenticated successfully.",

    @SerializedName("query_messages")
    private List<ThreadMessagesModel> threadMessagesModelList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ThreadMessagesModel> getThreadMessagesModelList() {
        return threadMessagesModelList;
    }

    public void setThreadMessagesModelList(List<ThreadMessagesModel> threadMessagesModelList) {
        this.threadMessagesModelList = threadMessagesModelList;
    }
}
