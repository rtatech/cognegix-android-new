package com.cognegix.cognegixapp.model.badges;

import com.google.gson.annotations.SerializedName;

public class BadgesLederResponse {
    @SerializedName("status")
    private String status;//": true,

    @SerializedName("badge_leaders_list")
    private BadgesLeaderModel badgesLeaderModel;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BadgesLeaderModel getBadgesLeaderModel() {
        return badgesLeaderModel;
    }

    public void setBadgesLeaderModel(BadgesLeaderModel badgesLeaderModel) {
        this.badgesLeaderModel = badgesLeaderModel;
    }
}
