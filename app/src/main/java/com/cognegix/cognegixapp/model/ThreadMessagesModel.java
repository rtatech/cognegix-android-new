package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

public class ThreadMessagesModel {
    @SerializedName("id")
    private String id;//": 55,
    @SerializedName("my_query_id")
    private String my_query_id;//": 20,
    @SerializedName("message")
    private String message;//": "abc",
    @SerializedName("is_from_sender")
    private String is_from_sender;//": 1,
    @SerializedName("is_read")
    private String is_read;//": 0,
    @SerializedName("created_at")
    private String created_at;//": "2018-06-28 23:15:08",
    @SerializedName("sender_name")
    private String sender_name;//": "Milllind Moravekar",
    @SerializedName("receiver_name")
    private String receiver_name;//": null,
    @SerializedName("formatted_created_at")
    private String formatted_created_at;//": "28th Jun 2018 11:15 PM"

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMy_query_id() {
        return my_query_id;
    }

    public void setMy_query_id(String my_query_id) {
        this.my_query_id = my_query_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIs_from_sender() {
        return is_from_sender;
    }

    public void setIs_from_sender(String is_from_sender) {
        this.is_from_sender = is_from_sender;
    }

    public String getIs_read() {
        return is_read;
    }

    public void setIs_read(String is_read) {
        this.is_read = is_read;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    public String getReceiver_name() {
        return receiver_name;
    }

    public void setReceiver_name(String receiver_name) {
        this.receiver_name = receiver_name;
    }

    public String getFormatted_created_at() {
        return formatted_created_at;
    }

    public void setFormatted_created_at(String formatted_created_at) {
        this.formatted_created_at = formatted_created_at;
    }
}
