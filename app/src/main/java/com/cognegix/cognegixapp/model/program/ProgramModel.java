package com.cognegix.cognegixapp.model.program;

import com.google.gson.annotations.SerializedName;

public class ProgramModel {

    @SerializedName("id")
    private String id;//": 1,
    @SerializedName("code")
    private String code;//": "TCS/Nego/001",
    @SerializedName("name")
    private String name;//": "Negotiation",
    @SerializedName("description")
    private String description;//": "This program will help the participants improve on their negotiation skill and enable them to efficiently apply various negotiation techniques in their work. ",
    @SerializedName("version")
    private String version;//": 1,
    @SerializedName("start_date")
    private String start_date;//": "2017-12-21 12:00:00",
    @SerializedName("end_date")
    private String end_date;//": "2017-12-26 12:00:00",
    @SerializedName("allow_self_enrol")
    private String allow_self_enrol;//": 0,
    @SerializedName("badges_enabled")
    private String badges_enabled;//": 1,
    @SerializedName("fastest_activity_completion_user_limit")
    private String fastest_activity_completion_user_limit;//": 0,
    @SerializedName("participant_distribution_badges")
    private String participant_distribution_badges;//": 0,
    @SerializedName("content_rating_badges")
    private String content_rating_badges;//": 0,
    @SerializedName("customer_party_id")
    private String customer_party_id;//": 6,
    @SerializedName("tenant_party_id")
    private String tenant_party_id;//": 1,
    private int color = -1;

    @SerializedName("photo")
    private String photo;//":"",;
    @SerializedName("photo_path")
    private String photo_path;//": null,
    @SerializedName("document_name")
    private String document_name;//": null,
    @SerializedName("document_path")
    private String document_path;//": null,

    @SerializedName("completion_percentage")
    private String completion_percentage;//":"",;

    @SerializedName("user_role")
    private String user_role;//": "Participant","user_role": "Participant",

    @SerializedName("remaining_distribution_badges")
    private String remaining_distribution_badges;

    @SerializedName("learning_tree")
    private LearningTree learningTree;

    @SerializedName("program_user_id")
    private String program_user_id;


    public ProgramModel() {
    }

    public ProgramModel(String name) {
        this.name = name;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getAllow_self_enrol() {
        return allow_self_enrol;
    }

    public void setAllow_self_enrol(String allow_self_enrol) {
        this.allow_self_enrol = allow_self_enrol;
    }

    public String getBadges_enabled() {
        return badges_enabled;
    }

    public void setBadges_enabled(String badges_enabled) {
        this.badges_enabled = badges_enabled;
    }

    public String getFastest_activity_completion_user_limit() {
        return fastest_activity_completion_user_limit;
    }

    public void setFastest_activity_completion_user_limit(String fastest_activity_completion_user_limit) {
        this.fastest_activity_completion_user_limit = fastest_activity_completion_user_limit;
    }

    public String getParticipant_distribution_badges() {
        return participant_distribution_badges;
    }

    public void setParticipant_distribution_badges(String participant_distribution_badges) {
        this.participant_distribution_badges = participant_distribution_badges;
    }

    public String getContent_rating_badges() {
        return content_rating_badges;
    }

    public void setContent_rating_badges(String content_rating_badges) {
        this.content_rating_badges = content_rating_badges;
    }

    public String getCustomer_party_id() {
        return customer_party_id;
    }

    public void setCustomer_party_id(String customer_party_id) {
        this.customer_party_id = customer_party_id;
    }

    public String getTenant_party_id() {
        return tenant_party_id;
    }

    public void setTenant_party_id(String tenant_party_id) {
        this.tenant_party_id = tenant_party_id;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCompletion_percentage() {
        return completion_percentage;
    }

    public void setCompletion_percentage(String completion_percentage) {
        this.completion_percentage = completion_percentage;
    }

    public LearningTree getLearningTree() {
        return learningTree;
    }

    public void setLearningTree(LearningTree learningTree) {
        this.learningTree = learningTree;
    }

    public String getPhoto_path() {
        return photo_path;
    }

    public void setPhoto_path(String photo_path) {
        this.photo_path = photo_path;
    }

    public String getDocument_name() {
        return document_name;
    }

    public void setDocument_name(String document_name) {
        this.document_name = document_name;
    }

    public String getDocument_path() {
        return document_path;
    }

    public void setDocument_path(String document_path) {
        this.document_path = document_path;
    }


    public String getUser_role() {
        return user_role;
    }

    public void setUser_role(String user_role) {
        this.user_role = user_role;
    }

    public String getRemaining_distribution_badges() {
        return remaining_distribution_badges;
    }

    public void setRemaining_distribution_badges(String remaining_distribution_badges) {
        this.remaining_distribution_badges = remaining_distribution_badges;
    }

    public String getProgram_user_id() {
        return program_user_id;
    }

    public void setProgram_user_id(String program_user_id) {
        this.program_user_id = program_user_id;
    }
}
