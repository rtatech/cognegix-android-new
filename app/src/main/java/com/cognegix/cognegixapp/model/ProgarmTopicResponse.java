package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProgarmTopicResponse {
    @SerializedName("status")
    private String status;//": true,

    @SerializedName("program_topics")
    private List<ProgramTopicModel> programTopicModelList;

    @SerializedName("person_to_ask")
    private List<ProgramPersonToAskModel> programPersonToAskModelList;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ProgramTopicModel> getProgramTopicModelList() {
        return programTopicModelList;
    }

    public void setProgramTopicModelList(List<ProgramTopicModel> programTopicModelList) {
        this.programTopicModelList = programTopicModelList;
    }

    public List<ProgramPersonToAskModel> getProgramPersonToAskModelList() {
        return programPersonToAskModelList;
    }

    public void setProgramPersonToAskModelList(List<ProgramPersonToAskModel> programPersonToAskModelList) {
        this.programPersonToAskModelList = programPersonToAskModelList;
    }
}
