package com.cognegix.cognegixapp.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ForumInterModel implements Serializable {
    @SerializedName("id")
    private String id;//": 1,
    @SerializedName("forum_id")
    private String forum_id;//": 1,
    @SerializedName("subject")
    private String subject;//": "First Thread",
    @SerializedName("description")
    private String description;//": "",
    @SerializedName("created_at")
    private String created_at;//": "2018-06-07 05:16:31",
    @SerializedName("updated_at")
    private String updated_at;//": "2018-06-07 05:16:31",
    @SerializedName("deleted_at")
    private String deleted_at;//": null,
    @SerializedName("created_by")
    private String created_by;//": "Manisha Tambe",
    @SerializedName("updated_by")
    private String updated_by;//": null,
    @SerializedName("deleted_by")
    private String deleted_by;//": null
    @SerializedName("created_by_photo")
    private String created_by_photo;//": "http://cogvc.testursites.info/cgx-front/public/storage/tenants/1/user/3/profiles/31528544358.jpeg",
    @SerializedName("formatted_created_at")
    private String formatted_created_at;//": "2 days ago"

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getForum_id() {
        return forum_id;
    }

    public void setForum_id(String forum_id) {
        this.forum_id = forum_id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getDeleted_by() {
        return deleted_by;
    }

    public void setDeleted_by(String deleted_by) {
        this.deleted_by = deleted_by;
    }

    public String getCreated_by_photo() {
        return created_by_photo;
    }

    public void setCreated_by_photo(String created_by_photo) {
        this.created_by_photo = created_by_photo;
    }

    public String getFormatted_created_at() {
        return formatted_created_at;
    }

    public void setFormatted_created_at(String formatted_created_at) {
        this.formatted_created_at = formatted_created_at;
    }
}
