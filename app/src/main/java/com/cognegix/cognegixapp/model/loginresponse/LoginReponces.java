package com.cognegix.cognegixapp.model.loginresponse;


import com.google.gson.annotations.SerializedName;

public class LoginReponces {

    @SerializedName("status")
    private String status;//": true,
    @SerializedName("message")
    private String message;//": "User authenticated successfully.",

    @SerializedName("user_hash")
    private String user_hash;//": "B0nYzweP7N",

    @SerializedName("user_detail")
    private UserDetail userDetail;

    @SerializedName("user_profile_detail")
    private UserProfileDetail userProfileDetail;
    @SerializedName("user_organization_detail")
    private UserOrganizationDetail userOrganizationDetail;


    public LoginReponces(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserDetail getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetail userDetail) {
        this.userDetail = userDetail;
    }

    public UserProfileDetail getUserProfileDetail() {
        return userProfileDetail;
    }

    public void setUserProfileDetail(UserProfileDetail userProfileDetail) {
        this.userProfileDetail = userProfileDetail;
    }

    public UserOrganizationDetail getUserOrganizationDetail() {
        return userOrganizationDetail;
    }

    public void setUserOrganizationDetail(UserOrganizationDetail userOrganizationDetail) {
        this.userOrganizationDetail = userOrganizationDetail;
    }

    public String getUser_hash() {
        return user_hash;
    }

    public void setUser_hash(String user_hash) {
        this.user_hash = user_hash;
    }
}
