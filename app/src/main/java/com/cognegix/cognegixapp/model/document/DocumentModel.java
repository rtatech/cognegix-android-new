package com.cognegix.cognegixapp.model.document;

import com.google.gson.annotations.SerializedName;

public class DocumentModel {


    @SerializedName("id")
    private String id;//": 2,
    @SerializedName("title")
    private String title;//": "First Document",
    @SerializedName("note")
    private String note;//": null,
    @SerializedName("program_id")
    private String program_id;//": 16,
    @SerializedName("topic_id")
    private String topic_id;//": null,
    @SerializedName("document_type")
    private String document_type;//": "pdf",
    @SerializedName("document_path")
    private String document_path;//": "http://182.59.4.193:8010/storage/program/16/documents/default.pdf"

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getProgram_id() {
        return program_id;
    }

    public void setProgram_id(String program_id) {
        this.program_id = program_id;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getDocument_type() {
        return document_type;
    }

    public void setDocument_type(String document_type) {
        this.document_type = document_type;
    }

    public String getDocument_path() {
        return document_path;
    }

    public void setDocument_path(String document_path) {
        this.document_path = document_path;
    }
}
