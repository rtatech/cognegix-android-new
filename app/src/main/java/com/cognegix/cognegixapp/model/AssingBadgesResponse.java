package com.cognegix.cognegixapp.model;

import com.google.gson.annotations.SerializedName;

public class AssingBadgesResponse {
    @SerializedName("status")
    private String status;//": false,
    @SerializedName("message")
    private String message;//": "Something went wrong. Please try again.",
    @SerializedName("error")
    private String error;//": "Insufficient distribution badges!! Cannot give badges to participant! "
    @SerializedName("remaining_distribution_badges")
    private String remaining_distribution_badges;//": 3

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getRemaining_distribution_badges() {
        return remaining_distribution_badges;
    }

    public void setRemaining_distribution_badges(String remaining_distribution_badges) {
        this.remaining_distribution_badges = remaining_distribution_badges;
    }
}
