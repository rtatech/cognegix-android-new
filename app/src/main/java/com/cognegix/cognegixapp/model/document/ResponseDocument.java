package com.cognegix.cognegixapp.model.document;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseDocument {
    @SerializedName("status")
    private boolean status;//":false,
    @SerializedName("message")
    private String message;//":"No program found."
    @SerializedName("documents")
    private List<DocumentModel> documents;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DocumentModel> getDocuments() {
        return documents;
    }

    public void setDocuments(List<DocumentModel> documents) {
        this.documents = documents;
    }
}
