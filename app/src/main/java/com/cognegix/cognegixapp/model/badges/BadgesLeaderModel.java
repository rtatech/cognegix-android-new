package com.cognegix.cognegixapp.model.badges;


import com.google.gson.annotations.SerializedName;

public class BadgesLeaderModel {


    @SerializedName("fastestActivityBadge")
    private FastestActivityBadge fastestActivityBadge;
    @SerializedName("contentRatingBadgeCount")
    private ContentRatingBadgeCount contentRatingBadgeCount;

    @SerializedName("quizCompletionBadgeCount")
    private QuizCompletionBadgeCount quizCompletionBadgeCount;

    @SerializedName("facilitatorBadgeCount")
    private FacilitatorBadgeCount facilitatorBadgeCount;

    @SerializedName("participantBadgeCount")
    private ParticipantBadgeCount participantBadgeCount;


    public FastestActivityBadge getFastestActivityBadge() {
        return fastestActivityBadge;
    }

    public void setFastestActivityBadge(FastestActivityBadge fastestActivityBadge) {
        this.fastestActivityBadge = fastestActivityBadge;
    }

    public ContentRatingBadgeCount getContentRatingBadgeCount() {
        return contentRatingBadgeCount;
    }

    public void setContentRatingBadgeCount(ContentRatingBadgeCount contentRatingBadgeCount) {
        this.contentRatingBadgeCount = contentRatingBadgeCount;
    }

    public QuizCompletionBadgeCount getQuizCompletionBadgeCount() {
        return quizCompletionBadgeCount;
    }

    public void setQuizCompletionBadgeCount(QuizCompletionBadgeCount quizCompletionBadgeCount) {
        this.quizCompletionBadgeCount = quizCompletionBadgeCount;
    }

    public FacilitatorBadgeCount getFacilitatorBadgeCount() {
        return facilitatorBadgeCount;
    }

    public void setFacilitatorBadgeCount(FacilitatorBadgeCount facilitatorBadgeCount) {
        this.facilitatorBadgeCount = facilitatorBadgeCount;
    }

    public ParticipantBadgeCount getParticipantBadgeCount() {
        return participantBadgeCount;
    }

    public void setParticipantBadgeCount(ParticipantBadgeCount participantBadgeCount) {
        this.participantBadgeCount = participantBadgeCount;
    }














}


