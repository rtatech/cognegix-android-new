package com.cognegix.cognegixapp.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ForumInterResponse {
    @SerializedName("status")
    private String status;//": true,

    @SerializedName("thread_list")
    private List<ForumInterModel> forumInterModelList;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ForumInterModel> getForumInterModelList() {
        return forumInterModelList;
    }

    public void setForumInterModelList(List<ForumInterModel> forumInterModelList) {
        this.forumInterModelList = forumInterModelList;
    }
}
