package com.cognegix.cognegixapp.model;


public class EventModel {
    String date;


    public EventModel(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

