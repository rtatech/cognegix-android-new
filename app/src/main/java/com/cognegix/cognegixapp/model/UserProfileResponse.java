package com.cognegix.cognegixapp.model;

import com.cognegix.cognegixapp.model.loginresponse.UserOrganizationDetail;
import com.cognegix.cognegixapp.model.loginresponse.UserProfileDetail;
import com.google.gson.annotations.SerializedName;

public class UserProfileResponse {
    @SerializedName("status")
    private String status;//": true,
    @SerializedName("user_profile_detail")
    private UserProfileDetail userProfileDetail;
    @SerializedName("user_organization_detail")
    private UserOrganizationDetail userOrganizationDetail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserProfileDetail getUserProfileDetail() {
        return userProfileDetail;
    }

    public void setUserProfileDetail(UserProfileDetail userProfileDetail) {
        this.userProfileDetail = userProfileDetail;
    }

    public UserOrganizationDetail getUserOrganizationDetail() {
        return userOrganizationDetail;
    }

    public void setUserOrganizationDetail(UserOrganizationDetail userOrganizationDetail) {
        this.userOrganizationDetail = userOrganizationDetail;
    }
}
