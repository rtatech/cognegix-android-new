package com.cognegix.cognegixapp.facilitor.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.facilitor.activity.ActivityNotificationDetails;
import com.cognegix.cognegixapp.facilitor.activity.ActivityVideoCall;
import com.cognegix.cognegixapp.facilitor.activity.HomeActivity;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterNotification;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.interfaces.OnClickNotification;
import com.cognegix.cognegixapp.model.ForgotResponse;
import com.cognegix.cognegixapp.model.ModelNotification;
import com.cognegix.cognegixapp.model.NotificationReponseModel;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentNotification extends Fragment implements OnClickNotification {
    private static final String TAG = FragmentNotification.class.getSimpleName();
    View view;
    @BindView(R.id.rvData)
    RecyclerView rvData;


    //Todo create variable
    AdapterNotification adapterNotification;
    ApiInterface apiInterface;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_notification, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        Log.d(TAG, "init: ");
        //Todo all intiazaltion
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayout.VERTICAL, false);
        rvData.setLayoutManager(linearLayoutManager);
        adapterNotification = new AdapterNotification(getContext());
        adapterNotification.setItemOnClickListener(FragmentNotification.this);
        rvData.setAdapter(adapterNotification);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        // loadData();

    }


    @Override
    public void onStart() {
        super.onStart();

        loadData();
    }

    //Todo load notification data
    private void loadData() {

        Log.d(TAG, "loadData: ");
        adapterNotification.clearList();
        ((HomeActivity) getActivity()).showDialog(getActivity());

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(getContext()).getUserDetail().getApi_token();


        Call<NotificationReponseModel> responseCall = apiInterface.doNotiifcationList(accept, authorization,
                "all");
        responseCall.enqueue(new Callback<NotificationReponseModel>() {
            @Override
            public void onResponse(@NonNull Call<NotificationReponseModel> call, @NonNull Response<NotificationReponseModel> response) {
                BaseActivity.hideDialog();//  Log.d(TAG, "onResponse:     " + response.body().getStatus() + "===kffkdkb=====" + response.body().getMessage());
             //   Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));

                if (response.code() == 200) {
                    if (response.body().getStatus().equals("true")) {


                        adapterNotification.setList(response.body().getModelNotificationList());


                    }

                }
            }


            @Override
            public void onFailure(@NonNull Call<NotificationReponseModel> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                BaseActivity.hideDialog();
            }
        });


    }


    @Override
    public void onClickView(ModelNotification modelNotification) {
        if (modelNotification.getIs_video_call().equalsIgnoreCase("1")) {
           /* Intent intent = new Intent(getActivity(), ActivityVideoCall.class);
            intent.putExtra(ActivityVideoCall.KEY, modelNotificationList.get(position).getVideo_call_url());
            intent.putExtra(ActivityVideoCall.NOTIFICATION, modelNotificationList.get(position).getId());
            startActivity(intent);*/
            doSetNotificationISRead(modelNotification.getId());
            String url = modelNotification.getVideo_call_url();
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        } else {
            // Log.d(TAG, "setOnItemClickListener: nvlnx xc=== " + position);
            Intent intent = new Intent(getActivity(), ActivityNotificationDetails.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(ActivityNotificationDetails.KEY_ID, modelNotification);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }


    private void doSetNotificationISRead(String id) {
        // showDialog(this);

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(getContext()).getUserDetail().getApi_token();

        Call<ForgotResponse> responseCall = apiInterface.doSetNotificationRead(
                accept,
                authorization,
                id);
        responseCall.enqueue(new Callback<ForgotResponse>() {

            @Override
            public void onResponse(@NonNull Call<ForgotResponse> call, @NonNull Response<ForgotResponse> response) {
                Log.d(TAG, "onResponse:     " + response.body().getStatus() + "===kffkdkb=====" + response.body().getMessage());
                // hideDialog();


            }

            @Override
            public void onFailure(@NonNull Call<ForgotResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                //   hideDialog();
            }
        });

    }

}
