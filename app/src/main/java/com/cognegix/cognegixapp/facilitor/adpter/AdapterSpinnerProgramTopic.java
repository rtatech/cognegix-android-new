package com.cognegix.cognegixapp.facilitor.adpter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.model.ProgramTopicModel;
import com.cognegix.cognegixapp.model.SpinnerModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AdapterSpinnerProgramTopic extends BaseAdapter {
    private Context context;
    private List<ProgramTopicModel> spinnerModels;

    public AdapterSpinnerProgramTopic(Context context, List<ProgramTopicModel> spinnerModels) {
        this.context = context;
        this.spinnerModels = spinnerModels;
    }

    @Override
    public int getCount() {
        return spinnerModels.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_view_spinner, viewGroup, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        if (i == 0) {
            viewHolder.tvTitle.setGravity(Gravity.CENTER);
            viewHolder.tvTitle.setTextColor(context.getResources().getColor(R.color.colorBlue));
        } else {
            viewHolder.tvTitle.setGravity(Gravity.LEFT);
            viewHolder.tvTitle.setTextColor(context.getResources().getColor(R.color.black));

        }

        viewHolder.tvTitle.setText(spinnerModels.get(i).getTopic_name());

        return view;
    }


    class ViewHolder {
        @BindView(R.id.tvTitle)
        CustomTextView tvTitle;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

    }
}
