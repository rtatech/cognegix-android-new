package com.cognegix.cognegixapp.facilitor.adpter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.model.ForumInterModel;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterForumInter extends RecyclerView.Adapter<AdapterForumInter.ViewHolder> {
    private Context context;
    private List<ForumInterModel> forumInterModelList;

    private ItemOnClickListener itemOnClickListener;


    public AdapterForumInter(Context context, List<ForumInterModel> forumInterModelList) {
        this.context = context;
        this.forumInterModelList = forumInterModelList;
    }

    public void SetItemClickListener(ItemOnClickListener itemOnClickListener) {
        this.itemOnClickListener = itemOnClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_forum_inter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        holder.tvName.setText(forumInterModelList.get(position).getCreated_by());
        holder.tvTime.setText(forumInterModelList.get(position).getFormatted_created_at());
        holder.tvDescripation.setText(forumInterModelList.get(position).getDescription());
        holder.tvSubject.setText(forumInterModelList.get(position).getSubject());

        /*if (!forumInterModelList.get(position).getCreated_by_photo().equals("")) {
            Glide.with(context).load(forumInterModelList.get(position).getCreated_by_photo()).into(holder.ivProfile);
        }*/
        Glide.with(context).load(forumInterModelList.get(position).getCreated_by_photo()).into(holder.ivProfile);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemOnClickListener.setOnItemClickListener(holder.getAdapterPosition());
            }
        });

    }

    @Override
    public int getItemCount() {
        return forumInterModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvName)
        CustomTextView tvName;
        @BindView(R.id.tvTime)
        CustomTextView tvTime;
        @BindView(R.id.tvSubject)
        CustomTextView tvSubject;
        @BindView(R.id.tvDescripation)
        CustomTextView tvDescripation;

        @BindView(R.id.ivProfile)
        CircleImageView ivProfile;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
