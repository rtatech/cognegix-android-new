package com.cognegix.cognegixapp.facilitor.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterMyQuery;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterSpinnerAddQueary;
import com.cognegix.cognegixapp.model.MyQueryThreadModel;
import com.cognegix.cognegixapp.model.MyQueryThreadResponse;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.model.program.ProgramModel;
import com.cognegix.cognegixapp.myquery.MyQueryDetailsActivity;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyQueryActivity extends BaseActivity implements View.OnClickListener, ItemOnClickListener, AdapterView.OnItemSelectedListener {
    //todo create static verible
    private static final String TAG = MyQueryActivity.class.getSimpleName();
    public static final String KEY = "keyMyQuery";
    //todo create referance
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.rvData)
    RecyclerView rvData;
    @BindView(R.id.sFilter)
    AppCompatSpinner sFilter;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.tvNotificationCount)
    CustomTextView tvNotificationCount;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    @BindView(R.id.fbAdd)
    FloatingActionButton fbAdd;

    @BindView(R.id.rlSetting)
    RelativeLayout rlSetting;
    @BindView(R.id.rlHome)
    RelativeLayout rlHome;
    @BindView(R.id.rvSearch)
    RelativeLayout rvSearch;
    @BindView(R.id.rlHelpDeskBottom)
    RelativeLayout rlHelpDeskBottom;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;


    AdapterMyQuery adapterMyQuery;
    ApiInterface apiInterface;
    List<MyQueryThreadModel> myQueryThreadModelList;
    String programId;
    boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int page = 1;
    LinearLayoutManager linearLayoutManager;
    AdapterSpinnerAddQueary adapterSpinnerProgram;
    List<ProgramModel> modelProgramList;
    String programName;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_document);

        init();
    }

    //Todo init all veribale
    private void init() {
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        tvToolbarTitle.setText(getString(R.string.myQuery));
        fbAdd.setOnClickListener(this);
        rlSetting.setOnClickListener(this);
        rlHome.setOnClickListener(this);
        rvSearch.setOnClickListener(this);
        rlHelpDeskBottom.setOnClickListener(this);
        rlNotification.setOnClickListener(this);

        linearLayoutManager = new LinearLayoutManager(this, LinearLayout.VERTICAL, false);
        rvData.setLayoutManager(linearLayoutManager);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        programId = getIntent().getStringExtra(KEY);

        modelProgramList = new ArrayList<>();
        adapterSpinnerProgram = new AdapterSpinnerAddQueary(this, modelProgramList);
        sFilter.setAdapter(adapterSpinnerProgram);
        myQueryThreadModelList = new ArrayList<>();
        adapterMyQuery = new AdapterMyQuery(this, myQueryThreadModelList);
        adapterMyQuery.setOnItemListners(this);
        rvData.setAdapter(adapterMyQuery);
        sFilter.setOnItemSelectedListener(this);


        Log.d(TAG, "init: programId " + programId);
        loadSpinnerData();
        // doLoadThreadList(page, false);
        doSetScrollistner();
    }

    private void doSetScrollistner() {
        rvData.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                Log.d(TAG, "onScrolled:   dx == " + dx);
                Log.d(TAG, "onScrolled:   dy == " + dy);

                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            Log.v(TAG, "Last Item Wow !");
                            //Do pagination.. i.e. fetch new data
                            page = page + 1;
                            doLoadThreadList(page, false);
                        }
                    }
                }

            }
        });


    }


    //Todo call thread list api 
    private void doLoadThreadList(int page, boolean isSpinner) {
        Log.d(TAG, "doLoadThreadList:  programIdprogramId+ " + programId);
        if (isSpinner) {
            myQueryThreadModelList.clear();
            adapterMyQuery.notifyDataSetChanged();
        }

        showDialog(this);
        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();
        Call<MyQueryThreadResponse> responseCall = apiInterface.doFetchQueryThread(accept,
                authorization,
                programId,
                String.valueOf(page),
                SharePref.getProgramUserId(this));

        Log.d(TAG, "params: " + new Gson().toJson(responseCall.request().body()));

        responseCall.enqueue(new Callback<MyQueryThreadResponse>() {

            @Override
            public void onResponse(@NonNull Call<MyQueryThreadResponse> call, @NonNull Response<MyQueryThreadResponse> response) {
                //  Log.d(TAG, "onResponse:    +++> " + response);
                hideDialog();
                if (response.code() == 200) {
                    if (response.body().getStatus().equals("true")) {
                        Log.d(TAG, "onResponse: " + response.body().getStatus());
                        Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));
                        myQueryThreadModelList.addAll(response.body().getMyQueryThreadModelList());
                        adapterMyQuery.notifyDataSetChanged();
                    } else {
                        showAlertDialog(MyQueryActivity.this, "", response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MyQueryThreadResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        Glide.with(this)
                .load(SharePref.getLoginResponse(this).getUserProfileDetail().getPhoto()) // Uri of the picture
                .into(ivUserProfile);
        if (SharePref.getNotificationCount(this) != null) {
            tvNotificationCount.setText(SharePref.getNotificationCount(this));
        }
    }

    //Todo load data for spinner
    private void loadSpinnerData() {

        modelProgramList.add(new ProgramModel("Select"));
        modelProgramList.addAll(SharePref.getProgramResponse(this).getProgram_list());
        adapterSpinnerProgram.notifyDataSetChanged();

        if (modelProgramList.size() > 0) {
            //  selectValue(programId);
            selectDropDownValue(modelProgramList, programId);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;
            case R.id.fbAdd:
                Intent intent = new Intent(this, ActivityAddMyQuery.class);
                intent.putExtra(ActivityAddMyQuery.KEY_PROGRAM_ID, programId);
                startActivity(intent);
                finish();
                break;
            case R.id.rlSetting:
                //Todo load my query fragment
                startActivity(new Intent(this, SettingActivity.class));
                break;
            case R.id.rlHome:
                // startActivity(new Intent(this, HomeActivity.class));
                finish();
                break;
            case R.id.rvSearch:
                startActivity(new Intent(this, ActivitySearch.class));
                break;
            case R.id.rlHelpDeskBottom:
                startActivity(new Intent(this, ActivityHelpDesk.class));
                break;

            case R.id.rlNotification:
                startActivity(new Intent(this, ActivityNotification.class));
                break;

        }

    }

    @Override
    public void setOnItemClickListener(int position) {
        Intent intent = new Intent(this, MyQueryDetailsActivity.class);
        intent.putExtra(MyQueryDetailsActivity.KEYThread, myQueryThreadModelList.get(position).getQuery_thread_id());
        intent.putExtra(MyQueryDetailsActivity.KEY_PROGRAM, programName);

        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


        if (i != 0) {
            programId = modelProgramList.get(i).getId();
            programName = modelProgramList.get(i).getName();
            SharePref.setProgramUserId(this, modelProgramList.get(i).getProgram_user_id());
            Log.d(TAG, "onItemSelected: " + programId);
            page = 1;
            adapterMyQuery.setRole(modelProgramList.get(i).getUser_role());
            if (modelProgramList.get(i).getUser_role().equalsIgnoreCase("Participant")) {
                fbAdd.setVisibility(View.VISIBLE);
            } else {
                fbAdd.setVisibility(View.GONE);
            }
            //myQueryThreadModelList.clear();
            doLoadThreadList(page, true);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void selectedDropDownValue(int position) {
        super.selectedDropDownValue(position);
        sFilter.setSelection(position);
    }

}
