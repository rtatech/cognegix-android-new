package com.cognegix.cognegixapp.facilitor.adpter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.model.ModelProgram;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.model.program.ProgramModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterProgramPager extends PagerAdapter {
    private Context context;
    private List<ProgramModel> modelProgramList;
    private ItemOnClickListener itemOnClickListener;

    public AdapterProgramPager(Context context, List<ProgramModel> modelProgramList) {
        this.context = context;
        this.modelProgramList = modelProgramList;
    }

    public void setOnClickButtonShare(ItemOnClickListener itemOnClickListener) {
        this.itemOnClickListener = itemOnClickListener;
    }

    @Override
    public int getCount() {
        return modelProgramList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View) object);


    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ViewHolder viewHolder;
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_program, container, false);
        viewHolder = new ViewHolder(view);
        ((ViewPager) container).addView(view);

        viewHolder.bLaunch.setTag(position);
        viewHolder.tvProgramTitle.setText(modelProgramList.get(position).getName());
        //  viewHolder.tvProgramDesc.setText(modelProgramList.get(position).getDescription());

        String desc = modelProgramList.get(position).getDescription();
        ///  viewHolder.ivProgramImage.setImageResource(modelProgramList.get(position).getImagePath());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            viewHolder.tvProgramDesc.setText(Html.fromHtml(desc, Html.FROM_HTML_MODE_COMPACT));
        } else {
            viewHolder.tvProgramDesc.setText(Html.fromHtml(desc));
        }

        if (modelProgramList.get(position).getUser_role().equalsIgnoreCase("Participant")) {

            String per = modelProgramList.get(position).getCompletion_percentage() + "%";
            viewHolder.tvParcentage.setText(per);

            try {

                String progress = modelProgramList.get(position).getCompletion_percentage();
                float f1 = Float.parseFloat(progress);
                viewHolder.progress.setProgress((int) f1);

            } catch (NumberFormatException ex) { // handle your exception
                Log.e("Adapter", "instantiateItem: " + ex);
            }
        } else {
            viewHolder.progress.setVisibility(View.GONE);
            viewHolder.tvParcentage.setVisibility(View.GONE);
        }

        if (modelProgramList.get(position).getPhoto_path() != null && !modelProgramList.get(position).getPhoto_path().equals("")) {
            String path = getPath(String.valueOf(ApiClient.getClient().baseUrl())) + modelProgramList.get(position).getPhoto_path();
            Glide.with(context).load(path).into(viewHolder.ivProgramImage);
        }


        return view;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }


    class ViewHolder {

        @BindView(R.id.ivProgramImage)
        ImageView ivProgramImage;
        @BindView(R.id.bLaunch)
        CustomButtonView bLaunch;
        @BindView(R.id.tvProgramTitle)
        CustomTextView tvProgramTitle;
        @BindView(R.id.tvProgramDesc)
        CustomTextView tvProgramDesc;
        @BindView(R.id.tvParcentage)
        CustomTextView tvParcentage;

        //        @BindView(R.id.tvMaxPer)
//        CustomTextView tvMaxPer;
        @BindView(R.id.progressBar)
        ProgressBar progress;

        @BindView(R.id.cardMain)
        CardView cardMain;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);


            // cardMain.getLayoutParams().width = cardMain.getLayoutParams().width / 2;
            bLaunch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int pos = (int) view.getTag();
                    itemOnClickListener.setOnItemClickListener(pos);
                }
            });

        }


    }

    private Bitmap encodeImage(String base64) {
        final String pureBase64Encoded = base64.substring(base64.indexOf(",") + 1);
        byte[] decodedString = Base64.decode(pureBase64Encoded, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }


    public String getPath(String str) {
        str = str.substring(0, str.length() - 5);
        return str;
    }

}
