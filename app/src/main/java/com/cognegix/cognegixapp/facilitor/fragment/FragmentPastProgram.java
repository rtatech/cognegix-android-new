package com.cognegix.cognegixapp.facilitor.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.facilitor.activity.ActivityProgramDetail;
import com.cognegix.cognegixapp.facilitor.activity.HomeActivity;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterPastProgram;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterSpinner;
import com.cognegix.cognegixapp.model.SpinnerModel;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.model.program.ProgramModel;
import com.cognegix.cognegixapp.model.program.ProgramResponse;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.constraint.Constraints.TAG;

public class FragmentPastProgram extends Fragment implements ItemOnClickListener {
    private static String TAG = FragmentPastProgram.class.getSimpleName();
    View view;
    @BindView(R.id.rvData)
    RecyclerView rvData;
    AdapterPastProgram adapterPastProgram;
    List<ProgramModel> programModels;
    ApiInterface apiInterface;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_past_program, container, false);
        init();
        return view;
    }

    //Todo init all data and componant
    private void init() {
        ButterKnife.bind(this, view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayout.VERTICAL, false);
        rvData.setLayoutManager(linearLayoutManager);
        programModels = new ArrayList<>();
        adapterPastProgram = new AdapterPastProgram(getContext(), programModels);
        adapterPastProgram.setOnClickButtonShare(this);
        rvData.setAdapter(adapterPastProgram);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        loadData();
    }

    @Override
    public void setOnItemClickListener(int position) {
        SharePref.setProgramModel(getActivity(), programModels.get(position));
        startActivity(new Intent(getContext(), ActivityProgramDetail.class));
    }


    //Todo set past program data
    private void loadData() {
        ((HomeActivity) getActivity()).showDialog(getActivity());

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(getActivity()).getUserDetail().getApi_token();

        Call<ProgramResponse> responseCall = apiInterface.getPastProgram(accept, authorization);
        responseCall.enqueue(new Callback<ProgramResponse>() {
            @Override
            public void onResponse(@NonNull Call<ProgramResponse> call, @NonNull Response<ProgramResponse> response) {
                //  Log.d(TAG, "onResponse: " + response.headers());
              //  Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));
                BaseActivity.hideDialog();
              //  Log.d(TAG, "onResponse: " + response);

                if (response.code() == 200) {

                    if (response.body().getStatus().equals("true")) {
                        programModels.addAll(response.body().getProgram_list());
                        adapterPastProgram.notifyDataSetChanged();
                        SharePref.setNotificationCount(getActivity(), response.body().getUnread_notification_count());
                     //   SharePref.setProgramResponse(getActivity(), response.body().);
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<ProgramResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                BaseActivity.hideDialog();
            }
        });
    }

}
