package com.cognegix.cognegixapp.facilitor.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.cognegix.cognegixapp.LoginActivity;
import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.facilitor.fragment.FragmentDashboard;
import com.cognegix.cognegixapp.facilitor.fragment.FragmentForum;
import com.cognegix.cognegixapp.facilitor.fragment.FragmentBadges;
import com.cognegix.cognegixapp.facilitor.fragment.FragmentHelpdesk;
import com.cognegix.cognegixapp.facilitor.fragment.FragmentMyDocument;
import com.cognegix.cognegixapp.facilitor.fragment.FragmentMyQuery;
import com.cognegix.cognegixapp.facilitor.fragment.FragmentNotification;
import com.cognegix.cognegixapp.facilitor.fragment.FragmentPastProgram;
import com.cognegix.cognegixapp.facilitor.fragment.FragmentProfile;
import com.cognegix.cognegixapp.facilitor.fragment.FragmentProgram;
import com.cognegix.cognegixapp.model.NotificationReponseModel;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;
import com.cognegix.cognegixapp.utils.network_connectivity.ConnectivityReceiver;
import com.cognegix.cognegixapp.utils.network_connectivity.MyApplication;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = HomeActivity.class.getSimpleName();
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;
    @BindView(R.id.navigation)
    NavigationView navigation;

    @BindView(R.id.tvUserName)
    CustomTextView tvUserName;
    @BindView(R.id.tvDesg)
    CustomTextView tvDesg;
    @BindView(R.id.tvCompany)
    CustomTextView tvCompany;
    @BindView(R.id.ivToolMenu)
    ImageButton ivToolMenu;

    @BindView(R.id.ivProfile)
    CircleImageView ivProfile;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfileToolbar;

    @BindView(R.id.rlProgram)
    RelativeLayout rlProgram;
    @BindView(R.id.rlProfile)
    RelativeLayout rlProfile;
    @BindView(R.id.rlDoc)
    RelativeLayout rlDoc;
    @BindView(R.id.rlPastProgram)
    RelativeLayout rlPastProgram;
    @BindView(R.id.rlBadges)
    RelativeLayout rlBadges;
    @BindView(R.id.rlHelpDeskFAQ)
    RelativeLayout rlHelpDesk;
    @BindView(R.id.rlMyQuery)
    RelativeLayout rlMyQuery;
    @BindView(R.id.rlLogout)
    RelativeLayout rlLogout;

    @BindView(R.id.rlForum)
    RelativeLayout rlForum;

    @BindView(R.id.rlDashBoard)
    RelativeLayout rlDashBoard;


    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;

    @BindView(R.id.tvNotificationCount)
    CustomTextView tvNotificationCount;

    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;

    @BindView(R.id.rlSetting)
    RelativeLayout rlSetting;

    @BindView(R.id.rlHome)
    RelativeLayout rlHome;
    @BindView(R.id.rvSearch)
    RelativeLayout rvSearch;
    @BindView(R.id.rlHelpDeskBottom)
    RelativeLayout rlHelpDeskBottom;

    ApiInterface apiInterface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();
    }

    private void init() {
        ButterKnife.bind(this);

        ivToolMenu.setOnClickListener(this);
        rlProgram.setOnClickListener(this);
        rlProfile.setOnClickListener(this);
        rlDoc.setOnClickListener(this);
        rlPastProgram.setOnClickListener(this);
        rlBadges.setOnClickListener(this);
        rlHelpDesk.setOnClickListener(this);
        rlMyQuery.setOnClickListener(this);
        rlLogout.setOnClickListener(this);
        rlNotification.setOnClickListener(this);
        rlSetting.setOnClickListener(this);
        rlHome.setOnClickListener(this);
        rvSearch.setOnClickListener(this);
        rlHelpDeskBottom.setOnClickListener(this);
        rlForum.setOnClickListener(this);
        rlDashBoard.setOnClickListener(this);
        //Todo set bu defult fragment
        getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new FragmentProgram()).commit();
        tvToolbarTitle.setText(getString(R.string.lProgram));
        apiInterface = ApiClient.getClient().create(ApiInterface.class);


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.ivToolMenu:
                drawer_layout.openDrawer(Gravity.LEFT);
                break;
            case R.id.rlProgram:
                //Todo load program fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new FragmentProgram()).commit();
                drawer_layout.closeDrawers();
                tvToolbarTitle.setText(getString(R.string.lProgram));

                break;
            case R.id.rlProfile:
                //Todo load profile
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new FragmentProfile()).commit();
                drawer_layout.closeDrawers();
                tvToolbarTitle.setText(getString(R.string.Profile));

                break;
            case R.id.rlDoc:
                //Todo load Document
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new FragmentMyDocument()).commit();
                drawer_layout.closeDrawers();
                tvToolbarTitle.setText(getString(R.string.MyDocument));

                break;
            case R.id.rlPastProgram:
                //Todo load Past program
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new FragmentPastProgram()).commit();
                drawer_layout.closeDrawers();
                tvToolbarTitle.setText(getString(R.string.pastPrograms));
                break;
            case R.id.rlBadges:
                //Todo load profile
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new FragmentBadges()).commit();
                drawer_layout.closeDrawers();
                tvToolbarTitle.setText(getString(R.string.badges));

                break;
            case R.id.rlHelpDeskFAQ:
                //Todo load program fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new FragmentHelpdesk()).commit();
                drawer_layout.closeDrawers();
                tvToolbarTitle.setText(getString(R.string.HelpDesk));
                break;

            case R.id.rlNotification:
                //Todo load notification fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new FragmentNotification()).commit();
                drawer_layout.closeDrawers();
                tvToolbarTitle.setText(getString(R.string.Notification));
                break;
            case R.id.rlMyQuery:
                //Todo load my query fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new FragmentMyQuery()).commit();
                drawer_layout.closeDrawers();
                tvToolbarTitle.setText(getString(R.string.myQuery));
                break;
            case R.id.rlLogout:
                //Todo logut and all data clear
                doLogout();
                break;

            case R.id.rlSetting:
                //Todo load my query fragment
               /* getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new SettingActivity()).commit();
                tvToolbarTitle.setText(getString(R.string.settings));
              */
                startActivity(new Intent(this, SettingActivity.class));


                break;
            case R.id.rlHome:
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new FragmentProgram()).commit();
                tvToolbarTitle.setText(getString(R.string.lProgram));
                break;
            case R.id.rvSearch:
                startActivity(new Intent(this, ActivitySearch.class));
                break;
            case R.id.rlHelpDeskBottom:
                startActivity(new Intent(this, ActivityHelpDesk.class));
                break;

            case R.id.rlForum:
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new FragmentForum()).commit();
                tvToolbarTitle.setText(getString(R.string.Forum));
                drawer_layout.closeDrawers();
                break;
           /* case R.id.rlQuiz:
                drawer_layout.closeDrawers();
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new FragmentDashboard()).commit();
                tvToolbarTitle.setText(getString(R.string.Assessment));
                drawer_layout.closeDrawers();
                break;*/
            case R.id.rlDashBoard:
                drawer_layout.closeDrawers();
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new FragmentDashboard()).commit();
                tvToolbarTitle.setText(getString(R.string.DashBoard));
                drawer_layout.closeDrawers();
                break;

        }

    }

    private void doLogout() {
        SharePref.setLoginResponse(this, null);
        SharePref.setRemeainingBadges(this, null);
        SharePref.setProfileType(this, null);
        SharePref.setNotificationCount(this, null);
        SharePref.setProgramModel(this, null);
        SharePref.setProgramResponse(this, null);
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        doProfileDataSet();
    }

    @Override
    protected void onStart() {
        super.onStart();
        doProfileDataSet();
        loadData();

    }

    private void doProfileDataSet() {
        Glide.with(this)
                .load(SharePref.getLoginResponse(this).getUserProfileDetail().getPhoto()) // Uri of the picture
                .into(ivProfile);
        Glide.with(this)
                .load(SharePref.getLoginResponse(this).getUserProfileDetail().getPhoto()) // Uri of the picture
                .into(ivUserProfileToolbar);
        String name = SharePref.getLoginResponse(this).getUserProfileDetail().getFirst_name() + " " + SharePref.getLoginResponse(this).getUserProfileDetail().getLast_name();
        String oName = SharePref.getLoginResponse(this).getUserOrganizationDetail().getName();
        String desg = SharePref.getLoginResponse(this).getUserProfileDetail().getDesignation();
        tvUserName.setText(name);
        tvDesg.setText(desg);
        tvCompany.setText(oName);

        if (SharePref.getNotificationCount(this) != null) {
            tvNotificationCount.setText(SharePref.getNotificationCount(this));
        }
    }


    //Todo load notification data
    private void loadData() {
        //  modelNotificationList.add(new ModelNotification("New program Understanding of 'Negotiation Skills' is about to start this week.", "Posted by Facilitator", "1 day ago"));
        // modelNotificationList.add(new ModelNotification("You have successfully completed topic 'Identifying objectives and building agendas'", "Posted by Facilitator", "1 day ago"));
        // adapterNotification.notifyDataSetChanged();


        //getActivity().showDialog();
        //showDialog(getActivity());
        // showDialog(this);

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();

        Call<NotificationReponseModel> responseCall = apiInterface.doNotiifcationList(accept, authorization,
                "0");

        responseCall.enqueue(new Callback<NotificationReponseModel>() {

            @Override
            public void onResponse(@NonNull Call<NotificationReponseModel> call, @NonNull Response<NotificationReponseModel> response) {
                //  BaseActivity.hideDialog();//  Log.d(TAG, "onResponse:     " + response.body().getStatus() + "===kffkdkb=====" + response.body().getMessage());
              //  Log.d(TAG, "Notification onResponse: " + response);

                if (response.code() == 200) {
                    if (response.body().getStatus().equals("true")) {
                        Log.d(TAG, "onResponse: " + response.body().getModelNotificationList().size());
                        String count = String.valueOf(response.body().getModelNotificationList().size());
                        tvNotificationCount.setText(count);
                        SharePref.setNotificationCount(getApplicationContext(), count);
                    } else {
                        tvNotificationCount.setText("0");
                    }
                }
            }


            @Override
            public void onFailure(@NonNull Call<NotificationReponseModel> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                // BaseActivity.hideDialog();
            }
        });


    }


}
