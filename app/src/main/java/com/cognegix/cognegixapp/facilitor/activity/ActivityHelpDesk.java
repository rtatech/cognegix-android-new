package com.cognegix.cognegixapp.facilitor.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomEditTextView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterFAQ;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.model.FAQModel;
import com.cognegix.cognegixapp.model.FAQResponse;
import com.cognegix.cognegixapp.model.ForgotResponse;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityHelpDesk extends BaseActivity implements View.OnClickListener, ItemOnClickListener {
    private static final String TAG = ActivityHelpDesk.class.getSimpleName();
    private static final String CALL_PERMISION = Manifest.permission.CALL_PHONE;
    private static final int PERMISSION = 3;
    View view;
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    @BindView(R.id.rlEmail)
    RelativeLayout rlEmail;
    @BindView(R.id.rlMsg)
    RelativeLayout rlMsg;
    @BindView(R.id.rlCall)
    RelativeLayout rlCall;
    @BindView(R.id.rvData)
    RecyclerView rvData;
    List<FAQModel> faqModelList;
    AdapterFAQ adapterFAQ;
    ApiInterface apiInterface;
    String number;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_helpdesk);
        init();
    }

    private void init() {
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        rlEmail.setOnClickListener(this);
        rlMsg.setOnClickListener(this);
        rlCall.setOnClickListener(this);
        tvToolbarTitle.setText(getString(R.string.HelpDesk));
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayout.VERTICAL, false);
        rvData.setLayoutManager(linearLayoutManager);
        faqModelList = new ArrayList<>();
        adapterFAQ = new AdapterFAQ(this, faqModelList);
        adapterFAQ.onClickHeader(this);
        rvData.setAdapter(adapterFAQ);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        loadData();
    }

    @SuppressLint("ResourceType")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;
            case R.id.rlEmail:
                doShowDialog("mail");
                break;
            case R.id.rlMsg:
                doShowDialog("sms");
                break;
            case R.id.rlCall:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkPermission()) {

                        if (!number.equals("")) {
                            Intent call = new Intent(Intent.ACTION_CALL);
                            call.setData(Uri.parse("tel:" + number));
                            startActivity(call);
                        }

                    } else {
                        requestPermission();
                    }
                } else {
                    if (!number.equals("")) {
                        Intent call = new Intent(Intent.ACTION_CALL);
                        call.setData(Uri.parse("tel:" + number));
                        startActivity(call);
                    }

                }

                break;

        }
    }

    //todo show dialog
    private void doShowDialog(final String type) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dailog_view_forum_message);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        CustomButtonView bCancel = dialog.findViewById(R.id.bCancel);
        CustomButtonView bPost = dialog.findViewById(R.id.bPost);

        final CustomEditTextView etMessage = dialog.findViewById(R.id.etMessage);
        CustomTextView tvTitle = dialog.findViewById(R.id.tvTitle);

        if (type.equals("sms")) {
            tvTitle.setText(getString(R.string.SMS));
        } else {
            tvTitle.setText(getString(R.string.Email1));

        }

        bPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = etMessage.getText().toString().trim();
                if (!msg.equalsIgnoreCase("")) {
                    dialog.dismiss();
                    doSendRequestToServer(type, msg);
                } else {
                    etMessage.setError("Enter Message");
                }
            }
        });
        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    //todo call helpdesk api
    private void doSendRequestToServer(final String type, String msg) {
        Log.d(TAG, "doSendRequestToServer:  type = " + type + "== msg " + msg);


        showDialog(this);
        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();

        Call<ForgotResponse> responseCall = apiInterface.doHelpASkAsUser(accept, authorization, type, msg);

        responseCall.enqueue(new Callback<ForgotResponse>() {

            @Override
            public void onResponse(@NonNull Call<ForgotResponse> call, @NonNull Response<ForgotResponse> response) {
              //  Log.d(TAG, "onResponse:     " + response.body().getStatus() + "===kffkdkb=====" + response.body().getMessage());
                hideDialog();

                if (response.code() == 200) {
                    Log.d(TAG, "onResponse: " + response.body().getMessage());
                    if (response.body().getStatus().equalsIgnoreCase("true")) {
                        showAlertDialog(ActivityHelpDesk.this, type, response.body().getMessage());
                    } else {
                        showAlertDialog(ActivityHelpDesk.this, type, response.body().getMessage());
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<ForgotResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    //Todo FAQ List
    private void loadData() {
        showDialog(this);

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();

        Call<FAQResponse> responseCall = apiInterface.doGetFAQList(accept, authorization);
        responseCall.enqueue(new Callback<FAQResponse>() {
            @Override
            public void onResponse(@NonNull Call<FAQResponse> call, @NonNull Response<FAQResponse> response) {
                //  Log.d(TAG, "onResponse: " + response.headers());
              //  Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));
                BaseActivity.hideDialog();
              //  Log.d(TAG, "onResponse: " + response);

                if (response.code() == 200) {

                    if (response.body().getStatus().equals("true")) {
                        number = response.body().getHelp_desk_phone();
                        Log.d(TAG, "onResponse: number==    " + number);
                        faqModelList.addAll(response.body().getFaqModelList());
                        adapterFAQ.notifyDataSetChanged();
                    } else {
                        showAlertDialog(ActivityHelpDesk.this, "FAQ", response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<FAQResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                BaseActivity.hideDialog();
            }
        });
    }


    @Override
    public void setOnItemClickListener(int position) {
        if (faqModelList.get(position).isSelectde()) {
            faqModelList.get(position).setSelectde(false);
        } else {
            faqModelList.get(position).setSelectde(true);
        }
        adapterFAQ.notifyDataSetChanged();
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{CALL_PERMISION}, PERMISSION);
    }

    private boolean checkPermission() {
        int call = ContextCompat.checkSelfPermission(this, CALL_PERMISION);
        return call == PackageManager.PERMISSION_GRANTED;
    }
}