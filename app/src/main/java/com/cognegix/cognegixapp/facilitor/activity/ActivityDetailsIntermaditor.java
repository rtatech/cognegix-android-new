package com.cognegix.cognegixapp.facilitor.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterCoordinate;
import com.cognegix.cognegixapp.model.program.ProgramDetailsResponse;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityDetailsIntermaditor extends BaseActivity implements View.OnClickListener {
    private static final String TAG = ActivityDetailsIntermaditor.class.getSimpleName();
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    @BindView(R.id.ivToolMenu)
    ImageButton ivToolMenu;
    @BindView(R.id.tvProgramTitle)
    CustomTextView tvProgramTitle;

    @BindView(R.id.tvProgramDesc)
    CustomTextView tvProgramDesc;
    @BindView(R.id.ivProgramImage)
    ImageView ivProgramImage;
    ApiInterface apiInterface;
    @BindView(R.id.tvPercentage)
    CustomTextView tvPercentage;
    @BindView(R.id.progressBar)
    ProgressBar progress;

    @BindView(R.id.tvProgramName)
    CustomTextView tvProgramName;
    @BindView(R.id.tvPDesc)
    CustomTextView tvPDesc;

    @BindView(R.id.tvDurationFrom)
    CustomTextView tvDurationFrom;

    @BindView(R.id.tvDurationTo)
    CustomTextView tvDurationTo;


    @BindView(R.id.tvProgramDirectorName)
    CustomTextView tvProgramDirectorName;
    @BindView(R.id.tvProgramDirectorProfile)
    CustomTextView tvProgramDirectorProfile;
    @BindView(R.id.rlDownloadDetails)
    RelativeLayout rlDownloadDetails;

    @BindView(R.id.vpCoordinor)
    ViewPager vpCoordinor;
    @BindView(R.id.tlCoordinor)
    TabLayout tlCoordinor;
    AdapterCoordinate adapterCoordinate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_intermaditor);

        init();


    }

    @SuppressLint("ClickableViewAccessibility")
    private void init() {
        ButterKnife.bind(this);
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);
        tvToolbarTitle.setText(getString(R.string.proramDetails));
        ivToolMenu.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        ivToolMenu.setOnClickListener(this);
        rlDownloadDetails.setOnClickListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayout.VERTICAL, false);

        adapterCoordinate = new AdapterCoordinate(this);
        vpCoordinor.setAdapter(adapterCoordinate);
        tlCoordinor.setupWithViewPager(vpCoordinor, true);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);


    }


    @Override
    protected void onStart() {
        super.onStart();
        doGetProgramDetails();
    }

    private void doGetProgramDetails() {
        showDialog(this);
        adapterCoordinate.clearList();
        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();


        Call<ProgramDetailsResponse> responseCall = apiInterface.getProgramDetails(accept, authorization,
                SharePref.getProgramModel(this).getId()
        );

        responseCall.enqueue(new Callback<ProgramDetailsResponse>() {

            @Override
            public void onResponse(@NonNull Call<ProgramDetailsResponse> call, @NonNull Response<ProgramDetailsResponse> response) {
                ////    Log.d(TAG, "onResponse:    +++> " + new Gson().toJson(response));
                hideDialog();


                if (response.code() == 200) {

                    if (response.body().getStatus().equalsIgnoreCase("true")) {


                        adapterCoordinate.setProgramCoordinatorsModelList(response.body().getProgramCoordinatorsModelList());

                        if (response.body().getProgramModel().getUser_role().equalsIgnoreCase("Participant")) {


                            String per = response.body().getProgramModel().getCompletion_percentage() + "%";
                            tvPercentage.setText(per);
                            try {

                                String progres = response.body().getProgramModel().getCompletion_percentage();

                                if (progres != null && !progres.equals("")) {
                                    float f1 = Float.parseFloat(progres);
                                    progress.setProgress((int) f1);
                                }
                            } catch (NumberFormatException ex) { // handle your exception
                                Log.e("Adapter", "instantiateItem: " + ex);
                            }

                        } else {
                            progress.setVisibility(View.GONE);
                            tvPercentage.setVisibility(View.GONE);
                        }


                        if (response.body().getProgramModel().getPhoto_path() != null && !response.body().getProgramModel().getPhoto_path().equals("")) {
                            String path = getPath(String.valueOf(ApiClient.getClient().baseUrl())) + response.body().getProgramModel().getPhoto_path();
                            Glide.with(getApplicationContext()).load(path).into(ivProgramImage);
                        }

                        SharePref.setRemeainingBadges(getApplicationContext(), response.body().getProgramModel().getRemaining_distribution_badges());
                        tvProgramTitle.setText(response.body().getProgramModel().getName());

                        tvProgramName.setText(response.body().getProgramModel().getName());


                        // String date = ": " + response.body().getProgram_start_date() + " - " +
                        //      response.body().getProgram_end_date();
                        String fromDate = "From: " + response.body().getProgram_start_date();
                        String toDate = "To: " + response.body().getProgram_end_date();

                        tvDurationTo.setText(toDate);
                        tvDurationFrom.setText(fromDate);

                        tvProgramDirectorName.setText(response.body().getProgramDirectorModel().getName());
                        // tvProgramDirectorProfile.setText(response.body().getProgramDirectorModel().getProfile());


                        String directorProfile = response.body().getProgramDirectorModel().getProfile();
                        String programDesc = response.body().getProgramModel().getDescription();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            tvProgramDirectorProfile.setText(Html.fromHtml(directorProfile, Html.FROM_HTML_MODE_COMPACT));
                            // tvProgramDesc.setText(response.body().getProgramModel().getDescription());
                            tvProgramDesc.setText(Html.fromHtml(programDesc, Html.FROM_HTML_MODE_COMPACT));
                            tvPDesc.setText(Html.fromHtml(programDesc, Html.FROM_HTML_MODE_COMPACT));

                            //tvPDesc.setText(response.body().getProgramModel().getDescription());

                        } else {
                            tvProgramDirectorProfile.setText(Html.fromHtml(directorProfile));
                            tvProgramDesc.setText(Html.fromHtml(programDesc));
                            tvPDesc.setText(Html.fromHtml(programDesc));
                        }

                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<ProgramDetailsResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });


    }

    public String getPath(String str) {
        str = str.substring(0, str.length() - 5);
        return str;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivToolMenu:
                onBackPressed();
                break;
            case R.id.rlDownloadDetails:
                String path = getPath(String.valueOf(ApiClient.getClient().baseUrl())) + SharePref.getProgramModel(this).getDocument_path();
                Intent intent = new Intent(this, LuanchDetailActivity.class);
                LuanchDetailActivity.isPDF = true;
                ActivitySearch.isActivitySearch = false;
                intent.putExtra(LuanchDetailActivity.KEY, path);
                startActivity(intent);
                finish();

                break;
        }
    }
}
