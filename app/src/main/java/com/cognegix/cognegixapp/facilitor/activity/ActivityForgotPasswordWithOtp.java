package com.cognegix.cognegixapp.facilitor.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomEditTextView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.model.ForgotResponse;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityForgotPasswordWithOtp extends BaseActivity implements View.OnClickListener {
    private static final String TAG = ActivityForgotPasswordWithOtp.class.getSimpleName();
    public static final String KEY = "keyActivityForgot";

    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;

    @BindView(R.id.etOtp)
    CustomEditTextView etOtp;
    @BindView(R.id.etNewPassword)
    CustomEditTextView etNewPassword;
    @BindView(R.id.etConfirmPassword)
    CustomEditTextView etConfirmPassword;

    @BindView(R.id.bSubmit)
    CustomButtonView bSubmit;


    //todo create varible
    String otp;
    String password;
    String confirm;
    ApiInterface apiInterface;
    String email;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password_with_otp);

        inti();
    }

    //Todo initalize all varible
    private void inti() {
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        tvToolbarTitle.setText(getString(R.string.forgotPassword));
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);
        bSubmit.setOnClickListener(this);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getIntent().getStringExtra(KEY) != null) {
            email = getIntent().getStringExtra(KEY);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;
            case R.id.bSubmit:
                if (isValidate()) {
                    doCallApiForForgotOtp();
                }
                break;
        }
    }


    //Todo call api for forgot password api
    private void doCallApiForForgotOtp() {
        showDialog(this);
        Call<ForgotResponse> responseCall = apiInterface.
                doForgotPasswordOtpVerification(email,
                        otp,
                        password,
                        confirm);


        Log.d(TAG, "doCallApiForForgotPassword: = = = " + responseCall.request().body());

        responseCall.enqueue(new Callback<ForgotResponse>() {

            @Override
            public void onResponse(@NonNull Call<ForgotResponse> call, @NonNull Response<ForgotResponse> response) {
               // Log.d(TAG, "onResponse:     " + response.body().getStatus() + "===kffkdkb=====" + response.body().getMessage());
                hideDialog();


                //  Log.d(TAG, "onResponse: " + response);


                if (response.code() == 200) {

                    if (response.body().getStatus().equalsIgnoreCase("true")) {
                        showAlertDialog(ActivityForgotPasswordWithOtp.this, "Forgot password", response.body().getMessage());

                    } else {
                        showAlertDialog(ActivityForgotPasswordWithOtp.this, "Forgot password", response.body().getMessage());

                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<ForgotResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


    //todo for store variable for api
    void initVariable() {
        otp = etOtp.getText().toString().trim();
        password = etNewPassword.getText().toString().trim();
        confirm = etConfirmPassword.getText().toString().trim();

    }

    //todo check validation for APi
    boolean isValidate() {
        boolean isValidate = true;
        initVariable();

        if (otp.equals("")) {
            etOtp.setError("Enter OTP");
            isValidate = false;
        }

        if (password.equals("")) {
            etNewPassword.setError("Enter Password");
            isValidate = false;
        }
        if (confirm.equals("")) {
            etConfirmPassword.setError("Enter Confirm Password");
            isValidate = false;
        } else {

            if (!password.equals(confirm)) {
                etConfirmPassword.setError("Enter Confirm does'nt match");
                isValidate = false;
            }

        }

        return isValidate;
    }
}
