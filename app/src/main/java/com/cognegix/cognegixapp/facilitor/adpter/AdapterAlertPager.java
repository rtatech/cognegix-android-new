package com.cognegix.cognegixapp.facilitor.adpter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AdapterAlertPager extends PagerAdapter {

    private Context context;
    private List<String> alrtList;

    public AdapterAlertPager(Context context, List<String> alrtList) {
        this.context = context;
        this.alrtList = alrtList;
    }

    @Override
    public int getCount() {
        return alrtList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ViewHolder viewHolder;
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_alert, container, false);
        viewHolder = new ViewHolder(view);
        ((ViewPager) container).addView(view);
        viewHolder.tvAlert.setText(alrtList.get(position));
        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }


    class ViewHolder {


        @BindView(R.id.tvAlert)
        CustomTextView tvAlert;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);

        }


    }


}
