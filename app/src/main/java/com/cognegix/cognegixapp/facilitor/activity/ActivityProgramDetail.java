package com.cognegix.cognegixapp.facilitor.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.model.program.ProgramDetailsResponse;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityProgramDetail extends BaseActivity implements View.OnClickListener {


    private static final String TAG = ActivityProgramDetail.class.getSimpleName();


    @BindView(R.id.tvProgramTitle)
    CustomTextView tvProgramTitle;
    @BindView(R.id.tvMessage)
    CustomTextView tvMessage;
    @BindView(R.id.tvProgramDesc)
    CustomTextView tvProgramDesc;

    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;

    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;

    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;

    @BindView(R.id.ivToolMenu)
    ImageButton ivToolMenu;

    @BindView(R.id.bViewDetails)
    CustomButtonView bViewDetails;
    @BindView(R.id.bForum)
    CustomButtonView bForum;
    @BindView(R.id.bParticipant)
    CustomButtonView bParticipant;

    @BindView(R.id.rlDownloadDetails)
    RelativeLayout rlDownloadDetails;

    @BindView(R.id.ivProgramImage)
    ImageView ivProgramImage;
    @BindView(R.id.rlHome)
    RelativeLayout rlHome;
    @BindView(R.id.rvSearch)
    RelativeLayout rvSearch;
    @BindView(R.id.rlHelpDeskBottom)
    RelativeLayout rlHelpDeskBottom;
    @BindView(R.id.rlSetting)
    RelativeLayout rlSetting;

    @BindView(R.id.tvPercentage)
    CustomTextView tvPercentage;
    @BindView(R.id.progressBar)
    ProgressBar progress;
    @BindView(R.id.ivPlaceHolder)
    ImageView ivPlaceHolder;

    ApiInterface apiInterface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_details);
        init();
    }

    private void init() {
        ButterKnife.bind(this);
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);

        ivToolMenu.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));

        tvMessage.setText(getString(R.string.directorMsg));
        tvToolbarTitle.setText(getString(R.string.proramDetails));

        ivToolMenu.setOnClickListener(this);
        bViewDetails.setOnClickListener(this);
        bForum.setOnClickListener(this);
        bParticipant.setOnClickListener(this);
        rlSetting.setOnClickListener(this);
        rlHome.setOnClickListener(this);
        rvSearch.setOnClickListener(this);
        ivPlaceHolder.setOnClickListener(this);
        rlDownloadDetails.setOnClickListener(this);

        rlHelpDeskBottom.setOnClickListener(this);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);


    }


    @Override
    protected void onStart() {
        super.onStart();
        doGetProgramDetails();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivToolMenu:
                onBackPressed();
                break;

            case R.id.bViewDetails:
                startActivity(new Intent(this, ProgramDetailsWthLearningActivity.class));
                finish();
                break;
            case R.id.bForum:
                startActivity(new Intent(this, ForumInterMediatorActivity.class));
                break;
            case R.id.bParticipant:
                startActivity(new Intent(this, ActivityParticipantList.class));
                break;

            case R.id.rlHome:
                // startActivity(new Intent(this, HomeActivity.class));
                finish();
                break;
            case R.id.rvSearch:
                //Todo goto search
                startActivity(new Intent(this, ActivitySearch.class));
                break;
            case R.id.rlHelpDeskBottom:
                //Todo goto help desk page
                startActivity(new Intent(this, ActivityHelpDesk.class));
                break;
            case R.id.rlSetting:
                //Todo goto setting page
                startActivity(new Intent(this, SettingActivity.class));
                break;


            case R.id.ivPlaceHolder:
                //Todo goto activity video director
                startActivity(new Intent(this, ActivityVideoDirector.class));
                break;


            case R.id.rlDownloadDetails:
               /* String path = getPath(String.valueOf(ApiClient.getClient().baseUrl())) + SharePref.getProgramModel(this).getDocument_path();
                Intent intent = new Intent(this, LuanchDetailActivity.class);
                LuanchDetailActivity.isPDF = true;
                ActivitySearch.isActivitySearch = false;
                intent.putExtra(LuanchDetailActivity.KEY, path);
                startActivity(intent);
                finish();*/

                Intent intent = new Intent(this, ActivityDetailsIntermaditor.class);
                startActivity(intent);

                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // startActivity(new Intent(this, HomeActivity.class));
        finish();
    }

    public String getPath(String str) {
        str = str.substring(0, str.length() - 5);
        return str;
    }

    private void doGetProgramDetails() {
        showDialog(this);
        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();


        Call<ProgramDetailsResponse> responseCall = apiInterface.getProgramDetails(accept, authorization,
                SharePref.getProgramModel(this).getId()
        );

        responseCall.enqueue(new Callback<ProgramDetailsResponse>() {

            @Override
            public void onResponse(@NonNull Call<ProgramDetailsResponse> call, @NonNull Response<ProgramDetailsResponse> response) {
                //  Log.d(TAG, "onResponse:    +++> " + response);
                hideDialog();


                if (response.code() == 200) {

                    if (response.body().getStatus().equalsIgnoreCase("true")) {


                        if (response.body().getProgramModel().getUser_role().equalsIgnoreCase("Participant")) {
                            String per = response.body().getProgramModel().getCompletion_percentage() + "%";
                            tvPercentage.setText(per);
                            try {

                                String progres = response.body().getProgramModel().getCompletion_percentage();

                                if (progres != null && !progres.equals("")) {
                                    float f1 = Float.parseFloat(progres);
                                    progress.setProgress((int) f1);
                                }
                            } catch (NumberFormatException ex) { // handle your exception
                                Log.e("Adapter", "instantiateItem: " + ex);
                            }

                        } else {
                            progress.setVisibility(View.GONE);
                            tvPercentage.setVisibility(View.GONE);
                        }


                        if (response.body().getProgramModel().getPhoto_path() != null && !response.body().getProgramModel().getPhoto_path().equals("")) {
                            String path = getPath(String.valueOf(ApiClient.getClient().baseUrl())) + response.body().getProgramModel().getPhoto_path();
                            Glide.with(getApplicationContext()).load(path).into(ivProgramImage);
                        }

                        SharePref.setRemeainingBadges(getApplicationContext(), response.body().getProgramModel().getRemaining_distribution_badges());
                        tvProgramTitle.setText(response.body().getProgramModel().getName());
                        tvProgramDesc.setText(response.body().getProgramModel().getDescription());
                        String desc = response.body().getProgramModel().getDescription();
                        ///  viewHolder.ivProgramImage.setImageResource(modelProgramList.get(position).getImagePath());
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            tvProgramDesc.setText(Html.fromHtml(desc, Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            tvProgramDesc.setText(Html.fromHtml(desc));
                        }

                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<ProgramDetailsResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });


    }


}

