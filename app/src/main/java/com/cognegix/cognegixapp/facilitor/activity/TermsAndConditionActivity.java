package com.cognegix.cognegixapp.facilitor.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class TermsAndConditionActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_condiation);
        init();
    }

    private void init() {
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        tvToolbarTitle.setText(getString(R.string.terms1));
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
