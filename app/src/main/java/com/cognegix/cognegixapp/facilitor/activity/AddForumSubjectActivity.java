package com.cognegix.cognegixapp.facilitor.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomEditTextView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.model.ForgotResponse;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddForumSubjectActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = AddForumSubjectActivity.class.getSimpleName();
    @BindView(R.id.bPost)
    CustomButtonView bPost;
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    ApiInterface apiInterface;

    @BindView(R.id.etSubject)
    CustomEditTextView etSubject;
    @BindView(R.id.etDescription)
    CustomEditTextView etDescription;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_forum_subject);
        init();
    }

    private void init() {
        ButterKnife.bind(this);
        bPost.setOnClickListener(this);
        ibBack.setOnClickListener(this);
        tvToolbarTitle.setText(getString(R.string.Forum));
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;

            case R.id.bPost:

                //    to post
                if (etSubject.getText().toString().trim().equals("")) {
                    etSubject.setError("Enter Subject");
                } else {
                    doPostToServer();
                }
                break;
        }

    }

    //Todo call api for post api
    private void doPostToServer() {

        showDialog(this);
        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();
        String progarmId = SharePref.getProgramModel(this).getId();
        Call<ForgotResponse> responseCall = apiInterface.doAddForumThread(accept, authorization,
                progarmId,
                etSubject.getText().toString().trim(),
                etDescription.getText().toString().trim());

        responseCall.enqueue(new Callback<ForgotResponse>() {

            @Override
            public void onResponse(@NonNull Call<ForgotResponse> call, @NonNull Response<ForgotResponse> response) {
              //  Log.d(TAG, "onResponse:     " + response.body().getStatus() + "===kffkdkb=====");
                hideDialog();
              //  Log.d(TAG, "onResponse: " + response);
               // Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));
                etSubject.setText("");
                etDescription.setText("");
                if (response.code() == 200) {
                    if (response.body().getStatus().equals("true")) {
                        // do somting
//                      showAlertDialog(AddForumSubjectActivity.this,"Forum", response.body().getMessage());

                        etSubject.setText("");
                        etDescription.setText("");

                        onBackPressed();
                    } else {
                        showAlertDialog(AddForumSubjectActivity.this, "Forum", response.body().getMessage());
                    }


                }


            }

            @Override
            public void onFailure(@NonNull Call<ForgotResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });


    }

    @Override
    public void doneDialog() {
        super.doneDialog();
        onBackPressed();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, ForumInterMediatorActivity.class));
        finish();
    }
}
