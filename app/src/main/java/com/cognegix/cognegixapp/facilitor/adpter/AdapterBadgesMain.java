package com.cognegix.cognegixapp.facilitor.adpter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.model.BadgesModelMain;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.model.program.ProgramModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterBadgesMain extends RecyclerView.Adapter<AdapterBadgesMain.ViewHolder> {
    private Context context;
    private List<ProgramModel> modelProgramList;
    private ItemOnClickListener itemOnClickListener;


    public AdapterBadgesMain(Context context, List<ProgramModel> modelProgramList) {
        this.context = context;
        this.modelProgramList = modelProgramList;
    }

    public void SetItemClickListener(ItemOnClickListener itemOnClickListener) {
        this.itemOnClickListener = itemOnClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_badges_main, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.tvFolderName.setText(modelProgramList.get(position).getName());
        //holder.tvShortName.setText(badgesModelMainList.get(position).getShortForm());

        holder.ivFolder.setImageResource(R.drawable.round_folder);
        //    holder.ivCircle.setBackgroundColor(modelRecyclerList.get(position).getColor());
        holder.ivFolder.setColorFilter(modelProgramList.get(position).getColor());

        final int pos = position;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemOnClickListener.setOnItemClickListener(pos);
            }
        });

    }

    @Override
    public int getItemCount() {
        return modelProgramList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvFolderName)
        CustomTextView tvFolderName;


        @BindView(R.id.ivFolder)
        ImageView ivFolder;
        @BindView(R.id.rlSide)
        RelativeLayout rlSide;
        @BindView(R.id.tvShortName)
        CustomTextView tvShortName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
