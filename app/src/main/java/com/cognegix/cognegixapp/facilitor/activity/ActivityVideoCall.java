package com.cognegix.cognegixapp.facilitor.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.model.ForgotResponse;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityVideoCall extends BaseActivity implements View.OnClickListener {
    private static final String TAG = ActivityVideoCall.class.getSimpleName();
    public static final String KEY = "keyVideo";
    public static final String NOTIFICATION = "idNotification";
    public static final String TOOLBAR = "idTool";


    private static final String RECORD_AUDIO_PERMISSION = Manifest.permission.RECORD_AUDIO;
    private static final String MODIFY_AUDIO_SETTINGS_PERMISSION = Manifest.permission.MODIFY_AUDIO_SETTINGS;

    private static final int PERMISSION = 3;

    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    @BindView(R.id.wvVideo)
    WebView wvVideo;
    String url;
    ApiInterface apiInterface;
    String nId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_call);
        init();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!checkPermission()) {
                requestPermission();
            }
        }
    }

    //todo intilization all veriable
    private void init() {
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        tvToolbarTitle.setText(getString(R.string.video));
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        // String url = method(String.valueOf(ApiClient.getClient().baseUrl())) + "" + SharePref.getProgramModel(this).getLearningTree().getProgramDirectorVideoUrl();

        if (getIntent().getStringExtra(KEY) != null) {
            url = getIntent().getStringExtra(KEY);
            nId = getIntent().getStringExtra(NOTIFICATION);
            Log.d(TAG, "init: id  " + nId);
            //wvVideo.loadUrl(url);
            doLoadVideoInWebView();
            if (!nId.equals("null")) {
                doSetNotificationISRead();
            }

        }


    }

    @SuppressLint("SetJavaScriptEnabled")
    private void doLoadVideoInWebView() {

        wvVideo.getSettings().setJavaScriptEnabled(true);
        wvVideo.clearCache(true);
        wvVideo.clearHistory();
        wvVideo.getSettings().setBuiltInZoomControls(true);
        wvVideo.getSettings().setJavaScriptEnabled(true);
        wvVideo.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wvVideo.getSettings().setUseWideViewPort(true);
        wvVideo.getSettings().setLoadWithOverviewMode(true);
        wvVideo.getSettings().setJavaScriptEnabled(true);
        wvVideo.getSettings().setAllowFileAccess(true);
        wvVideo.getSettings().setPluginState(WebSettings.PluginState.ON_DEMAND);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            wvVideo.getSettings().setAllowUniversalAccessFromFileURLs(true);
            wvVideo.getSettings().setAllowFileAccessFromFileURLs(true);

        }


        wvVideo.loadUrl(url);


        wvVideo.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    request.grant(request.getResources());
                }

              /*  myRequest = request;

                for (String permission : request.getResources()) {
                    switch (permission) {
                        case "android.webkit.resource.AUDIO_CAPTURE": {
                            askForPermission(request.getOrigin().toString(), Manifest.permission.RECORD_AUDIO, MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
                            break;
                        }
                    }
                }
            }*/

            }

        });

        wvVideo.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                Log.d(TAG, "shouldOverrideUrlLoading: " + url);

                view.loadUrl(url);

                return true;
            }

            public void onLoadResource(WebView view, String url) {

                //  Log.d(TAG, "onLoadResource: url === >  " + url);

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                showDialog(ActivityVideoCall.this);

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                hideDialog();

            }
        });

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;
        }

    }

    private void doSetNotificationISRead() {
        // showDialog(this);

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();

        Call<ForgotResponse> responseCall = apiInterface.doSetNotificationRead(
                accept,
                authorization,
                nId);
        responseCall.enqueue(new Callback<ForgotResponse>() {

            @Override
            public void onResponse(@NonNull Call<ForgotResponse> call, @NonNull Response<ForgotResponse> response) {
              //  Log.d(TAG, "onResponse:     " + response.body().getStatus() + "===kffkdkb=====" + response.body().getMessage());
                // hideDialog();


            }

            @Override
            public void onFailure(@NonNull Call<ForgotResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                //   hideDialog();
            }
        });

    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{RECORD_AUDIO_PERMISSION, MODIFY_AUDIO_SETTINGS_PERMISSION}, PERMISSION);
    }

    private boolean checkPermission() {
        int call = ContextCompat.checkSelfPermission(this, RECORD_AUDIO_PERMISSION);
        int mic_setting = ContextCompat.checkSelfPermission(this, MODIFY_AUDIO_SETTINGS_PERMISSION);

        return call == PackageManager.PERMISSION_GRANTED && mic_setting == PackageManager.PERMISSION_GRANTED;
    }
}
