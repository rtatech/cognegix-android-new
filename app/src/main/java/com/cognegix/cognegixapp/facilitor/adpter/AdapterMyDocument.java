package com.cognegix.cognegixapp.facilitor.adpter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.interfaces.OnClickDocument;
import com.cognegix.cognegixapp.model.document.DocumentModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterMyDocument extends RecyclerView.Adapter<AdapterMyDocument.ViewHolder> {
    private Context context;
    private OnClickDocument onClickDocument;
    private List<DocumentModel> documentModels = new ArrayList<>();

    public AdapterMyDocument(Context context) {
        this.context = context;
    }

    public void setOnClickButtons(OnClickDocument onClickDocument) {
        this.onClickDocument = onClickDocument;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_my_document, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.tvProgramTitle.setText(documentModels.get(position).getTitle());
        holder.bDownload.setTag(position);
        holder.bView.setTag(position);

    }

    @Override
    public int getItemCount() {
        return documentModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvProgramTitle)
        CustomTextView tvProgramTitle;

        @BindView(R.id.bDownload)
        CustomButtonView bDownload;
        @BindView(R.id.bView)
        CustomButtonView bView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            bDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int) view.getTag();
                    onClickDocument.onClickDownload(documentModels.get(pos));
                }
            });

            bView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int) view.getTag();
                    onClickDocument.onClickView(documentModels.get(pos));
                }
            });

        }
    }

    public void setList(List<DocumentModel> documentModels) {
        this.documentModels = documentModels;
        notifyDataSetChanged();
    }

    public void clerList() {
        this.documentModels.clear();
        notifyDataSetChanged();
    }

}
