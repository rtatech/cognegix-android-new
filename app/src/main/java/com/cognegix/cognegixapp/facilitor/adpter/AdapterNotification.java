package com.cognegix.cognegixapp.facilitor.adpter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.interfaces.OnClickNotification;
import com.cognegix.cognegixapp.model.ModelNotification;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterNotification extends RecyclerView.Adapter<AdapterNotification.ViewHolder> {
    private Context context;
    private List<ModelNotification> modelNotifications = new ArrayList<>();
    private OnClickNotification onClickNotification;


    public AdapterNotification(Context context) {
        this.context = context;
    }


    public void setItemOnClickListener(OnClickNotification onClickNotification) {
        this.onClickNotification = onClickNotification;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_notification, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        //  holder.tvPostBy.setText("pending froom response");
        holder.tvDate.setText(modelNotifications.get(position).getFormatted_created_at());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickNotification.onClickView(modelNotifications.get(position));
            }
        });


        Log.d("Adapter", "onBindViewHolder: " + modelNotifications.get(position).getIs_read()
                + "  posiosn=  " + position);
        if (modelNotifications.get(position).getIs_read().equals("0")) {
            String no = "*" + modelNotifications.get(position).getNotification();
            holder.tvNotificationTitle.setText(no);
            holder.rlTop.setBackgroundColor(Color.parseColor("#666666"));

        } else {
            holder.tvNotificationTitle.setText(modelNotifications.get(position).getNotification());
            holder.rlTop.setBackgroundColor(Color.parseColor("#ffffff"));
        }

    }

    @Override
    public int getItemCount() {
        return modelNotifications.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvNotificationTitle)
        CustomTextView tvNotificationTitle;
        //   @BindView(R.id.tvPostBy)
        //   CustomTextView tvPostBy;
        @BindView(R.id.tvDate)
        CustomTextView tvDate;

        @BindView(R.id.rlTop)
        RelativeLayout rlTop;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setList(List<ModelNotification> modelNotifications) {
        this.modelNotifications = modelNotifications;
        notifyDataSetChanged();
    }

    public void clearList() {
        this.modelNotifications.clear(); //= modelNotifications;
        notifyDataSetChanged();
    }

}
