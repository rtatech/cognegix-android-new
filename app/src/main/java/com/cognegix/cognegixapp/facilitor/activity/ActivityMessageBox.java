package com.cognegix.cognegixapp.facilitor.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.ParseException;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.model.MainResponseMessageBoard;
import com.cognegix.cognegixapp.model.program.BoardMessagesModel;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;

import java.sql.Date;
import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityMessageBox extends BaseActivity implements View.OnClickListener {
    private static final String TAG = ActivityMessageBox.class.getSimpleName();
    public static final String KEY = "boardKey";

    @BindView(R.id.tvDate)
    CustomTextView tvDate;
    @BindView(R.id.tvSender)
    CustomTextView tvSender;
    @BindView(R.id.tvSubject)
    CustomTextView tvSubject;
    @BindView(R.id.wvMessage)
    WebView wvMessage;
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.tvNotificationCount)
    CustomTextView tvNotificationCount;
    //Todo varible
    ApiInterface apiInterface;
    BoardMessagesModel boardMessagesModel;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfileToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_box);
        init();
    }

    //Todo init varible and set data
    @SuppressLint("SimpleDateFormat")
    private void init() {
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        rlNotification.setOnClickListener(this);
        tvToolbarTitle.setText(getString(R.string.messageBoard));
        boardMessagesModel = (BoardMessagesModel) getIntent().getSerializableExtra(KEY);

        String date_before = boardMessagesModel.getStart_date();

        Log.d(TAG, "init: " + date_before);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            java.util.Date date1 = formatter.parse(date_before);
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
            String date = format.format(date1);
            tvDate.setText(date);
            Log.d(TAG, "init: " + date1);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }


        tvSender.setText(boardMessagesModel.getMessage_from());
        tvSubject.setText(boardMessagesModel.getMessage_subject());
        //String summary = "<html><body><p> 'Understanding of Negotiation Skills' virtual classroom is scheduled on 25th March 2018 at 2:00 PM</p> <p>Lorem Ipsum text Lorem Ipsum text Lorem Ipsum text</p> <p>Lorem Ipsum text Lorem Ipsum text Lorem Ipsum text</p><p>Lorem Ipsum text Lorem Ipsum text Lorem Ipsum text</p> <p>Lorem Ipsum text Lorem Ipsum text Lorem Ipsum text</p> </body></html>";
        String summary = boardMessagesModel.getMessage_body();

        wvMessage.loadData(summary, "text/html", null);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);



        loadUI();

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;
            case R.id.rlNotification:
                startActivity(new Intent(this, ActivityNotification.class));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate) {

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = (Date) df_input.parse(inputDate);
            // parsed = new java.sql.Date(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            //  LOGE(TAG, "ParseException - dateFormat");
            Log.d(TAG, "formateDateFromstring: " + e);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        return outputDate;

    }

    @Override
    protected void onRestart() {
        super.onRestart();

        loadUI();

    }

    private void loadUI() {
        Glide.with(this)
                .load(SharePref.getLoginResponse(this).getUserProfileDetail().getPhoto()) // Uri of the picture
                .into(ivUserProfileToolbar);

        if (SharePref.getNotificationCount(this) != null) {
            tvNotificationCount.setText(SharePref.getNotificationCount(this));
        }


    }

}
