package com.cognegix.cognegixapp.facilitor.adpter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.bumptech.glide.Glide;
import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListenerParticipant;
import com.cognegix.cognegixapp.model.ModelParticipant;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class AdapterParticipant extends RecyclerView.Adapter<AdapterParticipant.ViewHolder>
        implements Filterable {

    private Context context;

    private List<ModelParticipant> modelParticipants;
    private ItemOnClickListenerParticipant itemOnClickListener;
    private List<ModelParticipant> filter;

    private String userType;

    public AdapterParticipant(Context context, List<ModelParticipant> modelParticipants) {
        this.context = context;
        this.modelParticipants = modelParticipants;
        this.filter = modelParticipants;
        //  this.filter.addAll(modelParticipants);
    }

    public void setOnItemListners(ItemOnClickListenerParticipant itemOnClickListener) {
        this.itemOnClickListener = itemOnClickListener;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_participant, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String name = filter.get(position).getFirst_name() + " " + filter.get(position).getLast_name();
        holder.tvParticipantName.setText(name);
        holder.tvParticipantDesg.setText(filter.get(position).getDesignation());
        String mobile = context.getString(R.string.mobile) + " " + filter.get(position).getWork_phone_number();
        holder.tvParticipantPhone.setText(mobile);

        Log.d("adapter par", "onBindViewHolder:==>   " + filter.get(position).getProfileBadgeModel().getContentRatingBadgeCount());
        if (filter.get(position).getPhoto() != null && !filter.get(position).getPhoto().equals("")) {
            Glide.with(context).load(filter.get(position).getPhoto()).into(holder.ivProfileParticipant);
        }
        holder.bView.setTag(position);
        holder.itemView.setTag(position);
        if (userType.equalsIgnoreCase("Participant")) {
            holder.cbParticipant.setVisibility(View.GONE);
        } else {
            holder.cbParticipant.setChecked(filter.get(position).isSelected());

        }
    }


    @Override
    public int getItemCount() {
        return filter.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filter = modelParticipants;
                } else {
                    List<ModelParticipant> filteredList = new ArrayList<>();
                    for (ModelParticipant row : modelParticipants) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getFirst_name().toLowerCase().contains(charString.toLowerCase())
                                || row.getWork_phone_number().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    filter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filter = (List<ModelParticipant>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvParticipantName)
        CustomTextView tvParticipantName;
        @BindView(R.id.tvParticipantDesg)
        CustomTextView tvParticipantDesg;
        @BindView(R.id.tvParticipantPhone)
        CustomTextView tvParticipantPhone;
        @BindView(R.id.bView)
        CustomButtonView bView;

        @BindView(R.id.ivProfileParticipant)
        CircleImageView ivProfileParticipant;

        @BindView(R.id.cbParticipant)
        AppCompatCheckBox cbParticipant;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            cbParticipant.setClickable(false);
            bView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int) view.getTag();
                    itemOnClickListener.setOnItemClickListener(filter.get(pos));

                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int) view.getTag();
                    itemOnClickListener.setOnItemClick(pos);
                    //  cbParticipant.setChecked(true);
                }
            });
        }


    }


}
