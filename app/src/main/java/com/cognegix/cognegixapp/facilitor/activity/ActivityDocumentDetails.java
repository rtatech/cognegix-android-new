package com.cognegix.cognegixapp.facilitor.activity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.utils.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ActivityDocumentDetails extends BaseActivity implements View.OnClickListener {
    private static final String TAG = ActivityDocumentDetails.class.getSimpleName();
    public static final String KEY = "keyDoc";



    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    @BindView(R.id.wvVideo)
    WebView wvVideo;
    String url;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_call);
        init();


    }

    //todo intilization all veriable
    private void init() {
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        tvToolbarTitle.setText(getString(R.string.MyDocument));
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        // String url = method(String.valueOf(ApiClient.getClient().baseUrl())) + "" + SharePref.getProgramModel(this).getLearningTree().getProgramDirectorVideoUrl();

        if (getIntent().getStringExtra(KEY) != null) {
            url = getIntent().getStringExtra(KEY);

            doLoadVideoInWebView();


        }


    }

    @SuppressLint("SetJavaScriptEnabled")
    private void doLoadVideoInWebView() {

        wvVideo.getSettings().setJavaScriptEnabled(true);
        wvVideo.clearCache(true);
        wvVideo.clearHistory();
        wvVideo.getSettings().setBuiltInZoomControls(true);
        wvVideo.getSettings().setJavaScriptEnabled(true);
        wvVideo.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wvVideo.getSettings().setUseWideViewPort(true);
        wvVideo.getSettings().setLoadWithOverviewMode(true);
        wvVideo.getSettings().setJavaScriptEnabled(true);
        wvVideo.getSettings().setAllowFileAccess(true);
        wvVideo.getSettings().setPluginState(WebSettings.PluginState.ON_DEMAND);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            wvVideo.getSettings().setAllowUniversalAccessFromFileURLs(true);
            wvVideo.getSettings().setAllowFileAccessFromFileURLs(true);

        }

        wvVideo.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url=" + url);

       // wvVideo.loadUrl(url);


        wvVideo.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    request.grant(request.getResources());
                }

              /*  myRequest = request;

                for (String permission : request.getResources()) {
                    switch (permission) {
                        case "android.webkit.resource.AUDIO_CAPTURE": {
                            askForPermission(request.getOrigin().toString(), Manifest.permission.RECORD_AUDIO, MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
                            break;
                        }
                    }
                }
            }*/

            }

        });

        wvVideo.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                Log.d(TAG, "shouldOverrideUrlLoading: " + url);
                if (url.toLowerCase().contains(".pdf".toLowerCase()) ||
                        url.toLowerCase().contains(".doc".toLowerCase()) ||
                        url.toLowerCase().contains(".ashx".toLowerCase()) ||
                        url.toLowerCase().contains(".docx".toLowerCase()) ||
                        url.toLowerCase().contains(".xls".toLowerCase()) ||
                        url.toLowerCase().contains(".xlsx".toLowerCase()) ||
                        url.toLowerCase().contains(".ppt".toLowerCase()) ||
                        url.toLowerCase().contains(".pptx".toLowerCase()) ||
                        url.toLowerCase().contains(".txt".toLowerCase())) {

                    view.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url="
                            + url);
                }
                // view.loadUrl(url);

                return true;
            }

            public void onLoadResource(WebView view, String url) {

                //  Log.d(TAG, "onLoadResource: url === >  " + url);

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                showDialog(ActivityDocumentDetails.this);

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                hideDialog();

            }
        });

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;
        }

    }

}
