package com.cognegix.cognegixapp.facilitor.activity;

import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomEditTextView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterSearch;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.model.SearchModel;
import com.cognegix.cognegixapp.model.SearchResponse;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySearch extends BaseActivity implements View.OnClickListener, ItemOnClickListener {
    private static final String TAG = ActivitySearch.class.getSimpleName();
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    @BindView(R.id.etSearch)
    CustomEditTextView etSearch;
    @BindView(R.id.bSearch)
    CustomButtonView bSearch;
    @BindView(R.id.rvData)
    RecyclerView rvData;
    ApiInterface apiInterface;

    AdapterSearch adapterSearch;
    List<SearchModel> searchModelList;

    public static boolean isActivitySearch = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        init();
    }

    //todo create
    private void init() {
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        tvToolbarTitle.setText(getString(R.string.Search));
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);
        bSearch.setOnClickListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayout.VERTICAL, false);
        rvData.setLayoutManager(linearLayoutManager);

        searchModelList = new ArrayList<>();
        adapterSearch = new AdapterSearch(this, searchModelList);
        rvData.setAdapter(adapterSearch);
        adapterSearch.setItemClickListener(this);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;

            case R.id.bSearch:
                if (!etSearch.getText().toString().equals("")) {
                    doSearch();
                } else {
                    etSearch.setText(getString(R.string.serarcKey));
                }
                break;


        }
    }

    //Todo call search api
    private void doSearch() {

        if (searchModelList.size() > 0) {
            searchModelList.clear();
        }
        showDialog(this);
        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();
        Call<SearchResponse> responseCall = apiInterface.doSearch(accept,
                authorization,
                etSearch.getText().toString().trim());

        responseCall.enqueue(new Callback<SearchResponse>() {

            @Override
            public void onResponse(@NonNull Call<SearchResponse> call, @NonNull Response<SearchResponse> response) {
               // Log.d(TAG, "onResponse:    +++> " + response);
                hideDialog();


                if (response.code() == 200) {
                    if (response.body().getStatus().equals("true")) {
                        Log.d(TAG, "onResponse: " + response.body().getStatus());
                        Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));
                        searchModelList.addAll(response.body().getSearchModelList());
                        doAddColor();
                    } else {
                        showAlertDialog(ActivitySearch.this, "Search", response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<SearchResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });

    }

    @Override
    public void setOnItemClickListener(int position) {
        String path = searchModelList.get(position).getLaunch_url();
        Intent intent = new Intent(this, LuanchDetailActivity.class);
        LuanchDetailActivity.isPDF = false;
        intent.putExtra(LuanchDetailActivity.KEY, path);
        isActivitySearch = true;
        startActivity(intent);
     ///   finish();
    }

    private void doAddColor() {
        for (int i = 0; i < searchModelList.size(); i++) {
            searchModelList.get(i).setColor(getRandomMaterialColor());
        }
        adapterSearch.notifyDataSetChanged();
    }

    //Todo set random color
    private int getRandomMaterialColor() {
        int returnColor = Color.GRAY;
        int arrayId = getResources().getIdentifier("mdcolor_400", "array", getPackageName());

        if (arrayId != 0) {
            TypedArray colors = getResources().obtainTypedArray(arrayId);
            int index = (int) (Math.random() * colors.length());
            returnColor = colors.getColor(index, Color.GRAY);
            colors.recycle();
        }
        return returnColor;
    }
}
