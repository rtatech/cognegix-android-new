package com.cognegix.cognegixapp.facilitor.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomEditTextView;
import com.cognegix.cognegixapp.customview.CustomTextInput;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.model.ForgotResponse;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private static final String TAG = SettingActivity.class.getSimpleName();
    //create
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
     @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;

    @BindView(R.id.etOldPass)
    CustomEditTextView etOldPass;
    @BindView(R.id.etNewPass)
    CustomEditTextView etNewPass;
    @BindView(R.id.etConfirmPass)
    CustomEditTextView etConfirmPass;
    @BindView(R.id.inlOldPass)
    CustomTextInput inlOldPass;
    @BindView(R.id.inlNewPass)
    CustomTextInput inlNewPass;
    @BindView(R.id.inlConfirmPass)
    CustomTextInput inlConfirmPass;
    @BindView(R.id.bChangePassword)
    CustomButtonView bChangePassword;
    @BindView(R.id.tbProfileType)
    ToggleButton tbProfileType;
    String oldPassword;
    String newPassword;
    String confirmPassword;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_setting);
        init();
    }

    private void init() {
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        bChangePassword.setOnClickListener(this);
        tvToolbarTitle.setText(getString(R.string.settings));
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);

        Log.d(TAG, "init: :===" + SharePref.getProfileType(this));

        if (SharePref.getProfileType(this).equals("1")) {
            tbProfileType.setChecked(true);
        } else {
            tbProfileType.setChecked(false);
        }

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        tbProfileType.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;

            case R.id.bChangePassword:
                if (isValidate()) {
                    doSendDataToServer();

                }
                break;

        }
    }



    private void doSendDataToServer() {
        showDialog(this);

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();

        Call<ForgotResponse> responseCall = apiInterface.doChangePassword(accept, authorization, oldPassword, newPassword, confirmPassword);
        responseCall.enqueue(new Callback<ForgotResponse>() {

            @Override
            public void onResponse(@NonNull Call<ForgotResponse> call, @NonNull Response<ForgotResponse> response) {
//                Log.d(TAG, "onResponse:     " + response.body().getStatus() + "===kffkdkb=====" + response.body().getMessage());
                hideDialog();

                if (response.code() == 200) {
                    etConfirmPass.setText("");
                    etNewPass.setText("");
                    etOldPass.setText("");

                    if (response.body().getStatus().equals("true")) {
                        showAlertDialog(SettingActivity.this, "Setting", response.body().getMessage());



                    } else {
                        showAlertDialog(SettingActivity.this, "Setting", response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ForgotResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });


    }


    void doUpdateProfile(String type) {
        showDialog(this);
        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();
        Call<ForgotResponse> responseCall = apiInterface.doUpdateProfileType(accept, authorization, type);
        Log.d(TAG, "doSendDataToServer: ==   " + responseCall.request().headers());
        Log.d(TAG, "doCallApiForForgotPassword: = = = " + responseCall.request().body());
        responseCall.enqueue(new Callback<ForgotResponse>() {

            @Override
            public void onResponse(@NonNull Call<ForgotResponse> call, @NonNull Response<ForgotResponse> response) {
//                Log.d(TAG, "onResponse:     " + response.body().getStatus() + "===kffkdkb=====" + response.body().getMessage());
                hideDialog();

                if (response.code() == 200) {
                    Log.d(TAG, "onResponse: " + response.body().getMessage());

                    if (response.body().getStatus().equals("true")) {

                        if (SharePref.getProfileType(getApplicationContext()).equals("1")) {
                            SharePref.setProfileType(getApplicationContext(), "0");
                        } else {
                            SharePref.setProfileType(getApplicationContext(), "1");
                        }
                        showAlertDialog(SettingActivity.this, "Setting", response.body().getMessage());

                    } else {
                        showAlertDialog(SettingActivity.this, "Setting", response.body().getMessage());

                    }
                  }


            }

            @Override
            public void onFailure(@NonNull Call<ForgotResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


    //todo for store variable for api
    void initVariable() {
        oldPassword = etOldPass.getText().toString().trim();
        newPassword = etNewPass.getText().toString().trim();
        confirmPassword = etConfirmPass.getText().toString().trim();

    }

    //todo check validation for APi
    boolean isValidate() {
        boolean isValidate = true;
        initVariable();

        if (oldPassword.equals("")) {
            inlOldPass.setError("Enter Old Password");
            isValidate = false;
        } else {
            inlOldPass.setError("");
        }

        if (newPassword.equals("")) {
            inlNewPass.setError("Enter New Password");
            isValidate = false;
        } else {
            inlNewPass.setError("");

        }
        if (confirmPassword.equals("")) {
            inlConfirmPass.setError("Enter Confirm Password");
            isValidate = false;
        } else {

            if (!newPassword.equals(confirmPassword)) {
                inlConfirmPass.setError("Password does'nt match");
                isValidate = false;
            } else {
                inlConfirmPass.setError("");
            }

        }


        if (oldPassword.equals(newPassword)) {
            inlNewPass.setError("New password and current password are same");
            isValidate = false;
        } else {
            inlNewPass.setError("");
        }

        return isValidate;
    }


    //todo handle profile type
    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (b) {
            //private
            Log.d(TAG, "onCheckedChanged:  check");
            doUpdateProfile("1");
        } else {
            Log.d(TAG, "onCheckedChanged:  un check");
            //public
            doUpdateProfile("0");
        }

    }
}
