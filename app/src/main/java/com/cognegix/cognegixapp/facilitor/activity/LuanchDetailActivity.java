package com.cognegix.cognegixapp.facilitor.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class LuanchDetailActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = LuanchDetailActivity.class.getSimpleName();
    public static final String KEY = "KeyForLuanch";
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;

    @BindView(R.id.rlSetting)
    RelativeLayout rlSetting;
    @BindView(R.id.rlHome)
    RelativeLayout rlHome;
    @BindView(R.id.rvSearch)
    RelativeLayout rvSearch;
    @BindView(R.id.rlHelpDeskBottom)
    RelativeLayout rlHelpDeskBottom;

    @BindView(R.id.wvDetails)
    WebView wvDetails;


    List<String> listUrls;
    boolean isBackUrl = false;

    String url;
    public static boolean isPDF = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_details_with_learning);
        init();
    }


    @SuppressLint("SetJavaScriptEnabled")
    private void init() {
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        tvToolbarTitle.setText(getString(R.string.proramDetails));
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);
        rlSetting.setOnClickListener(this);
        rlHome.setOnClickListener(this);
        rvSearch.setOnClickListener(this);
        rlHelpDeskBottom.setOnClickListener(this);
        listUrls = new ArrayList<>();
        //http://cogvc.testursites.info/cgx-front/MLT_5_19
        if (getIntent().getStringExtra(KEY) != null)
            url = getIntent().getStringExtra(KEY);


        Log.d(TAG, "init: " + url);
        wvDetails.getSettings().setJavaScriptEnabled(true);
        wvDetails.clearCache(true);
        wvDetails.clearHistory();
        wvDetails.getSettings().setBuiltInZoomControls(true);
        wvDetails.getSettings().setJavaScriptEnabled(true);
        wvDetails.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wvDetails.getSettings().setUseWideViewPort(true);
        wvDetails.getSettings().setLoadWithOverviewMode(true);
        wvDetails.getSettings().setJavaScriptEnabled(true);
        wvDetails.getSettings().setAllowFileAccess(true);
        wvDetails.getSettings().setPluginState(WebSettings.PluginState.ON_DEMAND);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            wvDetails.getSettings().setAllowUniversalAccessFromFileURLs(true);
            wvDetails.getSettings().setAllowFileAccessFromFileURLs(true);

        }


        if (isPDF) {
            // wvDetails.loadUrl("http://docs.google.com/gview?embedded=true&url=" + url);
            // Added line by Nitin
            String newUrl = "https://drive.google.com/viewerng/viewer?embedded=true&url=" + url;
            Log.d(TAG, "init:   " + newUrl);
            wvDetails.loadUrl(newUrl);

        } else {
            wvDetails.loadUrl(url);
        }


        wvDetails.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                Log.d(TAG, "shouldOverrideUrlLoading: " + url);
                String search = "mediaserver/document";

                if (url.toLowerCase().contains(ApiClient.LEARNING_TREE_LINK.toLowerCase())) {
                    Log.d(TAG, "shouldOverrideUrlLoading:  true");
                    isBackUrl = true;
                  //  listUrls.add(url);
                }
                if (url.toLowerCase().contains(ApiClient.LEARNING_TREE_LINK_ADD.toLowerCase())) {
                    listUrls.add(url);
                }


                // added if loop by Nitin
                if (url.toLowerCase().contains(".pdf".toLowerCase()) ||
                        url.toLowerCase().contains(".doc".toLowerCase()) ||
                        url.toLowerCase().contains(".ashx".toLowerCase()) ||
                        url.toLowerCase().contains(".docx".toLowerCase()) ||
                        url.toLowerCase().contains(".xls".toLowerCase()) ||
                        url.toLowerCase().contains(".xlsx".toLowerCase()) ||
                        url.toLowerCase().contains(".ppt".toLowerCase()) ||
                        url.toLowerCase().contains(".pptx".toLowerCase()) ||
                        url.toLowerCase().contains(".txt".toLowerCase())) {

                    view.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url="
                            + url);
                } else

                {

                    view.loadUrl(url);
                }

                return true;
            }

            public void onLoadResource(WebView view, String url) {

                //  Log.d(TAG, "onLoadResource: url === >  " + url);

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                showDialog(LuanchDetailActivity.this);

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                hideDialog();

              /*  Log.d(TAG,"listUrls===>"+listUrls.size());
                //int iCounter=0;
                String encodedZero = "LO3YeJPKrA";
                if(url.contains(encodedZero)){
                    int idx = 1;
                    Log.d("FinaDelete","fd"+idx);
                    while (idx <= listUrls.size()-1){
                        listUrls.remove(idx);
                    }

                }else if(!listUrls.contains(url)){
                    listUrls.add(url);
                } else{
                    int idx = listUrls.indexOf(url);
                    Log.d("Finalidx","fd"+idx);
                    idx = idx +1;
                    while (idx == listUrls.size()-1){
                        listUrls.remove(idx);
                    }
                }

                Log.d(TAG,"onPageFinished===>"+listUrls);*/


            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;

            case R.id.rlSetting:
                //Todo load my query fragment
                startActivity(new Intent(this, SettingActivity.class));
                break;
            case R.id.rlHome:
                // startActivity(new Intent(this, HomeActivity.class));
                //  finish();
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
               /* if (isPDF) {
                    startActivity(new Intent(this, HomeActivity.class));
                    finish();
                } else {
                    onBackPressed();
                }*/
                break;
            case R.id.rvSearch:
                startActivity(new Intent(this, ActivitySearch.class));

                break;
            case R.id.rlHelpDeskBottom:
                startActivity(new Intent(this, ActivityHelpDesk.class));
                break;

        }
    }

    @Override
    public void onBackPressed() {


        if (isBackUrl) {
            isBackUrl = false;

            String url = listUrls.get(listUrls.size() - 1);
            wvDetails.loadUrl(url);
            Log.d(TAG, "onBackPressed:  " + url);


        } else {
            super.onBackPressed();

            if (ActivitySearch.isActivitySearch) {
                //   startActivity(new Intent(this, ActivitySearch.class));
                finish();
            } else if (isPDF) {
                startActivity(new Intent(this, ActivityDetailsIntermaditor.class));
                finish();
            } else {
                finish();
            }
            // startActivity(new Intent(this, SettingActivity.class));
        }
      /*  boolean isExternal = false;
        List<Integer> indices = new ArrayList<Integer>();
        for (int iCounter=0; iCounter<listUrls.size();iCounter++){
            String url = listUrls.get(iCounter);
            if (!url.toLowerCase().contains(ApiClient.LEARNING_TREE_LINK_ADD.toLowerCase())) {
                indices.add(iCounter);
            }
        }
        Collections.sort(indices, Collections.reverseOrder());
        for (int iCounter=0; iCounter<indices.size();iCounter++){
            Log.d(TAG,"iCounter"+iCounter);
            int i = (indices.get(iCounter).intValue());
            listUrls.remove(i);
            isExternal = true;

        }

        if(listUrls.size()==1){
            startActivity(new Intent(this, ActivityProgramDetail.class));
            finish();
        }else{
            int counter=2;
            if (isExternal){
                counter = 1;
            }
            if ((listUrls.size() - counter) >= 0){
                String url = listUrls.get(listUrls.size() - counter);
                wvDetails.loadUrl(url);
            }

        }*/
    }
}
