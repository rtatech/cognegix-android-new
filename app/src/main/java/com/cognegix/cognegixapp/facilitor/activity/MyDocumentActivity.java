package com.cognegix.cognegixapp.facilitor.activity;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterMyDocument;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterSpinner;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterSpinnerAddQueary;
import com.cognegix.cognegixapp.interfaces.OnClickDocument;
import com.cognegix.cognegixapp.model.SpinnerModel;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.model.document.DocumentModel;
import com.cognegix.cognegixapp.model.document.ResponseDocument;
import com.cognegix.cognegixapp.model.program.ProgramModel;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyDocumentActivity extends BaseActivity implements View.OnClickListener, ItemOnClickListener, AdapterView.OnItemSelectedListener, OnClickDocument {
    private static final String TAG = MyDocumentActivity.class.getSimpleName();
    public static final String KEY = "keyForDocument";

    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.rvData)
    RecyclerView rvData;
    @BindView(R.id.sFilter)
    AppCompatSpinner sFilter;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    @BindView(R.id.fbAdd)
    FloatingActionButton fbAdd;
    @BindView(R.id.tvNotificationCount)
    CustomTextView tvNotificationCount;
    @BindView(R.id.rlSetting)
    RelativeLayout rlSetting;
    @BindView(R.id.rlHome)
    RelativeLayout rlHome;
    @BindView(R.id.rvSearch)
    RelativeLayout rvSearch;
    @BindView(R.id.rlHelpDeskBottom)
    RelativeLayout rlHelpDeskBottom;


    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;


    AdapterMyDocument adapterMyDocument;
    AdapterSpinnerAddQueary adapterSpinnerProgram;
    List<ProgramModel> modelProgramList;
    String programId;
    ApiInterface apiInterface;
    DownloadManager downloadManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_document);
        init();
    }


    //Todo inialazation all componant
    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        rlSetting.setOnClickListener(this);
        rlHome.setOnClickListener(this);
        rvSearch.setOnClickListener(this);
        rlHelpDeskBottom.setOnClickListener(this);
        rlNotification.setOnClickListener(this);


        fbAdd.setVisibility(View.GONE);
        modelProgramList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayout.VERTICAL, false);
        rvData.setLayoutManager(linearLayoutManager);
        adapterMyDocument = new AdapterMyDocument(this);
        adapterMyDocument.setOnClickButtons(this);
        // adapterBadges.setOnClickButtonShare(this);
        rvData.setAdapter(adapterMyDocument);
        adapterSpinnerProgram = new AdapterSpinnerAddQueary(this, modelProgramList);
        sFilter.setAdapter(adapterSpinnerProgram);
        programId = getIntent().getStringExtra(KEY);
        sFilter.setOnItemSelectedListener(this);


        loadSpinnerData();

        tvToolbarTitle.setText(getString(R.string.MyDocument));
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        doGetDocumentList();


    }

    private void doGetDocumentList() {

        showDialog(this);

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();

        Call<ResponseDocument> responseCall = apiInterface.doGetResponseDocument(
                accept,
                authorization,
                programId);
        responseCall.enqueue(new Callback<ResponseDocument>() {
            @Override
            public void onResponse(@NonNull Call<ResponseDocument> call, @NonNull Response<ResponseDocument> response) {
                //  Log.d(TAG, "onResponse: " + response.headers());
               // Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));
                hideDialog();
               // Log.d(TAG, "onResponse: " + response);
                if (response.code() == 200) {

                    if (response.body().isStatus()) {

                        adapterMyDocument.setList(response.body().getDocuments());
                    } else {
                        adapterMyDocument.clerList();
                        showAlertDialog(MyDocumentActivity.this, "My Document", response.body().getMessage());
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<ResponseDocument> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        Glide.with(this)
                .load(SharePref.getLoginResponse(this).getUserProfileDetail().getPhoto()) // Uri of the picture
                .into(ivUserProfile);
        if (SharePref.getNotificationCount(this) != null) {
            tvNotificationCount.setText(SharePref.getNotificationCount(this));
        }
    }

    //Todo load data for spinner
    private void loadSpinnerData() {
        modelProgramList.add(new ProgramModel("Select"));
        modelProgramList.addAll(SharePref.getProgramResponse(this).getProgram_list());
        adapterSpinnerProgram.notifyDataSetChanged();

        if (modelProgramList.size() > 0) {
            //  selectValue(programId);
            selectDropDownValue(modelProgramList, programId);
        }

    }

    @Override
    public void selectedDropDownValue(int position) {
        super.selectedDropDownValue(position);
        sFilter.setSelection(position);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;

            case R.id.rlSetting:
                //Todo load my query fragment
                startActivity(new Intent(this, SettingActivity.class));
                break;
            case R.id.rlHome:
                // startActivity(new Intent(this, HomeActivity.class));
                finish();
                break;
            case R.id.rvSearch:
                startActivity(new Intent(this, ActivitySearch.class));

                break;
            case R.id.rlNotification:
                startActivity(new Intent(this, ActivityNotification.class));
                break;
            case R.id.rlHelpDeskBottom:
                startActivity(new Intent(this, ActivityHelpDesk.class));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


    //todo open bottom sheet dialog for sharing
    private void doOpenSheet() {
        Log.d(TAG, "doOpenSheet: ");
        View view = getLayoutInflater().inflate(R.layout.dailog_share, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);
        dialog.show();
        CustomButtonView bShare = dialog.findViewById(R.id.bShare);
        CustomButtonView bCancel = dialog.findViewById(R.id.bCancel);


        assert bShare != null;
        bShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        assert bCancel != null;
        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


    }


    //todo click on share button
    @Override
    public void setOnItemClickListener(int position) {
        doOpenSheet();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (i != 0) {
            programId = modelProgramList.get(i).getId();
            doGetDocumentList();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onClickView(DocumentModel documentModel) {
        Intent intent = new Intent(getApplicationContext(), ActivityDocumentDetails.class);
        intent.putExtra(ActivityDocumentDetails.KEY, documentModel.getDocument_path());
        startActivity(intent);
    }

    @Override
    public void onClickDownload(DocumentModel documentModel) {
        downloadFromServer(documentModel.getDocument_path());
    }


    void downloadFromServer(String url) {
        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        Uri uri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        Long ref = downloadManager.enqueue(request);


    }

}
