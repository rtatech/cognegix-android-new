package com.cognegix.cognegixapp.facilitor.adpter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.interfaces.OnButtonLuanchClickListener;
import com.cognegix.cognegixapp.model.program.UpComingEventModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AdapterActivityCalenderPager extends PagerAdapter {

    private Context context;
    private List<UpComingEventModel> upComingEventModelList;
    private OnButtonLuanchClickListener itemOnClickListener;

    public AdapterActivityCalenderPager(Context context, List<UpComingEventModel> upComingEventModelList) {
        this.context = context;
        this.upComingEventModelList = upComingEventModelList;
    }

    public void setOnClickLuanchButton(OnButtonLuanchClickListener itemOnClickListener) {
        this.itemOnClickListener = itemOnClickListener;
    }

    @Override
    public int getCount() {
        return upComingEventModelList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ViewHolder viewHolder;
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_home_calender_activity, container, false);
        viewHolder = new ViewHolder(view);
        ((ViewPager) container).addView(view);

        viewHolder.tvEventTitle.setText(upComingEventModelList.get(position).getTopic_name());
        viewHolder.bLaunch.setTag(position);
        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }


    class ViewHolder {

        @BindView(R.id.tvEventTitle)
        CustomTextView tvEventTitle;
        @BindView(R.id.bLaunch)
        CustomButtonView bLaunch;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);

            bLaunch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int) view.getTag();
                    itemOnClickListener.setOnLuanchButtonClickListener(upComingEventModelList.get(pos).getLaunch_url());
                }
            });

        }


    }


}
