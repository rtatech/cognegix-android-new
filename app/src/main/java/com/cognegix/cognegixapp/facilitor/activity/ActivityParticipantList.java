package com.cognegix.cognegixapp.facilitor.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomEditTextView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterParticipant;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListenerParticipant;
import com.cognegix.cognegixapp.model.ModelParticipant;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.model.ProgramParticipantResponse;
import com.cognegix.cognegixapp.model.ProgramParticipantResponse;
import com.cognegix.cognegixapp.model.VideoCallResponse;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityParticipantList extends BaseActivity implements View.OnClickListener, TextWatcher, ItemOnClickListenerParticipant {
    private static final String TAG = ActivityParticipantList.class.getSimpleName();
    @BindView(R.id.rvData)
    RecyclerView rvData;
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    @BindView(R.id.etSearch)
    CustomEditTextView etSearch;

    @BindView(R.id.bVideo)
    CustomButtonView bVideo;

    AdapterParticipant adapteParticipant;
    List<ModelParticipant> modelParticipantList;
    ApiInterface apiInterface;

    ArrayList<String> participantIds = new ArrayList<>();

    int selectedCount = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_participant_list);

        init();

        //doIntiateCall();
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayout.VERTICAL, false);
        rvData.setLayoutManager(linearLayoutManager);
        tvToolbarTitle.setText(getString(R.string.participantList));
        ibBack.setOnClickListener(this);
        bVideo.setOnClickListener(this);
        etSearch.addTextChangedListener(this);
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);

        modelParticipantList = new ArrayList<>();
        adapteParticipant = new AdapterParticipant(this, modelParticipantList);
        adapteParticipant.setOnItemListners(this);
        adapteParticipant.setUserType(SharePref.getProgramModel(getApplicationContext()).getUser_role());
        rvData.setAdapter(adapteParticipant);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        if (SharePref.getProgramModel(getApplicationContext()).getUser_role().equalsIgnoreCase("Participant")) {
            bVideo.setVisibility(View.GONE);
        } else {
            bVideo.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        loadData();
        selectedCount = 0;
        String bText = getString(R.string.video);//+ " (" + selectedCount + ")";
        bVideo.setText(bText);
    }

    private void loadData() {

        if (modelParticipantList.size() > 0) {
            modelParticipantList.clear();
        }


        showDialog(this);

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();


        Call<ProgramParticipantResponse> responseCall = apiInterface.doGetParticipantList(accept, authorization,
                SharePref.getProgramModel(this).getId());

        responseCall.enqueue(new Callback<ProgramParticipantResponse>() {

            @Override
            public void onResponse(@NonNull Call<ProgramParticipantResponse> call, @NonNull Response<ProgramParticipantResponse> response) {
                BaseActivity.hideDialog();
                //  Log.d(TAG, "onResponse:     " + response.body().getStatus() + "===kffkdkb=====" + response.body().getMessage());
              //  Log.d(TAG, "onResponse:====    " + new Gson().toJson(response));
                if (response.code() == 200) {
                    if (response.body().getStatus().equals("true")) {
                        Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));

                        modelParticipantList.addAll(response.body().getModelParticipantList());
                        adapteParticipant.notifyDataSetChanged();
                    } else {
                        showAlertDialog(ActivityParticipantList.this, "Program Participant", response.body().getMessage());
                    }

                }
            }


            @Override
            public void onFailure(@NonNull Call<ProgramParticipantResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                BaseActivity.hideDialog();
            }
        });


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.ibBack:
                onBackPressed();
                break;
            case R.id.bVideo:

               /* if (participantIds.size() > 0) {
                    doIntiateCall();

                } else {
                    showAlertDialog(this, "", "Select participant first");
                }*/
                doIntiateCall();
                break;

        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        // adapteParticipant.filters(charSequence.toString());

        adapteParticipant.getFilter().filter(charSequence.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void setOnItemClickListener(ModelParticipant modelParticipant) {
        Intent intent = new Intent(this, ActivityParticipantProfile.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(ActivityParticipantProfile.KEY, modelParticipant);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void setOnItemClick(int pos) {

        if (!modelParticipantList.get(pos).isSelected()) {
            modelParticipantList.get(pos).setSelected(true);
            //  participantIds.add(modelParticipantList.get(pos).getCustomer_party_id());
            adapteParticipant.notifyDataSetChanged();
            selectedCount++;

        } else {
            modelParticipantList.get(pos).setSelected(false);
            adapteParticipant.notifyDataSetChanged();
            selectedCount--;

        }
        String bText = getString(R.string.video) + " (" + selectedCount + ")";
        bVideo.setText(bText);


    }


    private void doIntiateCall() {


        for (int i = 0; i < modelParticipantList.size(); i++) {

            if (modelParticipantList.get(i).isSelected()) {
                Log.d(TAG, "doIntiateCall: " + modelParticipantList.get(i).getUser_id());
                participantIds.add(modelParticipantList.get(i).getUser_id());

            }

        }

      /*  participantIds.add("34");
        participantIds.add("35");
        participantIds.add("36");
        participantIds.add("37");
        participantIds.add("38");*/


        showDialog(this);

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();


        Call<VideoCallResponse> responseCall = apiInterface.doInitiateCall(
                accept,
                authorization,
                SharePref.getProgramModel(this).getId()
                , participantIds);

        responseCall.enqueue(new Callback<VideoCallResponse>() {

            @Override
            public void onResponse(@NonNull Call<VideoCallResponse> call, @NonNull Response<VideoCallResponse> response) {
                BaseActivity.hideDialog();//  Log.d(TAG, "onResponse:     " + response.body().getStatus() + "===kffkdkb=====" + response.body().getMessage());

               // Log.d(TAG, "onResponse: video call == >  " + new Gson().toJson(response));

                if (response.code() == 200) {
                    if (response.body().getStatus().equals("true")) {
                        //  showAlertDialog(ActivityParticipantList.this, "Program Participant", response.body().getMessage());

                        Intent intent = new Intent(getApplicationContext(), ActivityVideoCall.class);
                        intent.putExtra(ActivityVideoCall.KEY, response.body().getJoin_url());
                        intent.putExtra(ActivityVideoCall.NOTIFICATION, "null");
                        startActivity(intent);

                      /*  String url = response.body().getJoin_url();
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(browserIntent);*/

                    } else {
                        showAlertDialog(ActivityParticipantList.this, "Program Participant", response.body().getMessage());
                    }

                }
            }


            @Override
            public void onFailure(@NonNull Call<VideoCallResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                BaseActivity.hideDialog();
            }
        });


    }

}
