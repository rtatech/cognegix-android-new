package com.cognegix.cognegixapp.facilitor.activity;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ActivityVideoDirector extends BaseActivity implements View.OnClickListener {
    private static final String TAG = ActivityVideoDirector.class.getSimpleName();
    @BindView(R.id.vvProgram)
    VideoView vvProgram;
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    @BindView(R.id.wvDetails)
    WebView wvDetails;
    MediaController mediaController;
    String url;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_director_video);
        init();
    }

    //todo intilization all veriable
    private void init() {
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        tvToolbarTitle.setText(getString(R.string.directorMsg));
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);

         mediaController = new MediaController(this);
        mediaController.setAnchorView(vvProgram);
        // String url = method(String.valueOf(ApiClient.getClient().baseUrl())) + "" + SharePref.getProgramModel(this).getLearningTree().getProgramDirectorVideoUrl();

        url = SharePref.getProgramModel(this).getLearningTree().getProgramDirectorVideoUrl();
        Log.d(TAG, "init: " + url);


        if (url.toLowerCase().contains("/storage/mediaserver/video/".toLowerCase())) {
            doLoadVideoInVideoView();
        } else {
            doLoadVideoInWebView();
        }
        //  doLoadVideoInWebView();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void doLoadVideoInWebView() {
        vvProgram.setVisibility(View.GONE);

        wvDetails.clearCache(true);
        wvDetails.clearHistory();
        wvDetails.getSettings().setBuiltInZoomControls(true);
        wvDetails.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wvDetails.getSettings().setUseWideViewPort(true);
        wvDetails.getSettings().setLoadWithOverviewMode(true);
        wvDetails.getSettings().setJavaScriptEnabled(true);
        wvDetails.getSettings().setAllowFileAccess(true);
        wvDetails.getSettings().setPluginState(WebSettings.PluginState.ON_DEMAND);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            wvDetails.getSettings().setAllowUniversalAccessFromFileURLs(true);
            wvDetails.getSettings().setAllowFileAccessFromFileURLs(true);

        }
        wvDetails.loadUrl(url);

        wvDetails.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });


    }

    private void doLoadVideoInVideoView() {
        wvDetails.setVisibility(View.GONE);
        String url1 = method(String.valueOf(ApiClient.getClient().baseUrl())) + "" + url;
        Uri uri = Uri.parse(url1);
        vvProgram.setMediaController(mediaController);
        vvProgram.setVideoURI(uri);
        vvProgram.requestFocus();
        vvProgram.start();
    }

    public String method(String str) {
        str = str.substring(0, str.length() - 5);
        return str;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;
        }

    }


}
