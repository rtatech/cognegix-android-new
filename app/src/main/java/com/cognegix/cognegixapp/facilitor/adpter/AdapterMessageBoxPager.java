package com.cognegix.cognegixapp.facilitor.adpter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.interfaces.ButtonOnClickListener;
import com.cognegix.cognegixapp.model.program.BoardMessagesModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AdapterMessageBoxPager extends PagerAdapter {

    private Context context;
    private ButtonOnClickListener buttonOnClickListener;
    private List<BoardMessagesModel> messageBoardModelList;


    public AdapterMessageBoxPager(Context context, List<BoardMessagesModel> messageBoardModelList) {
        this.context = context;
        this.messageBoardModelList = messageBoardModelList;
    }

    public void setOnButtonClick(ButtonOnClickListener buttonOnClickListener) {
        this.buttonOnClickListener = buttonOnClickListener;
    }

    @Override
    public int getCount() {
        return messageBoardModelList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View) object);

        //return view == object;

    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ViewHolder viewHolder;
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_message_box, container, false);
        viewHolder = new ViewHolder(view);
        ((ViewPager) container).addView(view);

       /* viewHolder.tvDate.setText(modelMessageBoxList.get(position).getDate());
        viewHolder.tvSender.setText(modelMessageBoxList.get(position).getSender());
        viewHolder.tvSubject.setText(modelMessageBoxList.get(position).getSubject());
*/

        // viewHolder.tvDate.setText(messageBoardModelList.get(position).getStart_date());
        viewHolder.tvDate.setText(formatedDate(messageBoardModelList.get(position).getStart_date()));
        viewHolder.tvSender.setText(messageBoardModelList.get(position).getMessage_from());
        viewHolder.tvSubject.setText(messageBoardModelList.get(position).getMessage_subject());


        viewHolder.bView.setTag(position);
        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }


    class ViewHolder {


        @BindView(R.id.bView)
        CustomButtonView bView;

        @BindView(R.id.tvDate)
        CustomTextView tvDate;

        @BindView(R.id.tvSender)
        CustomTextView tvSender;

        @BindView(R.id.tvSubject)
        CustomTextView tvSubject;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);

            bView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int pos = (int) view.getTag();

                    buttonOnClickListener.setOnButtonClickListener(pos);
                }
            });

        }


    }

    @SuppressLint("SimpleDateFormat")
    private String formatedDate(String date) {

        // 10th March 2018
        String forDate = null;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            java.util.Date date1 = formatter.parse(date);
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");

            forDate = format.format(date1);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return forDate;
    }


}
