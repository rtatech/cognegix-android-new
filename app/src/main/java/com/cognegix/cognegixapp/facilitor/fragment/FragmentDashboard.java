package com.cognegix.cognegixapp.facilitor.fragment;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.facilitor.activity.HomeActivity;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentDashboard extends Fragment {
    private static final String TAG = FragmentDashboard.class.getSimpleName();


    View view;
    @BindView(R.id.wvAssessment)
    WebView wvAssessment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_assessment, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void init() {
        Log.d(TAG, "init: ");
        //Todo all intiazaltion
        ButterKnife.bind(this, view);
        wvAssessment.getSettings().setJavaScriptEnabled(true);
        wvAssessment.clearCache(true);
        wvAssessment.clearHistory();
        wvAssessment.getSettings().setBuiltInZoomControls(true);
        wvAssessment.getSettings().setJavaScriptEnabled(true);
        wvAssessment.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wvAssessment.setWebViewClient(new WebViewClient());
        wvAssessment.getSettings().setUseWideViewPort(true);
        wvAssessment.getSettings().setLoadWithOverviewMode(true);
        wvAssessment.getSettings().setJavaScriptEnabled(true);
        wvAssessment.getSettings().setAllowFileAccess(true);
        wvAssessment.getSettings().setPluginState(WebSettings.PluginState.ON_DEMAND);


        //  String newUrl = "http://192.168.0.113:8000/api/exercises/" + SharePref.getLoginResponse(getContext()).getUserDetail().getId();
        String newUrl = method(String.valueOf(ApiClient.getClient().baseUrl())) +
                "dashboard/" + SharePref.getLoginResponse(getContext()).getUser_hash();

        Log.d(TAG, "init: " + newUrl);
        wvAssessment.loadUrl(newUrl);

        wvAssessment.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                ((HomeActivity) getActivity()).showDialog(getActivity());
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                BaseActivity.hideDialog();

            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                Log.d(TAG, "shouldOverrideUrlLoading: " + url);

                view.loadUrl(url);
                return true;
            }

            public void onLoadResource(WebView view, String url) {
                Log.d(TAG, "onLoadResource: url === >  " + url);


            }


        });


    }

    public String method(String str) {
        str = str.substring(0, str.length() - 4);
        return str;
    }


}
