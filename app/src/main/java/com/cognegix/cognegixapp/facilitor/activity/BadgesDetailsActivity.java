package com.cognegix.cognegixapp.facilitor.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterBadges;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterLeaderPager;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterSpinner;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterSpinnerAddQueary;
import com.cognegix.cognegixapp.model.badges.BadgesLeaderModel;
import com.cognegix.cognegixapp.model.badges.BadgesLederResponse;
import com.cognegix.cognegixapp.model.badges.BadgesModel;
import com.cognegix.cognegixapp.model.badges.BadgesResponse;
import com.cognegix.cognegixapp.model.LeaderModel;
import com.cognegix.cognegixapp.model.SpinnerModel;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.model.program.ProgramModel;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BadgesDetailsActivity extends BaseActivity implements View.OnClickListener, ItemOnClickListener, AdapterView.OnItemSelectedListener {
    private static final String TAG = BadgesDetailsActivity.class.getSimpleName();

    public static final String KEY = "keyBadgesList";

    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.tlLeader)
    TabLayout tlLeader;
    @BindView(R.id.vpLeader)
    ViewPager vpLeader;
    @BindView(R.id.rvData)
    RecyclerView rvData;
    @BindView(R.id.sFilter)
    AppCompatSpinner sFilter;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    @BindView(R.id.tvNotificationCount)
    CustomTextView tvNotificationCount;

    AdapterLeaderPager adapterLeaderPager;
    AdapterBadges adapterBadges;
    List<BadgesModel> badgesModelList;
    List<LeaderModel> leaderModelList;
    ApiInterface apiInterface;
    String programId;
    AdapterSpinnerAddQueary adapterSpinnerProgram;
    List<ProgramModel> modelProgramList;
    @BindView(R.id.rlHome)
    RelativeLayout rlHome;
    @BindView(R.id.rvSearch)
    RelativeLayout rvSearch;
    @BindView(R.id.rlHelpDeskBottom)
    RelativeLayout rlHelpDeskBottom;
    @BindView(R.id.rlSetting)
    RelativeLayout rlSetting;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_badges_details);
        init();
    }


    //Todo inialazation all componant
    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        rlSetting.setOnClickListener(this);
        rlHome.setOnClickListener(this);
        rvSearch.setOnClickListener(this);
        rlHelpDeskBottom.setOnClickListener(this);
        rlNotification.setOnClickListener(this);

        modelProgramList = new ArrayList<>();

        tlLeader.setupWithViewPager(vpLeader, true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayout.VERTICAL, false);
        rvData.setLayoutManager(linearLayoutManager);
        badgesModelList = new ArrayList<>();
        adapterBadges = new AdapterBadges(this, badgesModelList);
        adapterBadges.setOnClickButtonShare(this);
        rvData.setAdapter(adapterBadges);
        adapterSpinnerProgram = new AdapterSpinnerAddQueary(this, modelProgramList);
        sFilter.setAdapter(adapterSpinnerProgram);
        tvToolbarTitle.setText(getString(R.string.badges));
        programId = getIntent().getStringExtra(KEY);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        sFilter.setOnItemSelectedListener(this);

        loadSpinnerData();
        //loadBadgesData();
        //loadLeaderData();

    }

    @Override
    protected void onStart() {
        super.onStart();
        Glide.with(this)
                .load(SharePref.getLoginResponse(this).getUserProfileDetail().getPhoto()) // Uri of the picture
                .into(ivUserProfile);
        if (SharePref.getNotificationCount(this) != null) {
            tvNotificationCount.setText(SharePref.getNotificationCount(this));
        }
    }

    //Todo load leader data
    private void loadLeaderData() {
       // leaderModelList.clear();
      //  adapterLeaderPager.notifyDataSetChanged();
        vpLeader.setVisibility(View.GONE);
        showDialog(this);
        final String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();
        String userId = SharePref.getLoginResponse(this).getUserDetail().getId();
        Log.d(TAG, "loadBadgesData: user id ++> " + userId + " progarm===>  " + programId);
        Call<BadgesLederResponse> responseCall = apiInterface.doGetLeaderBadgesListByProgarm(accept,
                authorization,
                programId);
        responseCall.enqueue(new Callback<BadgesLederResponse>() {
            @Override
            public void onResponse(@NonNull Call<BadgesLederResponse> call, @NonNull Response<BadgesLederResponse> response) {
              //  Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));

                BaseActivity.hideDialog();
                if (response.code() == 200) {
                    if (response.body().getStatus().equals("true")) {
                        leaderModelList = new ArrayList<>();

                        leaderModelList.add(new LeaderModel("Fastest completion", response.body().getBadgesLeaderModel().getFastestActivityBadge().getName(), response.body().getBadgesLeaderModel().getFastestActivityBadge().getCount(), R.drawable.badge_fastestcompletion));
                        leaderModelList.add(new LeaderModel("Faculty Appreciation", response.body().getBadgesLeaderModel().getFacilitatorBadgeCount().getName(), response.body().getBadgesLeaderModel().getFacilitatorBadgeCount().getCount(), R.drawable.badge_appreciationfrom_faculty));
                        leaderModelList.add(new LeaderModel("Participant Appreciation", response.body().getBadgesLeaderModel().getParticipantBadgeCount().getName(), response.body().getBadgesLeaderModel().getParticipantBadgeCount().getCount(), R.drawable.badge_appreciationfrom_farticipation));
                        leaderModelList.add(new LeaderModel("Content rating", response.body().getBadgesLeaderModel().getContentRatingBadgeCount().getName(), response.body().getBadgesLeaderModel().getContentRatingBadgeCount().getCount(), R.drawable.badge_content_rating));
                        leaderModelList.add(new LeaderModel("Quiz completion", response.body().getBadgesLeaderModel().getQuizCompletionBadgeCount().getName(), response.body().getBadgesLeaderModel().getQuizCompletionBadgeCount().getCount(), R.drawable.badge_quiz_completion));
                        adapterLeaderPager = new AdapterLeaderPager(getApplicationContext(), leaderModelList);
                        vpLeader.setAdapter(adapterLeaderPager);
                     //   adapterLeaderPager.notifyDataSetChanged();
                        vpLeader.setVisibility(View.VISIBLE);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<BadgesLederResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                BaseActivity.hideDialog();
            }
        });


    }

    //Todo Load badges data
    private void loadBadgesData() {
        badgesModelList.clear();
        adapterBadges.notifyDataSetChanged();
        showDialog(this);

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();

        String userId = SharePref.getLoginResponse(this).getUserDetail().getId();

        Log.d(TAG, "loadBadgesData: user id ++> " + userId + " progarm===>  " + programId);


        Call<BadgesResponse> responseCall = apiInterface.doGetBadgesListByProgarm(accept,
                authorization,
                programId,
                userId);
        responseCall.enqueue(new Callback<BadgesResponse>() {
            @Override
            public void onResponse(@NonNull Call<BadgesResponse> call, @NonNull Response<BadgesResponse> response) {
                //  Log.d(TAG, "onResponse: " + response.headers());
                Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));
                BaseActivity.hideDialog();
                Log.d(TAG, "onResponse: " + response);

                if (response.code() == 200) {

                    if (response.body().getStatus().equals("true")) {
                        badgesModelList.add(new BadgesModel("Fastest completion", response.body().getBadgesModel().getFastest_activity_badge_count(), R.drawable.badge_fastestcompletion));
                        badgesModelList.add(new BadgesModel("Faculty Appreciation", response.body().getBadgesModel().getFacilitator_badge_count(), R.drawable.badge_appreciationfrom_faculty));
                        badgesModelList.add(new BadgesModel("Participant Appreciation", response.body().getBadgesModel().getParticipant_badge_count(), R.drawable.badge_appreciationfrom_farticipation));
                        badgesModelList.add(new BadgesModel("Content rating", response.body().getBadgesModel().getContent_rating_badge_count(), R.drawable.badge_content_rating));
                        badgesModelList.add(new BadgesModel("Quiz completion", response.body().getBadgesModel().getQuiz_completion_badge_count(), R.drawable.badge_quiz_completion));
                        adapterBadges.notifyDataSetChanged();

                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<BadgesResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                BaseActivity.hideDialog();
            }
        });


    }


    //Todo load data for spinner
    private void loadSpinnerData() {
        modelProgramList.add(new ProgramModel("Select"));
        modelProgramList.addAll(SharePref.getProgramResponse(this).getProgram_list());
        adapterSpinnerProgram.notifyDataSetChanged();
        if (modelProgramList.size() > 0) {
            //  selectValue(programId);
            selectDropDownValue(modelProgramList, programId);
        }

    }

    //todo set selected drop down value
    @Override
    public void selectedDropDownValue(int position) {
        super.selectedDropDownValue(position);
        sFilter.setSelection(position);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;

            case R.id.rlHome:
                // startActivity(new Intent(this, HomeActivity.class));
                finish();
                break;
            case R.id.rvSearch:
                startActivity(new Intent(this, ActivitySearch.class));
                break;
            case R.id.rlHelpDeskBottom:
                startActivity(new Intent(this, ActivityHelpDesk.class));
                break;
            case R.id.rlSetting:
                //Todo load my query fragment
                startActivity(new Intent(this, SettingActivity.class));
                break;

            case R.id.rlNotification:
                //Todo load my query fragment
                startActivity(new Intent(this, ActivityNotification.class));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


    //todo open bottom sheet dialog for sharing
    private void doOpenSheet(final String shareText) {
        Log.d(TAG, "doOpenSheet: ");
        View view = getLayoutInflater().inflate(R.layout.dailog_share, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);
        dialog.show();
        CustomButtonView bShare = dialog.findViewById(R.id.bShare);
        CustomButtonView bCancel = dialog.findViewById(R.id.bCancel);


        assert bShare != null;
        bShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });


        assert bCancel != null;
        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


    }


    //todo click on share button
    @Override
    public void setOnItemClickListener(int position) {
        String shareText = "I got " + badgesModelList.get(position).getCount() + " " + badgesModelList.get(position).getTitle() + " badges in Cognegix";
        doOpenSheet(shareText);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        if (i != 0) {
            programId = modelProgramList.get(i).getId();
            loadBadgesData();
            loadLeaderData();
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
