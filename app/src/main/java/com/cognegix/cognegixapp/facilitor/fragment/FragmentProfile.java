package com.cognegix.cognegixapp.facilitor.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.facilitor.activity.ActivityEditProfile;
import com.cognegix.cognegixapp.facilitor.activity.HomeActivity;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterBadgesPager;
import com.cognegix.cognegixapp.model.badges.ProfileBadgeModel;
import com.cognegix.cognegixapp.model.UserProfileResponse;
import com.cognegix.cognegixapp.model.badges.ProfileBadgesResponse;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentProfile extends Fragment implements View.OnClickListener {
    private static final String TAG = FragmentProfile.class.getSimpleName();
    View view;
    List<ProfileBadgeModel> profileBadgeModels;

    AdapterBadgesPager adapterBadgesPager;
    @BindView(R.id.tab_layout)
    TabLayout tab_layout;
    @BindView(R.id.vpBadges)
    ViewPager vpBadges;

    @BindView(R.id.tvProfileName)
    CustomTextView tvProfileName;
    @BindView(R.id.tvDesg)
    CustomTextView tvDesg;
    @BindView(R.id.tvCompany)
    CustomTextView tvCompany;

    @BindView(R.id.tvID)
    CustomTextView tvID;

    @BindView(R.id.ivArrowFacebbok)
    ImageView ivArrowFacebbok;
    @BindView(R.id.ivArrowTwitter)
    ImageView ivArrowTwitter;
    @BindView(R.id.ivArrowGooglePlus)
    ImageView ivArrowGooglePlus;
    @BindView(R.id.ivArrowLinkedin)
    ImageView ivArrowLinkedin;
    @BindView(R.id.ivArrowEmail)
    ImageView ivArrowEmail;


    @BindView(R.id.rlFacebook)
    RelativeLayout rlFacebook;
    @BindView(R.id.rlTwitter)
    RelativeLayout rlTwitter;
    @BindView(R.id.rlGooglePlus)
    RelativeLayout rlGooglePlus;
    @BindView(R.id.rlLinkedin)
    RelativeLayout rlLinkedin;
    @BindView(R.id.rlEmail)
    RelativeLayout rlEmail;

    @BindView(R.id.rlEditProfile)
    RelativeLayout rlEditProfile;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.tvProfileCompletionCount)
    CustomTextView tvProfileCompletionCount;
    @BindView(R.id.ivProfile)
    CircleImageView ivProfile;

    ApiInterface apiInterface;
    UserProfileResponse userProfileResponse;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        inti();
        return view;
    }

    private void inti() {
        rlFacebook.setOnClickListener(this);
        rlTwitter.setOnClickListener(this);
        rlGooglePlus.setOnClickListener(this);
        rlLinkedin.setOnClickListener(this);
        rlEmail.setOnClickListener(this);
        rlEditProfile.setOnClickListener(this);

        //tvProfileName.setText("Marco Fernandez");
        //tvDesg.setText("CEO & Co-founder");
        //tvCompany.setText("ICICI bank");

        profileBadgeModels = new ArrayList<>();
        adapterBadgesPager = new AdapterBadgesPager(getActivity(), profileBadgeModels);
        vpBadges.setAdapter(adapterBadgesPager);
        tab_layout.setupWithViewPager(vpBadges, true);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);


        doLoadBagesData();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    //Todo call api for badges count
    private void doLoadBagesData() {
      /*  profileBadgeModels.add(new ProfileBadgeModel("Fastest completion", "0", R.drawable.badge_fastestcompletion));
        profileBadgeModels.add(new ProfileBadgeModel("Faculty Appreciation", "0", R.drawable.badge_appreciationfrom_faculty));
        profileBadgeModels.add(new ProfileBadgeModel("Participant Appreciation", "0", R.drawable.badge_appreciationfrom_farticipation));
        profileBadgeModels.add(new ProfileBadgeModel("Content Rating", "0", R.drawable.badge_content_rating));
        profileBadgeModels.add(new ProfileBadgeModel("Quiz Completion", "0", R.drawable.badge_quiz_completion));
        adapterBadgesPager.notifyDataSetChanged();        */

        ((HomeActivity) getActivity()).showDialog(getActivity());

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(getContext()).getUserDetail().getApi_token();

        Call<ProfileBadgesResponse> responseCall = apiInterface.doGetBagdesForProfile(accept, authorization);
        responseCall.enqueue(new Callback<ProfileBadgesResponse>() {
            @Override
            public void onResponse(@NonNull Call<ProfileBadgesResponse> call, @NonNull Response<ProfileBadgesResponse> response) {
             //   Log.d(TAG, "onResponse:     " + response);
             ////   Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));
                BaseActivity.hideDialog();


                if (response.code() == 200) {
                    assert response.body() != null;
                    if (response.body().getStatus().equals("true")) {
                       // profileBadgeModels.clear();
                        profileBadgeModels.add(new ProfileBadgeModel("Fastest completion", response.body().getProfileBadgeModel().getFastestActivityBadge(), R.drawable.badge_fastestcompletion));
                        profileBadgeModels.add(new ProfileBadgeModel("Faculty Appreciation", response.body().getProfileBadgeModel().getFacilitatorBadgeCount(), R.drawable.badge_appreciationfrom_faculty));
                        profileBadgeModels.add(new ProfileBadgeModel("Participant Appreciation", response.body().getProfileBadgeModel().getParticipantBadgeCount(), R.drawable.badge_appreciationfrom_farticipation));
                        profileBadgeModels.add(new ProfileBadgeModel("Content Rating", response.body().getProfileBadgeModel().getContentRatingBadgeCount(), R.drawable.badge_content_rating));
                        profileBadgeModels.add(new ProfileBadgeModel("Quiz Completion", response.body().getProfileBadgeModel().getQuizCompletionBadgeCount(), R.drawable.badge_quiz_completion));
                        adapterBadgesPager.notifyDataSetChanged();

                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<ProfileBadgesResponse> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t);
                BaseActivity.hideDialog();
            }
        });


    }

    //Todo call api for set profile data
    private void doSetProfileData() {

        // SharePref.showDialog(getActivity());
        ((HomeActivity) getActivity()).showDialog(getActivity());

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(getContext()).getUserDetail().getApi_token();

        Call<UserProfileResponse> responseCall = apiInterface.doViewProfile(accept, authorization);
        responseCall.enqueue(new Callback<UserProfileResponse>() {
            @Override
            public void onResponse(@NonNull Call<UserProfileResponse> call, @NonNull Response<UserProfileResponse> response) {
            //    Log.d(TAG, "onResponse:     " + response);
             //   Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));
                BaseActivity.hideDialog();


                if (response.code() == 200) {
                    assert response.body() != null;
                    if (response.body().getStatus().equals("true")) {
                        userProfileResponse = response.body();
                        SharePref.setProfileResponse(getActivity(), userProfileResponse);
                        loadData();
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<UserProfileResponse> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t);
                BaseActivity.hideDialog();
            }
        });


    }

    private void loadData() {
        String name = userProfileResponse.getUserProfileDetail().getFirst_name() + " " + userProfileResponse.getUserProfileDetail().getLast_name();
        tvProfileName.setText(name);
        tvCompany.setText(userProfileResponse.getUserOrganizationDetail().getName());
        tvDesg.setText(userProfileResponse.getUserProfileDetail().getDesignation());
        String per = SharePref.getProfileResponse(getActivity()).getUserProfileDetail().getProfile_completion_percentage() + "%";
        tvProfileCompletionCount.setText(per);
        float progressPer = 0;
        try {
            //  int i = Integer.parseInt(input);
            progressPer = Float.parseFloat(SharePref.getProfileResponse(getActivity()).getUserProfileDetail().getProfile_completion_percentage());

        } catch (NumberFormatException ex) { // handle your exception
            Log.d(TAG, "NumberFormatException: " + ex);
        }
        progress.setProgress((int) progressPer);
        Glide.with(getActivity())
                .load(SharePref.getLoginResponse(getActivity()).getUserProfileDetail().getPhoto()) // Uri of the picture
                .into(ivProfile);
        setArrowText(1);

    }

    private void setArrowText(int selectedEmail) {

        switch (selectedEmail) {

            case 1:
                //for facebook
                tvID.setText(userProfileResponse.getUserProfileDetail().getFacebook_id());
                ivArrowFacebbok.setVisibility(View.VISIBLE);
                ivArrowTwitter.setVisibility(View.GONE);
                ivArrowGooglePlus.setVisibility(View.GONE);
                ivArrowLinkedin.setVisibility(View.GONE);
                ivArrowEmail.setVisibility(View.GONE);
                break;

            case 2:
                //for Twitter
                //tvID.setText("http://www.teitter.com/marco.fernandez");
                tvID.setText(userProfileResponse.getUserProfileDetail().getTwitter_handle());
                ivArrowFacebbok.setVisibility(View.GONE);
                ivArrowTwitter.setVisibility(View.VISIBLE);
                ivArrowGooglePlus.setVisibility(View.GONE);
                ivArrowLinkedin.setVisibility(View.GONE);
                ivArrowEmail.setVisibility(View.GONE);
                break;

            case 3:
                //for Google
                // tvID.setText("http://plus.google.com/marco.fernandez");
                tvID.setText(userProfileResponse.getUserProfileDetail().getGoogle_profile());
                ivArrowFacebbok.setVisibility(View.GONE);
                ivArrowTwitter.setVisibility(View.GONE);
                ivArrowGooglePlus.setVisibility(View.VISIBLE);
                ivArrowLinkedin.setVisibility(View.GONE);
                ivArrowEmail.setVisibility(View.GONE);
                break;


            case 4:
                //for Linkedin
//                tvID.setText("http://www.linkedin/marcofernandez");
                tvID.setText(userProfileResponse.getUserProfileDetail().getLinkedin_profile());
                ivArrowFacebbok.setVisibility(View.GONE);
                ivArrowTwitter.setVisibility(View.GONE);
                ivArrowGooglePlus.setVisibility(View.GONE);
                ivArrowLinkedin.setVisibility(View.VISIBLE);
                ivArrowEmail.setVisibility(View.GONE);
                break;


            case 5:
                //for Email
                //tvID.setText("marco.fernandez@gmail.com");
                tvID.setText(userProfileResponse.getUserProfileDetail().getEmail());
                ivArrowFacebbok.setVisibility(View.GONE);
                ivArrowTwitter.setVisibility(View.GONE);
                ivArrowGooglePlus.setVisibility(View.GONE);
                ivArrowLinkedin.setVisibility(View.GONE);
                ivArrowEmail.setVisibility(View.VISIBLE);
                break;


        }


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.rlFacebook:
                setArrowText(1);
                break;
            case R.id.rlTwitter:
                setArrowText(2);
                break;
            case R.id.rlGooglePlus:
                setArrowText(3);
                break;
            case R.id.rlLinkedin:
                setArrowText(4);
                break;
            case R.id.rlEmail:
                setArrowText(5);
                break;

            case R.id.rlEditProfile:
                startActivity(new Intent(getContext(), ActivityEditProfile.class));
                // getActivity().finish();

                break;


        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");

        if (SharePref.getLoginResponse(getActivity()) != null)
            Glide.with(getActivity())
                    .load(SharePref.getLoginResponse(getActivity()).getUserProfileDetail().getPhoto()) // Uri of the picture
                    .into(ivProfile);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
        doSetProfileData();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: ");
    }
}
