package com.cognegix.cognegixapp.facilitor.adpter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.model.LeaderModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class AdapterLeaderPager extends PagerAdapter {

    private Context context;
    private List<LeaderModel> leaderModelList;

    public AdapterLeaderPager(Context context, List<LeaderModel> leaderModelList) {
        this.context = context;
        this.leaderModelList = leaderModelList;
    }

    @Override
    public int getCount() {
        return leaderModelList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ViewHolder viewHolder;
        View view = LayoutInflater.from(context).inflate(R.layout.iteam_view_leaderboard_pager, container, false);
        viewHolder = new ViewHolder(view);
        ((ViewPager) container).addView(view);

        viewHolder.tvTitle.setText(leaderModelList.get(position).getActivity());
        viewHolder.tvLeaderName.setText(leaderModelList.get(position).getName());
        viewHolder.tvProfileCount.setText(leaderModelList.get(position).getCount());
        viewHolder.ivBadges.setImageResource(leaderModelList.get(position).getImagePath());
        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }


    class ViewHolder {


        @BindView(R.id.tvTitle)
        CustomTextView tvTitle;
        @BindView(R.id.tvProfileCount)
        CustomTextView tvProfileCount;
        @BindView(R.id.tvLeaderName)
        CustomTextView tvLeaderName;

        @BindView(R.id.ivProfile)
        CircleImageView ivProfile;

        @BindView(R.id.ivBadges)
        ImageView ivBadges;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);

        }


    }


}
