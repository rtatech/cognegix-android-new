package com.cognegix.cognegixapp.facilitor.adpter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.model.SearchModel;
import com.cognegix.cognegixapp.model.program.ProgramModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterSearch extends RecyclerView.Adapter<AdapterSearch.ViewHolder> {
    private Context context;
    private List<SearchModel> searchModelList;
    private ItemOnClickListener itemOnClickListener;

    public AdapterSearch(Context context, List<SearchModel> searchModelList) {
        this.context = context;
        this.searchModelList = searchModelList;
    }

    public void setItemClickListener(ItemOnClickListener itemOnClickListener) {
        this.itemOnClickListener = itemOnClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_search, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.tvTitle.setText(searchModelList.get(position).getProgram_name());
        holder.tvTitle.setText(searchModelList.get(position).getTopic_name());
        holder.ivFolder.setImageResource(R.drawable.round_folder);
        holder.ivFolder.setColorFilter(searchModelList.get(position).getColor());
        final int pos = position;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemOnClickListener.setOnItemClickListener(pos);
            }
        });

    }

    @Override
    public int getItemCount() {
        return searchModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        CustomTextView tvTitle;


        @BindView(R.id.ivFolder)
        ImageView ivFolder;
        @BindView(R.id.rlSide)
        RelativeLayout rlSide;
        @BindView(R.id.tvTopicName)
        CustomTextView tvTopicName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
