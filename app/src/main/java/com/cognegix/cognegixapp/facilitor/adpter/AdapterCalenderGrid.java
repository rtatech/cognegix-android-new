package com.cognegix.cognegixapp.facilitor.adpter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.model.program.UpComingModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AdapterCalenderGrid extends BaseAdapter {
    private final String TAG = AdapterCalenderGrid.class.getSimpleName();

    private Context context;
    private List<UpComingModel> upComingModelList;




    public AdapterCalenderGrid(Context context, List<UpComingModel> upComingModelList) {
        this.context = context;
        this.upComingModelList = upComingModelList;

    }

    public AdapterCalenderGrid(Context context) {
        this.context = context;
    }



    @Override
    public int getCount() {
        return upComingModelList.size();
    }

//     @Override
//     public DateTime getItem(int i) {
//         return dateTimeList.get(i);
//     }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_view_custom_calender, viewGroup, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();


        }
        viewHolder.rlMain.setTag(i);
        viewHolder.tvDate.setText(upComingModelList.get(i).getDate());
        viewHolder.tvDay.setText(upComingModelList.get(i).getDay());
        if (upComingModelList.get(i).getUpComingEventModelList().size() > 0) {
            viewHolder.viewLine.setVisibility(View.VISIBLE);
        } else {
            viewHolder.viewLine.setVisibility(View.GONE);
        }

        return view;
    }

    class ViewHolder {
        @BindView(R.id.tvDate)
        CustomTextView tvDate;
        @BindView(R.id.tvDay)
        CustomTextView tvDay;
        @BindView(R.id.viewLine)
        View viewLine;

        @BindView(R.id.rlMain)
        RelativeLayout rlMain;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);

        }
    }


}
