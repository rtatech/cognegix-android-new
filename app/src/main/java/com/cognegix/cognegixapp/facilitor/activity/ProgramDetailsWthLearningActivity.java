package com.cognegix.cognegixapp.facilitor.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.facilitor.fragment.FragmentProgram;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProgramDetailsWthLearningActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = ProgramDetailsWthLearningActivity.class.getSimpleName();
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;

    @BindView(R.id.rlSetting)
    RelativeLayout rlSetting;
    @BindView(R.id.rlHome)
    RelativeLayout rlHome;
    @BindView(R.id.rvSearch)
    RelativeLayout rvSearch;
    @BindView(R.id.rlHelpDeskBottom)
    RelativeLayout rlHelpDeskBottom;

    @BindView(R.id.wvDetails)
    WebView wvDetails;


    List<String> listUrls;
    boolean isBackUrl = false;
    // boolean isExUrl = false;
    boolean isYoutube = false;


    @Override

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_details_with_learning);
        init();
    }


    @SuppressLint("SetJavaScriptEnabled")
    private void init() {
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        tvToolbarTitle.setText(getString(R.string.learningTree));
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);
        rlSetting.setOnClickListener(this);
        rlHome.setOnClickListener(this);
        rvSearch.setOnClickListener(this);
        rlHelpDeskBottom.setOnClickListener(this);

        //http://cogvc.testursites.info/cgx-front/MLT_5_19

        wvDetails.getSettings().setJavaScriptEnabled(true);

        listUrls = new ArrayList<>();
        // wvDetails.loadUrl("http://cogvc.testursites.info/cgx-front/MLT_5_19");


        wvDetails.clearCache(true);
        wvDetails.clearHistory();
        wvDetails.getSettings().setBuiltInZoomControls(true);
        wvDetails.getSettings().setJavaScriptEnabled(true);
        wvDetails.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wvDetails.setWebViewClient(new WebViewClient());
        wvDetails.getSettings().setUseWideViewPort(true);
        wvDetails.getSettings().setLoadWithOverviewMode(true);
        wvDetails.getSettings().setJavaScriptEnabled(true);
        wvDetails.getSettings().setAllowFileAccess(true);
        wvDetails.getSettings().setPluginState(WebSettings.PluginState.ON_DEMAND);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            wvDetails.getSettings().setAllowUniversalAccessFromFileURLs(true);
            wvDetails.getSettings().setAllowFileAccessFromFileURLs(true);

        }
        //mWebView.getSettings().setTextZoom(percent);


        //wvDetails.loadUrl("http://59.94.34.59/LT/6_Nodes_Level2_With_Hammer.html");
        //  wvDetails.loadUrl("http://cogvc.testursites.info/cgx-front/MLT_32_0");
        // wvDetails.loadUrl("http://cogvc.testursites.info/cgx-front/MLT_32_1099");

        //base_url/program/learning/tree?tree_id=10&parent_node_title=Test&parent_node_id=0
        String url = "http://cogvc.testursites.info/cgx-front/public/index.php/api/" + SharePref.getProgramModel(this).getId() + "/program/learning_tree";

        String newUrl = ApiClient.getClient().baseUrl() +
                "program/learning/tree?tree_id=" + SharePref.getProgramModel(this).getId() +
                "&parent_node_title=" + SharePref.getProgramModel(this).getName() +
                "&parent_node_id=0" +
                "&user_id=" + SharePref.getLoginResponse(this).getUserDetail().getId();


        wvDetails.loadUrl(newUrl);

        wvDetails.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                showDialog(ProgramDetailsWthLearningActivity.this);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                hideDialog();

            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                Log.d(TAG, "dd shouldOverrideUrlLoading: " + url);

                if (url.toLowerCase().contains(ApiClient.LEARNING_TREE_LINK.toLowerCase())) {
                    Log.d(TAG, "shouldOverrideUrlLoading:  true");
                    isBackUrl = true;
                    // listUrls.add(url);
                }

                if (url.toLowerCase().contains(ApiClient.LEARNING_TREE_LINK_ADD.toLowerCase())) {
                    listUrls.add(url);
                }


                if (url.toLowerCase().contains(".pdf".toLowerCase()) ||
                        url.toLowerCase().contains(".doc".toLowerCase()) ||
                        url.toLowerCase().contains(".ashx".toLowerCase()) ||
                        url.toLowerCase().contains(".docx".toLowerCase()) ||
                        url.toLowerCase().contains(".xls".toLowerCase()) ||
                        url.toLowerCase().contains(".xlsx".toLowerCase()) ||
                        url.toLowerCase().contains(".ppt".toLowerCase()) ||
                        url.toLowerCase().contains(".pptx".toLowerCase()) ||
                        url.toLowerCase().contains(".txt".toLowerCase())) {

                    view.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url="
                            + url);

                    Log.d(TAG, "dd doc: ");
                } else {

                    Log.d(TAG, "dd not doc: ");

                    view.loadUrl(url);
                }
                return true;
            }

            public void onLoadResource(WebView view, String url) {
                Log.d(TAG, "onLoadResource: url === >  " + url);

               /* String search  = "mediaserver/document";

                if (url.toLowerCase().contains(search.toLowerCase())) {

                    System.out.println("I found the keyword");

                    wvDetails.loadUrl("http://docs.google.com/gview?embedded=true&url="
                                     +url);
                } else {

                    System.out.println("not found");

                }*/


            }


        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;

            case R.id.rlSetting:
                //Todo load my query fragment
                startActivity(new Intent(this, SettingActivity.class));
                break;
            case R.id.rlHome:
                // startActivity(new Intent(this, HomeActivity.class));
                finish();
                break;
            case R.id.rvSearch:
                startActivity(new Intent(this, ActivitySearch.class));
                break;
            case R.id.rlHelpDeskBottom:
                startActivity(new Intent(this, ActivityHelpDesk.class));
                break;

        }
    }

    @Override
    public void onBackPressed() {

        if (isBackUrl) {
            isBackUrl = false;

            String url = listUrls.get(listUrls.size() - 1);
            wvDetails.loadUrl(url);
            Log.d(TAG, "onBackPressed:  " + url);


        } else {
            super.onBackPressed();
            startActivity(new Intent(this, ActivityProgramDetail.class));
            finish();
        }

    }
}
