package com.cognegix.cognegixapp.facilitor.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomEditTextView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterSpinnerAddQueary;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterSpinnerProgramAskToPerson;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterSpinnerProgramTopic;
import com.cognegix.cognegixapp.model.ForgotResponse;
import com.cognegix.cognegixapp.model.ProgarmTopicResponse;
import com.cognegix.cognegixapp.model.ProgramPersonToAskModel;
import com.cognegix.cognegixapp.model.ProgramTopicModel;
import com.cognegix.cognegixapp.model.program.ProgramModel;
import com.cognegix.cognegixapp.model.program.ProgramResponse;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityAddMyQuery extends BaseActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    public static final String KEY_PROGRAM_ID = "programId";
    private static final String TAG = ActivityAddMyQuery.class.getSimpleName();
    @BindView(R.id.sToAsk)
    AppCompatSpinner sToAsk;
    @BindView(R.id.sTopic)
    AppCompatSpinner sTopic;
    @BindView(R.id.sProgram)
    AppCompatSpinner sProgram;
    @BindView(R.id.etQuery)
    CustomEditTextView etQuery;
    @BindView(R.id.bPost)
    CustomButtonView bPost;
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    List<ProgramPersonToAskModel> programPersonToAskModelList;

    AdapterSpinnerAddQueary adapterSpinnerProgram;
    AdapterSpinnerProgramTopic adapterSpinnerProgramTopic;
    AdapterSpinnerProgramAskToPerson adapterSpinnerProgramAskToPerson;


    String programID;
    List<ProgramModel> modelProgramList;
    List<ProgramTopicModel> programTopicModelList;

    ApiInterface apiInterface;

    String paramProramID;
    String paramTopicId;
    String paramPersonASkId;
    String receiver_program_user_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_my_query);
        init();
    }

    //do for init varible
    private void init() {
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        bPost.setOnClickListener(this);
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);

        tvToolbarTitle.setText(getString(R.string.myQuery));
        modelProgramList = new ArrayList<>();
        programTopicModelList = new ArrayList<>();
        programPersonToAskModelList = new ArrayList<>();
        adapterSpinnerProgram = new AdapterSpinnerAddQueary(this, modelProgramList);
        adapterSpinnerProgramTopic = new AdapterSpinnerProgramTopic(this, programTopicModelList);
        adapterSpinnerProgramAskToPerson = new AdapterSpinnerProgramAskToPerson(this, programPersonToAskModelList);
        programID = getIntent().getStringExtra(KEY_PROGRAM_ID);
        sProgram.setAdapter(adapterSpinnerProgram);
        sToAsk.setAdapter(adapterSpinnerProgramAskToPerson);
        sTopic.setAdapter(adapterSpinnerProgramTopic);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        sProgram.setOnItemSelectedListener(this);
        sToAsk.setOnItemSelectedListener(this);
        sTopic.setOnItemSelectedListener(this);

        loadProgarmData();
    }


    // do call api for get program data
    private void loadProgarmData() {
        modelProgramList.add(new ProgramModel("Select"));
        showDialog(this);

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();

        Call<ProgramResponse> responseCall = apiInterface.doProgarmListMain(accept,
                authorization);
        responseCall.enqueue(new Callback<ProgramResponse>() {
            @Override
            public void onResponse(@NonNull Call<ProgramResponse> call, @NonNull Response<ProgramResponse> response) {
                hideDialog();
                if (response.code() == 200) {

                    if (response.body().getStatus().equals("true")) {
                        modelProgramList.addAll(response.body().getProgram_list());
                        adapterSpinnerProgram.notifyDataSetChanged();

                        selectDropDownValue(modelProgramList, programID);
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<ProgramResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;

            case R.id.bPost:

                if (isValidate()) {
                    doPostThread();

                }

                break;


        }


    }

    //todo call post api
    private void doPostThread() {

        showDialog(this);

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();

        Call<ForgotResponse> responseCall = apiInterface.doCreateQueryThread(accept,
                authorization,
                paramProramID,
                paramTopicId,
                paramPersonASkId,
                etQuery.getText().toString().trim(),
                SharePref.getProgramUserId(this),
                receiver_program_user_id);

        Log.d(TAG, "param: " + new Gson().toJson(responseCall.request().body()));


        responseCall.enqueue(new Callback<ForgotResponse>() {
            @Override
            public void onResponse(@NonNull Call<ForgotResponse> call, @NonNull Response<ForgotResponse> response) {
                hideDialog();
                //      Log.d(TAG, "onResponse: " + response.body().getStatus());

                if (response.code() == 200) {

                    if (response.body().getStatus().equals("true")) {
                        onBackPressed();

                    } else {
                        showAlertDialog(ActivityAddMyQuery.this, "Fail", response.body().getMessage());

                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<ForgotResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });


    }

    //todo do validation for add
    private boolean isValidate() {
        boolean isValiadate = true;
        if (sProgram.getSelectedItemPosition() == 0) {
            showAlertDialog(this, "Select Program", "Select program first");
            isValiadate = false;
        } else {

            if (sToAsk.getSelectedItemPosition() == 0) {
                showAlertDialog(this, "Select Person To Ask", "Select program person to ask");
                isValiadate = false;
            } else if (etQuery.getText().toString().trim().equals("")) {
                showAlertDialog(this, "Question", "Enter Question");
                isValiadate = false;
            }

        }

        return isValiadate;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        Log.d(TAG, "onItemSelected: outer ");

        AppCompatSpinner spinner = (AppCompatSpinner) adapterView;
        switch (spinner.getId()) {

            case R.id.sProgram:
                if (i != 0) {
                    Log.d(TAG, "onItemSelected: inner");
                    doGetTopicAndAskPersonData(modelProgramList.get(i).getId());
                    paramProramID = modelProgramList.get(i).getId();
                }
                break;

            case R.id.sTopic:
                if (i != 0) {
                    paramTopicId = programTopicModelList.get(i).getTopic_id();
                }
                break;
            case R.id.sToAsk:
                if (i != 0) {
                    paramPersonASkId = programPersonToAskModelList.get(i).getId();
                    receiver_program_user_id = programPersonToAskModelList.get(i).getProgram_user_id();

                }
                break;

        }

    }


    //todo call api for topic and person to ask
    private void doGetTopicAndAskPersonData(String programID) {
        Log.d(TAG, "doGetTopicAndAskPersonData: ");
        showDialog(this);
        programTopicModelList.clear();
        programPersonToAskModelList.clear();

        programTopicModelList.add(new ProgramTopicModel("Select"));
        programPersonToAskModelList.add(new ProgramPersonToAskModel("Select"));

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();

        Call<ProgarmTopicResponse> responseCall = apiInterface.doGetProgramTopicForQuery(accept,
                authorization,
                programID);
        responseCall.enqueue(new Callback<ProgarmTopicResponse>() {
            @Override
            public void onResponse(@NonNull Call<ProgarmTopicResponse> call, @NonNull Response<ProgarmTopicResponse> response) {
                //  Log.d(TAG, "onResponse: " + response.headers());
                //  Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));
                hideDialog();
                //  Log.d(TAG, "onResponse: " + response);

                if (response.code() == 200) {
                    if (response.body().getStatus().equals("true")) {
                        programTopicModelList.addAll(response.body().getProgramTopicModelList());
                        programPersonToAskModelList.addAll(response.body().getProgramPersonToAskModelList());
                        adapterSpinnerProgramTopic.notifyDataSetChanged();
                        adapterSpinnerProgramAskToPerson.notifyDataSetChanged();
                        sTopic.setSelection(0);
                        sToAsk.setSelection(0);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ProgarmTopicResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(this, MyQueryActivity.class);
        intent.putExtra(MyQueryActivity.KEY, programID);
        startActivity(intent);
        finish();
    }

    //todo set drop value by defult select

    @Override
    public void selectedDropDownValue(int position) {
        super.selectedDropDownValue(position);
        sProgram.setSelection(position);
    }


}
