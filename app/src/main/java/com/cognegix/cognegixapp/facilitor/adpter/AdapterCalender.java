package com.cognegix.cognegixapp.facilitor.adpter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.interfaces.OnClickCalender;
import com.cognegix.cognegixapp.model.program.UpComingModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterCalender extends PagerAdapter {
    private Context context;
    private List<List<UpComingModel>> lists;
    private OnClickCalender onClickCalender;

    public AdapterCalender(Context context, List<List<UpComingModel>> lists) {
        this.context = context;
        this.lists = lists;
    }


    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View) object);


    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        AdapterCalender.ViewHolder viewHolder;
        View view = LayoutInflater.from(context).inflate(R.layout.item_calender, container, false);
        viewHolder = new AdapterCalender.ViewHolder(view);
        ((ViewPager) container).addView(view);
        viewHolder.gvCustomCalender.setAdapter(new AdapterCalenderGrid(context, lists.get(position)));

        viewHolder.gvCustomCalender.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d("AdapterCa", "onItemClick:  " + lists.get(position).get(i).getDay());
                onClickCalender.onClickItemCalender(lists.get(position).get(i));
            }
        });
        return view;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }


    class ViewHolder {
        @BindView(R.id.gvCustomCalender)
        GridView gvCustomCalender;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);

        }
    }

    public void onClickCalnder(OnClickCalender onClickCalender) {
        this.onClickCalender = onClickCalender;
    }


}
