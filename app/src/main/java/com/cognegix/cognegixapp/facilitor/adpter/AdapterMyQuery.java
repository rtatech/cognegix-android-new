package com.cognegix.cognegixapp.facilitor.adpter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.model.MyQueryThreadModel;

import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AdapterMyQuery extends RecyclerView.Adapter<AdapterMyQuery.ViewHolder> {

    private Context context;
    private List<MyQueryThreadModel> myQueryThreadModelList;

    private ItemOnClickListener itemOnClickListener;
    private String role = "";

    public AdapterMyQuery(Context context, List<MyQueryThreadModel> myQueryThreadModelList) {
        this.context = context;
        this.myQueryThreadModelList = myQueryThreadModelList;
    }

    public void setOnItemListners(ItemOnClickListener itemOnClickListener) {
        this.itemOnClickListener = itemOnClickListener;
    }

    public void setRole(String role) {
        this.role = role;
        notifyDataSetChanged();

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_my_query, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvDate.setText(formatedDate(myQueryThreadModelList.get(position).getCreated_at()));
        holder.tvTitle.setText(myQueryThreadModelList.get(position).getProgram_name());
        holder.tvTopic.setText(myQueryThreadModelList.get(position).getTopic_name());
        holder.tvAskTo.setText(myQueryThreadModelList.get(position).getReceiver_name());
        holder.bView.setTag(position);

        if (role.equalsIgnoreCase("Participant")) {
            holder.tvAskedLable.setText(context.getString(R.string.Askto));
        } else {
            holder.tvAskedLable.setText(context.getString(R.string.AskBy));
        }
    }


    @Override
    public int getItemCount() {
        return myQueryThreadModelList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        CustomTextView tvTitle;
        @BindView(R.id.tvDate)
        CustomTextView tvDate;
        @BindView(R.id.tvTopic)
        CustomTextView tvTopic;
        @BindView(R.id.tvAskTo)
        CustomTextView tvAskTo;

        @BindView(R.id.tvAskedLable)
        CustomTextView tvAskedLable;

        @BindView(R.id.bView)
        CustomButtonView bView;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            bView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int) view.getTag();
                    itemOnClickListener.setOnItemClickListener(pos);

                }
            });
        }

    }

    @SuppressLint("SimpleDateFormat")
    private String formatedDate(String date) {
        if (!date.equals("")) {
            String forDate = null;
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                java.util.Date date1 = formatter.parse(date);
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");

                forDate = format.format(date1);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            return forDate;
        }
        // 10th March 2018

        return null;
    }

}
