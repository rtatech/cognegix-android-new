package com.cognegix.cognegixapp.facilitor.adpter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AdapterListAssgin extends BaseAdapter {
    private Context context;
    private List<String> stringList;

    public AdapterListAssgin(Context context, List<String> stringList) {
        this.context = context;
        this.stringList = stringList;
    }

    @Override
    public int getCount() {
        return stringList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_view_assgin, viewGroup, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.tvTitle.setText(stringList.get(i));

        return view;
    }


    class ViewHolder {
        @BindView(R.id.tvTitle)
        CustomTextView tvTitle;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

    }
}
