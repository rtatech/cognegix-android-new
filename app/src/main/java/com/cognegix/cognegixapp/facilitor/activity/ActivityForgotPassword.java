package com.cognegix.cognegixapp.facilitor.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomEditTextView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.model.ForgotResponse;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityForgotPassword extends BaseActivity implements View.OnClickListener {
    private static final String TAG = ActivityForgotPassword.class.getSimpleName();

    //todo Create refeance veriable

    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.bSubmit)
    CustomButtonView bSubmit;

    @BindView(R.id.etForgotPassword)
    CustomEditTextView etForgotPassword;


    //Todo create variable
    String email;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        inti();
    }

    private void inti() {
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        bSubmit.setOnClickListener(this);
        tvToolbarTitle.setText(getString(R.string.forgotPassword));
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;

            case R.id.bSubmit:

                if (isValidate()) {
                    doCallApiForForgotPassword();
                }
                break;

        }
    }


    //todo call api for forgot password
    private void doCallApiForForgotPassword() {
        showDialog(this);
        Call<ForgotResponse> responseCall = apiInterface.doForgotPassword(etForgotPassword.getText().toString().trim());


        Log.d(TAG, "doCallApiForForgotPassword: = = = " + responseCall.request().body());

        responseCall.enqueue(new Callback<ForgotResponse>() {

            @Override
            public void onResponse(@NonNull Call<ForgotResponse> call, @NonNull Response<ForgotResponse> response) {
                //  Log.d(TAG, "onResponse:     " + response.body().getStatus() + "===kffkdkb=====" + response.body().getMessage());
                hideDialog();
              //  Log.d(TAG, "onResponse: " + response);

                if (response.code() == 200) {

                    if (response.body().getStatus().equalsIgnoreCase("true")) {
                        // startActivity(new Intent(getApplicationContext(), ActivityForgotPasswordWithOtp.class));
                        Intent intent = new Intent(getApplicationContext(), ActivityForgotPasswordWithOtp.class);
                        intent.putExtra(ActivityForgotPasswordWithOtp.KEY, email);
                        startActivity(intent);

                    } else {

                        showAlertDialog(ActivityForgotPassword.this, "Forgot password", response.body().getMessage());

                    }

                }


            }

            @Override
            public void onFailure(@NonNull Call<ForgotResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });


    }


    //Todo handle back press event
    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    //todo for store variable for api
    void initVariable() {
        email = etForgotPassword.getText().toString().trim();
    }

    //todo check validation for email
    boolean isValidate() {
        boolean isValidate = true;
        initVariable();

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etForgotPassword.setError("Enter Valid Email Address");
            isValidate = false;
        }

        return isValidate;
    }


}
