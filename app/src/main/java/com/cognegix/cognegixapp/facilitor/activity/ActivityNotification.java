package com.cognegix.cognegixapp.facilitor.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterNotification;
import com.cognegix.cognegixapp.facilitor.fragment.FragmentNotification;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.interfaces.OnClickNotification;
import com.cognegix.cognegixapp.model.ModelNotification;
import com.cognegix.cognegixapp.model.NotificationReponseModel;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityNotification extends BaseActivity implements View.OnClickListener, OnClickNotification {
    private static final String TAG = ActivityNotification.class.getSimpleName();
    View view;
    @BindView(R.id.rvData)
    RecyclerView rvData;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;

    //Todo create variable
    AdapterNotification adapterNotification;
    List<ModelNotification> modelNotificationList;
    ApiInterface apiInterface;


    private void init() {
        Log.d(TAG, "init: ");
        //Todo all intiazaltion
        ibBack.setOnClickListener(this);
        tvToolbarTitle.setText(getString(R.string.notification));
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayout.VERTICAL, false);
        rvData.setLayoutManager(linearLayoutManager);
        modelNotificationList = new ArrayList<>();
        adapterNotification = new AdapterNotification(this);
        adapterNotification.setItemOnClickListener(this);
        rvData.setAdapter(adapterNotification);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        //  loadData();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        init();

    }

    @Override
    protected void onStart() {
        super.onStart();
        loadData();
    }

    //Todo load notification data
    private void loadData() {
        //  modelNotificationList.add(new ModelNotification("New program Understanding of 'Negotiation Skills' is about to start this week.", "Posted by Facilitator", "1 day ago"));
        // modelNotificationList.add(new ModelNotification("You have successfully completed topic 'Identifying objectives and building agendas'", "Posted by Facilitator", "1 day ago"));
        // adapterNotification.notifyDataSetChanged();

        adapterNotification.clearList();
        //getActivity().showDialog();
        //showDialog(getActivity());
        showDialog(this);

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();


        Call<NotificationReponseModel> responseCall = apiInterface.doNotiifcationList(accept, authorization,
                "all");


        Log.d(TAG, "doCallApiForForgotPassword: = = = " + responseCall.request().body());

        responseCall.enqueue(new Callback<NotificationReponseModel>() {

            @Override
            public void onResponse(@NonNull Call<NotificationReponseModel> call, @NonNull Response<NotificationReponseModel> response) {
                BaseActivity.hideDialog();//  Log.d(TAG, "onResponse:     " + response.body().getStatus() + "===kffkdkb=====" + response.body().getMessage());
              //  Log.d(TAG, "onResponse: " + response);

                if (response.code() == 200) {
                    if (response.body().getStatus().equals("true")) {

                        adapterNotification.setList(response.body().getModelNotificationList());
                        modelNotificationList.addAll(response.body().getModelNotificationList());
                        //  adapterNotification.notifyDataSetChanged();

                    }

                }
            }


            @Override
            public void onFailure(@NonNull Call<NotificationReponseModel> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                BaseActivity.hideDialog();
            }
        });


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;
        }
    }


    @Override
    public void onClickView(ModelNotification modelNotification) {
        if (modelNotification.getIs_video_call().equalsIgnoreCase("1")) {
           /* Intent intent = new Intent(getActivity(), ActivityVideoCall.class);
            intent.putExtra(ActivityVideoCall.KEY, modelNotificationList.get(position).getVideo_call_url());
            intent.putExtra(ActivityVideoCall.NOTIFICATION, modelNotificationList.get(position).getId());
            startActivity(intent);*/

            String url = modelNotification.getVideo_call_url();
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        } else {
            // Log.d(TAG, "setOnItemClickListener: nvlnx xc=== " + position);
            Intent intent = new Intent(this, ActivityNotificationDetails.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(ActivityNotificationDetails.KEY_ID, modelNotification);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }
}
