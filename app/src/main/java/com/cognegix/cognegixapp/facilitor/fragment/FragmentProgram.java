package com.cognegix.cognegixapp.facilitor.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.facilitor.activity.ActivitySearch;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterCalender;
import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.facilitor.activity.ActivityMessageBox;
import com.cognegix.cognegixapp.facilitor.activity.ActivityProgramDetail;
import com.cognegix.cognegixapp.facilitor.activity.HomeActivity;
import com.cognegix.cognegixapp.facilitor.activity.LuanchDetailActivity;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterActivityCalenderPager;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterAlertPager;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterMessageBoxPager;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterProgramPager;
import com.cognegix.cognegixapp.interfaces.ButtonOnClickListener;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.interfaces.OnButtonLuanchClickListener;
import com.cognegix.cognegixapp.interfaces.OnClickCalender;
import com.cognegix.cognegixapp.model.program.BoardMessagesModel;
import com.cognegix.cognegixapp.model.program.ProgramModel;
import com.cognegix.cognegixapp.model.program.ProgramResponse;
import com.cognegix.cognegixapp.model.program.UpComingModel;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;
import com.google.gson.Gson;

import org.joda.time.DateTime;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentProgram extends Fragment implements View.OnClickListener, ItemOnClickListener, ButtonOnClickListener, OnClickCalender, OnButtonLuanchClickListener {
    //Todo declare all variable and widgets
    private static final String TAG = FragmentProgram.class.getSimpleName();
    View view;
    AdapterProgramPager adapterProgramPager;
    @BindView(R.id.vpProgram)
    ViewPager vpProgram;
    @BindView(R.id.tlProgram)
    TabLayout tlProgram;


    private DateTime date;
    @BindView(R.id.tvDate)
    CustomTextView tvDate;
    @BindView(R.id.tvMonth)
    CustomTextView tvMonth;

    @BindView(R.id.vpCalenderActivity)
    ViewPager vpCalenderActivity;
    @BindView(R.id.tlActivityCal)
    TabLayout tlActivityCal;
    AdapterActivityCalenderPager adapterActivityCalenderPager;

    @BindView(R.id.ibPrevios)
    ImageView ibPrevios;
    @BindView(R.id.ibNext)
    ImageView ibNext;

    @BindView(R.id.vpAlert)    ViewPager vpAlert;
    @BindView(R.id.tlAlert)    TabLayout tlAlert;
    AdapterAlertPager adapterAlertPager;
    @BindView(R.id.vpMsgBox)
    ViewPager vpMsgBox;
    @BindView(R.id.tlMsgBox)
    TabLayout tlMsgBox;
    @BindView(R.id.vpCalender)
    ViewPager vpCalender;
    @BindView(R.id.tlCalender)
    TabLayout tlCalender;

    @BindView(R.id.rlAlert)
    RelativeLayout rlAlert;

    @BindView(R.id.ibClose)
    ImageButton ibClose;


    AdapterMessageBoxPager adapterMessageBoxPager;
    ApiInterface apiInterface;
    List<BoardMessagesModel> messageBoardModelList;

    //11-6-2018
    List<ProgramModel> modelProgramList;
    List<UpComingModel> upComingModelList;
    ProgramResponse programResponse;
    List<List<UpComingModel>> calnderListForPager;
    AdapterCalender adapterCalender;
    List<String> alertList;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fregment_program_facilotor, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // init();
    }

    private void init() {
        ibPrevios.setOnClickListener(this);
        ibNext.setOnClickListener(this);
        ibClose.setOnClickListener(this);

        messageBoardModelList = new ArrayList<>();
        //upComingModelList = new ArrayList<>();
        calnderListForPager = new ArrayList<>();
        date = new DateTime();
        adapterCalender = new AdapterCalender(getActivity(), calnderListForPager);
        modelProgramList = new ArrayList<>();
        adapterProgramPager = new AdapterProgramPager(getContext(), modelProgramList);
        adapterProgramPager.setOnClickButtonShare(this);

        vpCalenderActivity.setVisibility(View.GONE);
        tlActivityCal.setVisibility(View.GONE);

        adapterCalender.onClickCalnder(this);
        vpCalender.setAdapter(adapterCalender);
        tlCalender.setupWithViewPager(vpCalender, true);

        vpProgram.setAdapter(adapterProgramPager);
        tlProgram.setupWithViewPager(vpProgram, true);

        alertList = new ArrayList<>();
        adapterAlertPager = new AdapterAlertPager(getContext(), alertList);
        vpAlert.setAdapter(adapterAlertPager);
        tlAlert.setupWithViewPager(vpAlert, true);

        adapterMessageBoxPager = new AdapterMessageBoxPager(getContext(), messageBoardModelList);
        adapterMessageBoxPager.setOnButtonClick(this);
        vpMsgBox.setAdapter(adapterMessageBoxPager);
        tlMsgBox.setupWithViewPager(vpMsgBox, true);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Log.d(TAG, "init: " + date.getMonthOfYear());
        tvDate.setText(String.valueOf(date.getDayOfMonth()));
        tvMonth.setText(getMonth(date.getMonthOfYear()));
        doLoadProgramData();

        //  vpCalenderActivity.setVisibility(View.GONE);

    }

    private void doLoadProgramData() {
        Log.d(TAG, "doLoadProgramData: ");
        adapterProgramPager.notifyDataSetChanged();

        // SharePref.showDialog(getActivity());
        ((HomeActivity) getActivity()).showDialog(getActivity());

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(getActivity()).getUserDetail().getApi_token();


        Call<ProgramResponse> responseCall = apiInterface.doProgarmListMain(accept, authorization);

        responseCall.enqueue(new Callback<ProgramResponse>() {
            @Override
            public void onResponse(@NonNull Call<ProgramResponse> call, @NonNull Response<ProgramResponse> response) {
                //  Log.d(TAG, "onResponse: " + response.headers());
              //  Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));

            //    Log.d(TAG, "activity calnder:=>> " + new Gson().toJson(response.body().getUpComingModelList()));


                BaseActivity.hideDialog();
                Log.d(TAG, "onResponse: " + response);

                if (response.code() == 200) {

                    if (response.body().getStatus().equals("true")) {
                        programResponse = response.body();
                        messageBoardModelList.addAll(response.body().getBoard_messages());
                        modelProgramList.addAll(response.body().getProgram_list());
                        adapterProgramPager.notifyDataSetChanged();
                        adapterMessageBoxPager.notifyDataSetChanged();
                        SharePref.setNotificationCount(getActivity(), response.body().getUnread_notification_count());
                        alertList.addAll(response.body().getCritical_alerts());
                        adapterAlertPager.notifyDataSetChanged();
                        //  upComingModelList.addAll(response.body().getUpComingModelList());
                        // adapterCalenderGrid.notifyDataSetChanged();
                        doLoadDataCalData();
                        doLoadDefultEvent();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<ProgramResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                BaseActivity.hideDialog();
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibPrevios:
                vpProgram.setCurrentItem(getItem(-1), true);
                break;
            case R.id.ibNext:
                vpProgram.setCurrentItem(getItem(+1), true);
                break;
            case R.id.ibClose:
                rlAlert.setVisibility(View.GONE);
                break;
        }
    }


    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }

    @Override
    public void setOnItemClickListener(int position) {
        SharePref.setProgramModel(getActivity(), modelProgramList.get(position));

        startActivity(new Intent(getContext(), ActivityProgramDetail.class));
        // getActivity().finish();
    }


    private int getItem(int i) {
        return vpProgram.getCurrentItem() + i;
    }

    @Override
    public void setOnButtonClickListener(int position) {
        Intent intent = new Intent(getActivity(), ActivityMessageBox.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(ActivityMessageBox.KEY, messageBoardModelList.get(position));
        intent.putExtras(bundle);
        startActivity(intent);

//        startActivity(new Intent(getActivity(), ActivityMessageBox.class));

    }

    private void doLoadDataCalData() {
        int elementsPerGroup = 7;

        for (int i = 0; i < programResponse.getUpComingModelList().size(); i++) {

            if (i % elementsPerGroup == 0) {
                // upComingTestList = new ArrayList<>();
                upComingModelList = new ArrayList<>();

                calnderListForPager.add(upComingModelList);
            } else {
                upComingModelList = calnderListForPager.get(calnderListForPager.size() - 1);
            }
            upComingModelList.add(programResponse.getUpComingModelList().get(i));
        }
        adapterCalender.notifyDataSetChanged();


    }


    private void doLoadDefultEvent() {
        for (int i = 0; i < programResponse.getUpComingModelList().size(); i++) {

            if (programResponse.getUpComingModelList().get(i).getUpComingEventModelList().size() > 0) {


                Log.d(TAG, "doLoadDataCalData:  pos  = " + i);

                Log.d(TAG, "doLoadDataCalData:  event si  " + programResponse.getUpComingModelList().get(i).getUpComingEventModelList().size());
                adapterActivityCalenderPager = new AdapterActivityCalenderPager(getContext(), programResponse.getUpComingModelList().get(i).getUpComingEventModelList());
                adapterActivityCalenderPager.setOnClickLuanchButton(this);
                vpCalenderActivity.setAdapter(adapterActivityCalenderPager);
                tlActivityCal.setupWithViewPager(vpCalenderActivity, true);
                vpCalenderActivity.setVisibility(View.VISIBLE);
                tlActivityCal.setVisibility(View.VISIBLE);
                break;
            }


        }

    }


    @Override
    public void onClickItemCalender(UpComingModel upComingModel) {
        tvDate.setText(upComingModel.getDate());
        tvMonth.setText(upComingModel.getMonth());
        if (upComingModel.getUpComingEventModelList().size() > 0) {
            adapterActivityCalenderPager = new AdapterActivityCalenderPager(getContext(), upComingModel.getUpComingEventModelList());
            adapterActivityCalenderPager.setOnClickLuanchButton(this);
            vpCalenderActivity.setAdapter(adapterActivityCalenderPager);
            tlActivityCal.setupWithViewPager(vpCalenderActivity, true);
            vpCalenderActivity.setVisibility(View.VISIBLE);
            tlActivityCal.setVisibility(View.VISIBLE);
        } else {
            vpCalenderActivity.setVisibility(View.GONE);
            tlActivityCal.setVisibility(View.GONE);
        }
    }

    @Override
    public void setOnLuanchButtonClickListener(String url) {
        Intent intent = new Intent(getActivity(), LuanchDetailActivity.class);
        intent.putExtra(LuanchDetailActivity.KEY, url);
        LuanchDetailActivity.isPDF = false;
        ActivitySearch.isActivitySearch = false;
        startActivity(intent);
    }
}
