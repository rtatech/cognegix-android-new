package com.cognegix.cognegixapp.facilitor.fragment;

import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.facilitor.activity.HomeActivity;
import com.cognegix.cognegixapp.facilitor.activity.MyQueryActivity;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterBadgesMain;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.model.program.AllProgramResponse;
import com.cognegix.cognegixapp.model.program.ProgramModel;
import com.cognegix.cognegixapp.model.program.ProgramResponse;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMyQuery extends Fragment implements ItemOnClickListener {
    private static final String TAG = FragmentMyQuery.class.getSimpleName();
    View view;
    @BindView(R.id.rvData)
    RecyclerView rvData;
    AdapterBadgesMain adapterBadgesMain;

    List<ProgramModel> modelProgramList;
    ApiInterface apiInterface;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_mydocument, container, false);
        init();
        return view;
    }


    //Todo create all ui initialize all ui
    private void init() {
        ButterKnife.bind(this, view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayout.VERTICAL, false);
        rvData.setLayoutManager(linearLayoutManager);
        rvData.setLayoutManager(linearLayoutManager);
        modelProgramList = new ArrayList<>();
        adapterBadgesMain = new AdapterBadgesMain(getContext(), modelProgramList);
        adapterBadgesMain.SetItemClickListener(this);
        rvData.setAdapter(adapterBadgesMain);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        loadData();
    }


    //Todo load program data
    private void loadData() {


        // SharePref.showDialog(getActivity());
        ((HomeActivity) getActivity()).showDialog(getActivity());

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(getActivity()).getUserDetail().getApi_token();

        Call<AllProgramResponse> responseCall = apiInterface.doFeatchProgramList(accept, authorization);
        responseCall.enqueue(new Callback<AllProgramResponse>() {
            @Override
            public void onResponse(@NonNull Call<AllProgramResponse> call, @NonNull Response<AllProgramResponse> response) {
                //  Log.d(TAG, "onResponse: " + response.headers());
                // Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));
                BaseActivity.hideDialog();
                //  Log.d(TAG, "onResponse: " + response);

                if (response.code() == 200) {

                    if (response.body().getStatus().equals("true")) {
                        modelProgramList.addAll(response.body().getProgram_list());
                        adapterBadgesMain.notifyDataSetChanged();
                        // SharePref.setNotificationCount(getActivity(), response.body().getUnread_notification_count());
                        // SharePref.setAllProgramResponse(getActivity(), response.body());
                        SharePref.setProgramResponse(getActivity(), response.body());

                        doAddColor();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<AllProgramResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                BaseActivity.hideDialog();
            }
        });


    }

    private void doAddColor() {

        for (int i = 0; i < modelProgramList.size(); i++) {

            //    modelProgramList.addAll(new modelProgramList.get(i).setImagePath(getRandomMaterialColor("500")));)

            modelProgramList.get(i).setColor(getRandomMaterialColor());
        }
        adapterBadgesMain.notifyDataSetChanged();


    }


    //Todo set random color
    private int getRandomMaterialColor() {
        int returnColor = Color.GRAY;
        int arrayId = getResources().getIdentifier("mdcolor_" + "400", "array", getActivity().getPackageName());

        if (arrayId != 0) {
            TypedArray colors = getResources().obtainTypedArray(arrayId);
            int index = (int) (Math.random() * colors.length());
            returnColor = colors.getColor(index, Color.GRAY);
            colors.recycle();
        }
        return returnColor;
    }


    @Override
    public void setOnItemClickListener(int position) {

        Log.d(TAG, "setOnItemClickListener: " + modelProgramList.get(position).getProgram_user_id());
        SharePref.setProgramUserId(getActivity(), modelProgramList.get(position).getProgram_user_id());
        Intent intent = new Intent(getContext(), MyQueryActivity.class);
        intent.putExtra(MyQueryActivity.KEY, modelProgramList.get(position).getId());
        startActivity(intent);
    }
}
