package com.cognegix.cognegixapp.facilitor.activity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomEditTextView;
import com.cognegix.cognegixapp.customview.CustomTextInput;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterBadgesPager;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterListAssgin;
import com.cognegix.cognegixapp.model.AssingBadgesResponse;
import com.cognegix.cognegixapp.model.ModelParticipant;
import com.cognegix.cognegixapp.model.AssingBadgesResponse;
import com.cognegix.cognegixapp.model.badges.ProfileBadgeModel;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityParticipantProfile extends BaseActivity implements View.OnClickListener {
    private static final String TAG = ActivityParticipantProfile.class.getSimpleName();
    public static final String KEY = "keyParticipantProfile";

    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    @BindView(R.id.ivToolMenu)
    ImageButton ivToolMenu;

    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    List<ProfileBadgeModel> profileBadgeModels;
    AdapterBadgesPager adapterBadgesPager;
    @BindView(R.id.tab_layout)
    TabLayout tab_layout;
    @BindView(R.id.vpBadges)
    ViewPager vpBadges;
    @BindView(R.id.tvProfileName)
    CustomTextView tvProfileName;
    @BindView(R.id.tvDesg)
    CustomTextView tvDesg;
    @BindView(R.id.tvCompany)
    CustomTextView tvCompany;
    List<String> stringList;
    AdapterListAssgin adapterListAssgin;
    @BindView(R.id.rlAssign)
    RelativeLayout rlAssign;

    @BindView(R.id.rlMainBadges)
    RelativeLayout rlMainBadges;


    @BindView(R.id.tvID)
    CustomTextView tvID;

    @BindView(R.id.ivArrowFacebbok)
    ImageView ivArrowFacebbok;
    @BindView(R.id.ivArrowTwitter)
    ImageView ivArrowTwitter;
    @BindView(R.id.ivArrowGooglePlus)
    ImageView ivArrowGooglePlus;
    @BindView(R.id.ivArrowLinkedin)
    ImageView ivArrowLinkedin;
    @BindView(R.id.ivArrowEmail)
    ImageView ivArrowEmail;


    @BindView(R.id.rlFacebook)
    RelativeLayout rlFacebook;
    @BindView(R.id.rlTwitter)
    RelativeLayout rlTwitter;
    @BindView(R.id.rlGooglePlus)
    RelativeLayout rlGooglePlus;
    @BindView(R.id.rlLinkedin)
    RelativeLayout rlLinkedin;
    @BindView(R.id.rlEmail)
    RelativeLayout rlEmail;


    @BindView(R.id.ivProfile)
    CircleImageView ivProfile;
    ModelParticipant modelParticipant;
    ApiInterface apiInterface;
    String badges;
    boolean isApiCount = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_participant_profile);

        init();
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this);
        tvToolbarTitle.setText(getString(R.string.participantList));
        ivToolMenu.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        ivToolMenu.setOnClickListener(this);
        rlAssign.setOnClickListener(this);
        rlFacebook.setOnClickListener(this);
        rlTwitter.setOnClickListener(this);
        rlGooglePlus.setOnClickListener(this);
        rlLinkedin.setOnClickListener(this);
        rlEmail.setOnClickListener(this);
        tvToolbarTitle.setText(getString(R.string.profile));

        if (getIntent().getSerializableExtra(KEY) != null) {
            modelParticipant = (ModelParticipant) getIntent().getSerializableExtra(KEY);
        }

        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);

        String name = modelParticipant.getFirst_name() + " " + modelParticipant.getLast_name();
        tvProfileName.setText(name);
        tvDesg.setText(modelParticipant.getDesignation());
        if (modelParticipant.getPhoto() != null) {
            Glide.with(this).load(modelParticipant.getPhoto()).into(ivProfile);
        }
        tvCompany.setText(modelParticipant.getOrganization_name());
        profileBadgeModels = new ArrayList<>();
        adapterBadgesPager = new AdapterBadgesPager(this, profileBadgeModels);
        vpBadges.setAdapter(adapterBadgesPager);
        tab_layout.setupWithViewPager(vpBadges, true);
        adapterListAssgin = new AdapterListAssgin(this, stringList);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        if (modelParticipant.getIs_private().equalsIgnoreCase("0")) {
            //public
            rlMainBadges.setVisibility(View.VISIBLE);
            loadData();
            setArrowText(1);
        } else {
            //private
            rlMainBadges.setVisibility(View.GONE);
        }


    }

    private void loadData() {
        profileBadgeModels.add(new ProfileBadgeModel("Fastest completion", modelParticipant.getProfileBadgeModel().getFastestActivityBadge(), R.drawable.badge_fastestcompletion));
        profileBadgeModels.add(new ProfileBadgeModel("Faculty Appreciation", modelParticipant.getProfileBadgeModel().getFacilitatorBadgeCount(), R.drawable.badge_appreciationfrom_faculty));
        profileBadgeModels.add(new ProfileBadgeModel("Participant Appreciation", modelParticipant.getProfileBadgeModel().getParticipantBadgeCount(), R.drawable.badge_appreciationfrom_farticipation));
        profileBadgeModels.add(new ProfileBadgeModel("Content Rating", modelParticipant.getProfileBadgeModel().getContentRatingBadgeCount(), R.drawable.badge_content_rating));
        profileBadgeModels.add(new ProfileBadgeModel("Quiz Completion", modelParticipant.getProfileBadgeModel().getQuizCompletionBadgeCount(), R.drawable.badge_quiz_completion));
        adapterBadgesPager.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.ivToolMenu:
                onBackPressed();
                break;

            case R.id.rlAssign:
                doOpenDialog();
                break;

            case R.id.rlFacebook:
                setArrowText(1);
                break;
            case R.id.rlTwitter:
                setArrowText(2);
                break;
            case R.id.rlGooglePlus:
                setArrowText(3);
                break;
            case R.id.rlLinkedin:
                setArrowText(4);
                break;
            case R.id.rlEmail:
                setArrowText(5);
                break;


        }
    }

    private void doOpenDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dailog_view_assing_badges);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        CustomButtonView bAssign = dialog.findViewById(R.id.bAssign);
        CustomButtonView bCancel = dialog.findViewById(R.id.bCancel);
        // ListView lvData = dialog.findViewById(R.id.lvData);
        final CustomTextInput inlQuantity = dialog.findViewById(R.id.inlQuantity);
        final CustomEditTextView etQuantity = dialog.findViewById(R.id.etQuantity);
        final CustomTextView tvQuantity = dialog.findViewById(R.id.tvQuantity);
        dialog.show();

        // lvData.setAdapter(adapterListAssgin);
        Log.d(TAG, "doOpenDialog: " + badges);

        if (SharePref.getProgramModel(getApplicationContext()).getUser_role().equalsIgnoreCase("Participant")) {
            if (isApiCount) {
                String limit = "Current Limit: " + badges;
                tvQuantity.setText(limit);

            } else {
                String limit = "Current Limit: " + SharePref.getRemeainingBadges(getApplicationContext());
                tvQuantity.setText(limit);
            }
        } else {
            tvQuantity.setText("");
        }


        bAssign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String quantity = etQuantity.getText().toString().trim();

                if (quantity.equals("")) {
                    inlQuantity.setError("Enter quantity");
                } else {
                    if (quantity.equals("0")) {
                        inlQuantity.setError("Quantity should be grater than zero");
                    } else {
                        dialog.dismiss();
                        inlQuantity.setError("");
                        doDistributeBadgeForPartcipant(quantity);
                    }
                }
            }
        });
        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }


    private void doDistributeBadgeForPartcipant(String quantity) {


        showDialog(this);

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();

        Log.d(TAG, "doDistributeBadgeForPartcipant: "  + authorization);

        Log.d(TAG, "doDistributeBadgeForPartcipant: p+>>>" + SharePref.getProgramModel(this).getId());
        Log.d(TAG, "doDistributeBadgeForPartcipant:q +>>>" + quantity);
        Log.d(TAG, "doDistributeBadgeForPartcipant: pid+>>>" + modelParticipant.getUser_id());

        Call<AssingBadgesResponse> responseCall = apiInterface.doDistributeBadgeForPartcipant(accept, authorization,
                SharePref.getProgramModel(this).getId(),
                quantity,
                modelParticipant.getUser_id());



        responseCall.enqueue(new Callback<AssingBadgesResponse>() {

            @Override
            public void onResponse(@NonNull Call<AssingBadgesResponse> call, @NonNull Response<AssingBadgesResponse> response) {
                BaseActivity.hideDialog();//  Log.d(TAG, "onResponse:     " + response.body().getStatus() + "===kffkdkb=====" + response.body().getMessage());
             //   Log.d(TAG, "onResponse:  ++ " + new Gson().toJson(response));
                if (response.code() == 200) {
                    if (response.body().getStatus().equals("true")) {
                        showAlertDialog(ActivityParticipantProfile.this, "Participant", response.body().getMessage());
                        // tvQuantity.setText(response.body().getRemaining_distribution_badges());
                        badges = response.body().getRemaining_distribution_badges();
                        isApiCount = true;

                        SharePref.setRemeainingBadges(getApplicationContext(), badges);
                        Log.d(TAG, "onResponse: badges count  ===  " + badges);

                    } else {
                        showAlertDialog(ActivityParticipantProfile.this, "Participant", response.body().getError());
                    }

                }
            }


            @Override
            public void onFailure(@NonNull Call<AssingBadgesResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                BaseActivity.hideDialog();
            }
        });


    }


    private void setArrowText(int selectedEmail) {

        switch (selectedEmail) {

            case 1:
                //for facebook
                tvID.setText(modelParticipant.getFacebook_id());
                ivArrowFacebbok.setVisibility(View.VISIBLE);
                ivArrowTwitter.setVisibility(View.GONE);
                ivArrowGooglePlus.setVisibility(View.GONE);
                ivArrowLinkedin.setVisibility(View.GONE);
                ivArrowEmail.setVisibility(View.GONE);
                break;

            case 2:
                //for Twitter
                //tvID.setText("http://www.teitter.com/marco.fernandez");
                tvID.setText(modelParticipant.getTwitter_handle());
                ivArrowFacebbok.setVisibility(View.GONE);
                ivArrowTwitter.setVisibility(View.VISIBLE);
                ivArrowGooglePlus.setVisibility(View.GONE);
                ivArrowLinkedin.setVisibility(View.GONE);
                ivArrowEmail.setVisibility(View.GONE);
                break;

            case 3:
                //for Google
                // tvID.setText("http://plus.google.com/marco.fernandez");
                tvID.setText(modelParticipant.getGoogle_profile());
                ivArrowFacebbok.setVisibility(View.GONE);
                ivArrowTwitter.setVisibility(View.GONE);
                ivArrowGooglePlus.setVisibility(View.VISIBLE);
                ivArrowLinkedin.setVisibility(View.GONE);
                ivArrowEmail.setVisibility(View.GONE);
                break;


            case 4:
                //for Linkedin
//                tvID.setText("http://www.linkedin/marcofernandez");
                tvID.setText(modelParticipant.getLinkedin_profile());
                ivArrowFacebbok.setVisibility(View.GONE);
                ivArrowTwitter.setVisibility(View.GONE);
                ivArrowGooglePlus.setVisibility(View.GONE);
                ivArrowLinkedin.setVisibility(View.VISIBLE);
                ivArrowEmail.setVisibility(View.GONE);
                break;


            case 5:
                //for Email
                //tvID.setText("marco.fernandez@gmail.com");
                tvID.setText(modelParticipant.getEmail());
                ivArrowFacebbok.setVisibility(View.GONE);
                ivArrowTwitter.setVisibility(View.GONE);
                ivArrowGooglePlus.setVisibility(View.GONE);
                ivArrowLinkedin.setVisibility(View.GONE);
                ivArrowEmail.setVisibility(View.VISIBLE);
                break;


        }


    }

}
