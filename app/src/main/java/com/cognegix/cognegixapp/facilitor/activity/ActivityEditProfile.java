package com.cognegix.cognegixapp.facilitor.activity;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomEditTextView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.model.loginresponse.LoginReponces;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;
import com.google.gson.Gson;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityEditProfile extends BaseActivity implements View.OnClickListener {
    private static final String TAG = ActivityEditProfile.class.getSimpleName();
    @BindView(R.id.ivToolMenu)
    ImageButton ivToolMenu;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.etFirstName)
    CustomEditTextView etFirstName;
    @BindView(R.id.etLastName)
    CustomEditTextView etLastName;
    @BindView(R.id.etFacebook)
    CustomEditTextView etFacebook;
    @BindView(R.id.etTwitter)
    CustomEditTextView etTwitter;
    @BindView(R.id.etGooglePlus)
    CustomEditTextView etGooglePlus;
    @BindView(R.id.etLinkedin)
    CustomEditTextView etLinkedin;
    @BindView(R.id.etOutlook)
    CustomEditTextView etOutlook;
    @BindView(R.id.etDesignation)
    CustomEditTextView etDesignation;
    @BindView(R.id.etSecondaryEmail)
    CustomEditTextView etPersonalEmail;
    @BindView(R.id.etMobileNumber)
    CustomEditTextView etMobileNumber;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    @BindView(R.id.rlProfile)
    RelativeLayout rlProfile;


    @BindView(R.id.etCompany)
    CustomEditTextView etCompany;

    @BindView(R.id.etUserId)
    CustomEditTextView etUserId;


    @BindView(R.id.etWorkPhone)
    CustomEditTextView etWorkPhone;


    @BindView(R.id.rlEditProfile)
    RelativeLayout rlEditProfile;

    @BindView(R.id.tbGender)
    ToggleButton tbGender;

    @BindView(R.id.ivProfile)
    CircleImageView ivProfile;

    //for camara
    private static final int REQUEST_CAMARA = 1;
    private static final int REQUEST_GALARY = 2;
    private static final String ACCESS_STORAGE = android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private static final String CAMERA = android.Manifest.permission.CAMERA;
    private static final int PERMISSION = 3;
    Uri imageUri;
    String gender;
    String firstName;
    String lastName;
    String designation;
    String facebookLink;
    String twitterLink;
    String googlePlusLink;
    String email;
    String secondaryEmail;
    String personalPhone;
    String workPhoneNumber;
    String linkedin;
    ApiInterface apiInterface;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!checkPermission()) {
                requestPermission();
            }
        }
        init();

    }

    //Todo lets start coding on create
    private void init() {
        ButterKnife.bind(this);
        tvToolbarTitle.setText(getString(R.string.EditProfile));
        ivToolMenu.setOnClickListener(this);
        ivToolMenu.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        rlProfile.setOnClickListener(this);
        rlEditProfile.setOnClickListener(this);
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);
        etCompany.setClickable(false);
        etCompany.setEnabled(false);

        etUserId.setClickable(false);
        etUserId.setEnabled(false);


        if (SharePref.getProfileResponse(this).getUserProfileDetail().getGender().equals("")) {
            gender = "M";
        } else {
            gender = SharePref.getProfileResponse(this).getUserProfileDetail().getGender();
        }

        tbGender.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) {
                    gender = "F";
                    Log.d(TAG, "onCheckedChanged:   checked =>>" + gender);

                } else {
                    gender = "M";
                    Log.d(TAG, "onCheckedChanged: un checked+>>" + gender);

                }

            }
        });
        apiInterface = ApiClient.getClient().create(ApiInterface.class);


        doSetProfileData();

    }

    private void doSetProfileData() {
        etCompany.setText(SharePref.getProfileResponse(this).getUserOrganizationDetail().getName());
        etFirstName.setText(SharePref.getProfileResponse(this).getUserProfileDetail().getFirst_name());
        etLastName.setText(SharePref.getProfileResponse(this).getUserProfileDetail().getLast_name());
        etDesignation.setText(SharePref.getProfileResponse(this).getUserProfileDetail().getDesignation());
        etFacebook.setText(SharePref.getProfileResponse(this).getUserProfileDetail().getFacebook_id());
        etTwitter.setText(SharePref.getProfileResponse(this).getUserProfileDetail().getTwitter_handle());
        etGooglePlus.setText(SharePref.getProfileResponse(this).getUserProfileDetail().getGoogle_profile());
        etLinkedin.setText(SharePref.getProfileResponse(this).getUserProfileDetail().getLinkedin_profile());
        etOutlook.setText(SharePref.getProfileResponse(this).getUserProfileDetail().getEmail());
        etMobileNumber.setText(SharePref.getProfileResponse(this).getUserProfileDetail().getHome_phone_number());
        etWorkPhone.setText(SharePref.getProfileResponse(this).getUserProfileDetail().getWork_phone_number());
        etPersonalEmail.setText(SharePref.getProfileResponse(this).getUserProfileDetail().getPersonal_email());
        etUserId.setText(SharePref.getLoginResponse(this).getUserDetail().getUserId());

        Glide.with(this)
                .load(SharePref.getLoginResponse(this).getUserProfileDetail().getPhoto()) // Uri of the picture
                .into(ivProfile);


        if (SharePref.getProfileResponse(this).getUserProfileDetail().getGender().equals("F")) {
            tbGender.setChecked(true);
        } else {
            tbGender.setChecked(false);
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivToolMenu:

                onBackPressed();
                break;
            case R.id.rlProfile:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkPermission()) {
                        selectImageDailog();
                    } else {
                        requestPermission();
                    }
                } else {
                    selectImageDailog();
                }
                break;
            case R.id.rlEditProfile:
                if (isValid()) {
                    doUpdateProfile();

                }


                break;

        }

    }

    //Todo call update profile api
    private void doUpdateProfile() {
        showDialog(this);

        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();
       /* Log.d(TAG, "doUpdateProfile:params  ==> gender => " + gender + "firstName==>>"
                + firstName + "lastName  =>>" + lastName + "email=>>" + email
                + "secondaryEmail:" + secondaryEmail + "linkedin:" + linkedin +
                ",facebookLink:" + facebookLink + ",twitterLink:" + twitterLink + ",designation:" + designation +
                ",googlePlusLink:" + googlePlusLink);*/


        Call<LoginReponces> responseCall = apiInterface.doEditProfile(accept, authorization,
                firstName, lastName, gender, linkedin,
                facebookLink, twitterLink, googlePlusLink,
                designation, secondaryEmail, email, workPhoneNumber, personalPhone);


        Log.d(TAG, "doUpdateProfile:  params ===> " + responseCall.request().body().toString());
        Log.d(TAG, "doUpdateProfile:  params ===> " + new Gson().toJson(responseCall.request().body()));

        responseCall.enqueue(new Callback<LoginReponces>() {

            @Override
            public void onResponse(@NonNull Call<LoginReponces> call, @NonNull Response<LoginReponces> response) {
                //    Log.d(TAG, "onResponse:     " + response.body().getStatus() + "===kffkdkb=====" + response.body().getMessage());
                //SharePref.hideDialog();
                hideDialog();
                //   Log.d(TAG, "onResponse: " + response.headers());
                //   Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));

                if (response.code() == 200) {
                    if (response.body().getStatus().equals("true")) {
                        SharePref.setLoginResponse(getApplicationContext(), response.body());
                        showAlertDialog(ActivityEditProfile.this, "Profile update", response.body().getMessage());
                    } else {
                        showAlertDialog(ActivityEditProfile.this, "Profile update", response.body().getMessage());

                    }


                }


            }

            @Override
            public void onFailure(@NonNull Call<LoginReponces> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });
    }


    //Todo ope alert fo image pick
    private void selectImageDailog() {
        Log.d("lo", "selectImageDailog: ");
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                //camara
                if (options[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    // File file = getOutputMediaFile(1);
                    ContentValues values = new ContentValues(1);
                    values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                    imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri); // set the image file
                    startActivityForResult(intent, REQUEST_CAMARA);

                }
                //galary
                else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, REQUEST_GALARY);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }

        });
        builder.show();

    }


    public void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_STORAGE, CAMERA}, PERMISSION);
    }

    private boolean checkPermission() {
        int accessStorage = ContextCompat.checkSelfPermission(this, ACCESS_STORAGE);
        int camera = ContextCompat.checkSelfPermission(this, CAMERA);
        return accessStorage == PackageManager.PERMISSION_GRANTED &&
                camera == PackageManager.PERMISSION_GRANTED;
    }


    void initVariable() {
        firstName = etFirstName.getText().toString().trim();
        lastName = etLastName.getText().toString().trim();
        designation = etDesignation.getText().toString().trim();
        facebookLink = etFacebook.getText().toString().trim();
        twitterLink = etTwitter.getText().toString().trim();
        googlePlusLink = etGooglePlus.getText().toString().trim();
        email = etOutlook.getText().toString().trim();
        secondaryEmail = etPersonalEmail.getText().toString().trim();
        personalPhone = etMobileNumber.getText().toString().trim();
        workPhoneNumber = etWorkPhone.getText().toString().trim();
        linkedin = etLinkedin.getText().toString().trim();
    }


    private boolean isValid() {
        boolean isValidate = true;
        initVariable();

        if (firstName.equals("")) {
            etFirstName.setError("Enter First Name");
            isValidate = false;
        }

        if (lastName.equals("")) {
            etLastName.setError("Enter Last Name");
            isValidate = false;
        }
        // if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
        if (email.equals("")) {
            etOutlook.setError("Enter Valid Email Address");
            isValidate = false;
        }
     /*   if (secondaryEmail.equals("")) {
            etPersonalEmail.setError("Enter Valid Email Address");
            isValidate = false;
        }*/

        if (SharePref.getProfileResponse(this).getUserProfileDetail().getFacebook_id() != null) {
            if (facebookLink.equals("")) {
                etFacebook.setError("Enter Facebook link");
                isValidate = false;
            }
        }

        if (SharePref.getProfileResponse(this).getUserProfileDetail().getTwitter_handle() != null) {
            if (twitterLink.equals("")) {
                etTwitter.setError("Enter twitter link");
                isValidate = false;
            }
        }


        if (SharePref.getProfileResponse(this).getUserProfileDetail().getLinkedin_profile() != null) {
            if (linkedin.equals("")) {
                etLinkedin.setError("Enter linkedin link");
                isValidate = false;

            }
        }

        if (SharePref.getProfileResponse(this).getUserProfileDetail().getGoogle_profile() != null) {
            if (googlePlusLink.equals("")) {
                etGooglePlus.setError("Enter GooglePlus link");
                isValidate = false;

            }
        }


        if (workPhoneNumber.equals("")) {
            etWorkPhone.setError("Enter Work Phone number");
            isValidate = false;
        }
        return isValidate;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        switch (requestCode) {
            case REQUEST_CAMARA:
                if (resultCode == RESULT_OK) {
                    String path = getPathFromURI(imageUri);
                    File file = new File(path);
                    File compressImageFile = Compressor.getDefault(this).compressToFile(file);

                    Glide.with(this)
                            .load(compressImageFile) // Uri of the picture
                            .into(ivProfile);


                    doUploadPhoto();

                }
                break;
            case REQUEST_GALARY:
                if (resultCode == RESULT_OK) {
                    imageUri = data.getData();

                    String path = getPathFromURI(imageUri);
                    File file = new File(path);
                    File compressImageFile = Compressor.getDefault(this).compressToFile(file);

                    Glide.with(this)
                            .load(compressImageFile) // Uri of the picture
                            .into(ivProfile);
                    doUploadPhoto();


                }
                break;
        }


    }


    //todo call api for upload image as file
    private void doUploadPhoto() {
//        showDialog();
        showDialog(this);
        String path = getPathFromURI(imageUri);
        File file = new File(path);
        File compressImageFile = Compressor.getDefault(this).compressToFile(file);
        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), compressImageFile);
        MultipartBody.Part body = MultipartBody.Part.createFormData("photo", compressImageFile.getName(), requestFile);
        Call<LoginReponces> responseCall = apiInterface.doUploadImage(accept, authorization, body);
        responseCall.enqueue(new Callback<LoginReponces>() {

            @Override
            public void onResponse(@NonNull Call<LoginReponces> call, @NonNull Response<LoginReponces> response) {
//                SharePref.hideDialog();
                hideDialog();
                //  Log.d(TAG, "onResponse:     " + response.body().getStatus() + "===kffkdkb=====" + response.body().getMessage());
                //  Log.d(TAG, "onResponse: " + response);
                //   Log.d(TAG, "onResponse:=>> " + new Gson().toJson(response));
                if (response.code() == 200) {
                    if (response.body().getStatus().equals("true")) {
                        SharePref.setLoginResponse(getApplicationContext(), response.body());

                        Log.d(TAG, "onResponse:=>>  " + response.body().getUserProfileDetail().getPhoto());
                        Glide.with(getApplicationContext())
                                .load(response.body().getUserProfileDetail().getPhoto()) // Uri of the picture
                                .into(ivProfile);

                        //  SharePref.setProfileResponse()
                    }


                }

            }


            @Override
            public void onFailure(@NonNull Call<LoginReponces> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });


    }

    private String getPathFromURI(Uri contentUri) {
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        assert cursor != null;
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();
        cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();
        return path;
    }

    @Override
    public void doneDialog() {
        super.doneDialog();
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //   startActivity(new Intent(this, HomeActivity.class));
        // isEditAcivity = true;
        // finish();
    }
}
