package com.cognegix.cognegixapp.facilitor.adpter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.model.badges.BadgesModel;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterBadges extends RecyclerView.Adapter<AdapterBadges.ViewHolder> {
    private Context context;
    private ItemOnClickListener itemOnClickListener;
    List<BadgesModel> badgesModelList;

    public AdapterBadges(Context context, List<BadgesModel> badgesModelList) {
        this.context = context;
        this.badgesModelList = badgesModelList;
    }

    public void setOnClickButtonShare(ItemOnClickListener itemOnClickListener) {
        this.itemOnClickListener = itemOnClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_badges, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.tvBadgeTitle.setText(badgesModelList.get(position).getTitle());

        holder.tvProfileCount.setText(badgesModelList.get(position).getCount());

        holder.bShare.setTag(position);

        holder.ivBadges.setImageResource(badgesModelList.get(position).getImagePath());


    }

    @Override
    public int getItemCount() {
        return badgesModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvBadgeTitle)
        CustomTextView tvBadgeTitle;
        @BindView(R.id.tvProfileCount)
        CustomTextView tvProfileCount;


        @BindView(R.id.ivBadges)
        CircleImageView ivBadges;
        @BindView(R.id.bShare)
        CustomButtonView bShare;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            bShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int) view.getTag();
                    itemOnClickListener.setOnItemClickListener(pos);
                }
            });
        }
    }
}
