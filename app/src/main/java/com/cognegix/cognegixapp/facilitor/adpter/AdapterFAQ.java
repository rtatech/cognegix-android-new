package com.cognegix.cognegixapp.facilitor.adpter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.interfaces.OnButtonLuanchClickListener;
import com.cognegix.cognegixapp.model.FAQModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AdapterFAQ extends RecyclerView.Adapter<AdapterFAQ.ViewHolder> {

    private Context context;
    private List<FAQModel> faqModelList;

    ItemOnClickListener itemOnClickListener;

    public AdapterFAQ(Context context, List<FAQModel> faqModelList) {
        this.context = context;
        this.faqModelList = faqModelList;
    }


    public void onClickHeader(ItemOnClickListener itemOnClickListener) {
        this.itemOnClickListener = itemOnClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_faq_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d("adpter", "onBindViewHolder: " + faqModelList.get(position).isSelectde());
        holder.tvHelpDeskHeader.setText(faqModelList.get(position).getTitle());
        holder.tvHelpDeskChild.setText(faqModelList.get(position).getDescription());
        holder.rlHeader.setTag(position);

        if (faqModelList.get(position).isSelectde()) {
            holder.ivDownArrow.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
            holder.rlHeader.setBackgroundResource(R.drawable.back_top_text_help_half_round);
            holder.rlChild.setVisibility(View.VISIBLE);
        } else {
            holder.ivDownArrow.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
            holder.rlHeader.setBackgroundResource(R.drawable.back_top_text_help);
            holder.rlChild.setVisibility(View.GONE);
        }
    }


    @Override
    public int getItemCount() {
        return faqModelList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvHelpDeskHeader)
        CustomTextView tvHelpDeskHeader;

        @BindView(R.id.tvHelpDeskChild)
        CustomTextView tvHelpDeskChild;
        @BindView(R.id.rlChild)
        RelativeLayout rlChild;
        @BindView(R.id.rlHeader)
        RelativeLayout rlHeader;
        @BindView(R.id.ivDownArrow)
        ImageView ivDownArrow;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


            rlHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int) view.getTag();
                    itemOnClickListener.setOnItemClickListener(pos);
                }
            });


        }

    }


}
