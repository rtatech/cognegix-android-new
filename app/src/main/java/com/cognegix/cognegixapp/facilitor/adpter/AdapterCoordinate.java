package com.cognegix.cognegixapp.facilitor.adpter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.model.badges.BadgesModel;
import com.cognegix.cognegixapp.model.program.ProgramCoordinatorsModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterCoordinate extends PagerAdapter {
    private Context context;
    private List<ProgramCoordinatorsModel> programCoordinatorsModelList = new ArrayList<>();


    public AdapterCoordinate(Context context) {
        this.context = context;
    }


    @Override
    public int getCount() {
        return programCoordinatorsModelList.size();

    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ViewHolder holder;
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_program_cordinate, container, false);
        holder = new ViewHolder(view);
        ((ViewPager) container).addView(view);
        holder.tvProgramCoordinatorContactEmail.setText(programCoordinatorsModelList.get(position).getEmail());
        holder.tvProgramCoordinatorContactNumber.setText(programCoordinatorsModelList.get(position).getContact());
        holder.tvProgramCoordinatorName.setText(programCoordinatorsModelList.get(position).getName());

        return view;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View) object);
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvProgramCoordinatorName)
        CustomTextView tvProgramCoordinatorName;
        @BindView(R.id.tvProgramCoordinatorContactNumber)
        CustomTextView tvProgramCoordinatorContactNumber;
        @BindView(R.id.tvProgramCoordinatorContactEmail)
        CustomTextView tvProgramCoordinatorContactEmail;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void setProgramCoordinatorsModelList(List<ProgramCoordinatorsModel> programCoordinatorsModelList) {
        this.programCoordinatorsModelList = programCoordinatorsModelList;
        notifyDataSetChanged();
    }

    public void clearList() {
        programCoordinatorsModelList.clear();
        notifyDataSetChanged();
    }

}
