package com.cognegix.cognegixapp.facilitor.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.facilitor.adpter.AdapterForumInter;
import com.cognegix.cognegixapp.forum.ForumActivity;
import com.cognegix.cognegixapp.model.ForumInterModel;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.model.ForumInterResponse;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForumInterMediatorActivity extends BaseActivity implements View.OnClickListener, ItemOnClickListener {
    private static final String TAG = ForumInterMediatorActivity.class.getSimpleName();

    @BindView(R.id.rvData)
    RecyclerView rvData;
    AdapterForumInter adapterForumInter;
    List<ForumInterModel> forumInterModelList;
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    @BindView(R.id.fbAdd)
    FloatingActionButton fbAdd;
    ApiInterface apiInterface;

    @BindView(R.id.rlSetting)
    RelativeLayout rlSetting;
    @BindView(R.id.rlHome)
    RelativeLayout rlHome;
    @BindView(R.id.rvSearch)
    RelativeLayout rvSearch;
    @BindView(R.id.rlHelpDeskBottom)
    RelativeLayout rlHelpDeskBottom;


    LinearLayoutManager linearLayoutManager;
    boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int page = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum_intermeditor);
        init();
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        rlSetting.setOnClickListener(this);
        rlHome.setOnClickListener(this);
        rvSearch.setOnClickListener(this);
        rlHelpDeskBottom.setOnClickListener(this);
        tvToolbarTitle.setText(getString(R.string.Forum));
        fbAdd.setOnClickListener(this);
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayout.VERTICAL, false);
        rvData.setLayoutManager(linearLayoutManager);


        forumInterModelList = new ArrayList<>();
        adapterForumInter = new AdapterForumInter(this, forumInterModelList);
        adapterForumInter.SetItemClickListener(this);
        rvData.setAdapter(adapterForumInter);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        doSetScrollistner();
        page = 1;
        loadData(page, "1");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");

    }

    private void doSetScrollistner() {
        rvData.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                Log.d(TAG, "onScrolled:   dx == " + dx);
                Log.d(TAG, "onScrolled:   dy == " + dy);

                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            Log.v(TAG, "Last Item Wow !");
                            //Do pagination.. i.e. fetch new data
                            page = page + 1;
                            loadData(page, "0");
                        }
                    }
                }

            }
        });


    }

    private void loadData(int page, String firstTime) {
        //  forumInterModelList.add(new ForumInterModel("Jagat Kumar Gour", "30 min", "Contrary to popular belief, Lorem ipsum is not simply random text. It has root in a pieces of", "There are many variations of passages of Lorem"));
        // forumInterModelList.add(new ForumInterModel("Rupal Shetty ", "20 min", "Lorem ipsum in not simply random text", "Lorem ipsum dolor sir amet, Consectetur adipiscing elit, sed do eiusmod tempor"));
        //  adapterForumInter.notifyDataSetChanged();

        Log.d(TAG, "loadData: ");
       /* if (firstTime.equals("1")) {
            page = 1;
            forumInterModelList.clear();
            loading = true;
        }*/

        //forumInterModelList.clear();

        loading = true;
        String progarmId = SharePref.getProgramModel(this).getId();
        Log.d(TAG, "loadData: ===" + progarmId + " pages===" + page);
        showDialog(this);
        String accept = "application/json";
        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();

        Call<ForumInterResponse> responseCall = apiInterface.doFetchForumInterList(accept, authorization,
                progarmId,
                String.valueOf(page));

        Log.d(TAG, "loadData:   " + authorization);
        Log.d(TAG, "loadData: " + new Gson().toJson(responseCall.request()));

        responseCall.enqueue(new Callback<ForumInterResponse>() {

            @Override
            public void onResponse(@NonNull Call<ForumInterResponse> call, @NonNull Response<ForumInterResponse> response) {
                hideDialog();

                if (response.code() == 200) {
                    if (response.body().getStatus().equals("true")) {
                        forumInterModelList.addAll(response.body().getForumInterModelList());
                        adapterForumInter.notifyDataSetChanged();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<ForumInterResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;
            case R.id.fbAdd:
                startActivity(new Intent(this, AddForumSubjectActivity.class));
                finish();
                break;

            case R.id.rlSetting:
                //Todo load my query fragment
                startActivity(new Intent(this, SettingActivity.class));
                break;
            case R.id.rlHome:
                // startActivity(new Intent(this, HomeActivity.class));
                finish();
                break;
            case R.id.rvSearch:
                startActivity(new Intent(this, ActivitySearch.class));
                break;
            case R.id.rlHelpDeskBottom:
                startActivity(new Intent(this, ActivityHelpDesk.class));
                break;

        }


    }

    @Override
    public void setOnItemClickListener(int position) {
        Intent intent = new Intent(this, ForumActivity.class);
       // Intent intent = new Intent(this, ForumActivityTest.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(ForumActivity.Key, forumInterModelList.get(position));
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }
}
