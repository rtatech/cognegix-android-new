package com.cognegix.cognegixapp.facilitor.adpter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.model.badges.ProfileBadgeModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AdapterBadgesPager extends PagerAdapter {

    private Context context;
    private List<ProfileBadgeModel> badgeModelList;


    public AdapterBadgesPager(Context context, List<ProfileBadgeModel> badgeModelList) {
        this.context = context;
        this.badgeModelList = badgeModelList;
    }


    @Override
    public int getCount() {
        if (badgeModelList.size() > 0) {
            return 2;
        }
        return 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ViewHolder viewHolder;
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_profile_badges, container, false);
        viewHolder = new ViewHolder(view);
        ((ViewPager) container).addView(view);

        viewHolder.tvTitle.setText(badgeModelList.get(position).getTitle());
        viewHolder.tvProfileCount.setText(badgeModelList.get(position).getCount());
        viewHolder.ivBadges.setImageResource(badgeModelList.get(position).getImagePath());


        viewHolder.tvTitle2.setText(badgeModelList.get(1).getTitle());
        viewHolder.tvProfileCount2.setText(badgeModelList.get(1).getCount());
        viewHolder.ivBadges2.setImageResource(badgeModelList.get(1).getImagePath());

        viewHolder.tvTitle3.setText(badgeModelList.get(2).getTitle());
        viewHolder.tvProfileCount3.setText(badgeModelList.get(2).getCount());
        viewHolder.ivBadges3.setImageResource(badgeModelList.get(2).getImagePath());


        if (position > 0) {


            viewHolder.tvTitle.setText(badgeModelList.get(3).getTitle());
            viewHolder.tvProfileCount.setText(badgeModelList.get(3).getCount());
            viewHolder.ivBadges.setImageResource(badgeModelList.get(3).getImagePath());


            viewHolder.tvTitle2.setText(badgeModelList.get(4).getTitle());
            viewHolder.tvProfileCount2.setText(badgeModelList.get(4).getCount());
            viewHolder.ivBadges2.setImageResource(badgeModelList.get(4).getImagePath());

            viewHolder.tvTitle3.setVisibility(View.GONE);
            viewHolder.tvProfileCount3.setVisibility(View.GONE);
            viewHolder.ivBadges3.setVisibility(View.GONE);


        }

        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }


    class ViewHolder {


        @BindView(R.id.tvTitle)
        CustomTextView tvTitle;
        @BindView(R.id.tvProfileCount)
        CustomTextView tvProfileCount;
        @BindView(R.id.ivBadges)
        ImageView ivBadges;


        @BindView(R.id.tvTitle2)
        CustomTextView tvTitle2;
        @BindView(R.id.tvProfileCount2)
        CustomTextView tvProfileCount2;
        @BindView(R.id.ivBadges2)
        ImageView ivBadges2;


        @BindView(R.id.tvTitle3)
        CustomTextView tvTitle3;
        @BindView(R.id.tvProfileCount3)
        CustomTextView tvProfileCount3;
        @BindView(R.id.ivBadges3)
        ImageView ivBadges3;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);

        }


    }


}
