package com.cognegix.cognegixapp.facilitor.adpter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.interfaces.ItemOnClickListener;
import com.cognegix.cognegixapp.model.program.ProgramModel;

import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterPastProgram extends RecyclerView.Adapter<AdapterPastProgram.ViewHolder> {
    private Context context;
    private ItemOnClickListener itemOnClickListener;
    private List<ProgramModel> programModels;


    public AdapterPastProgram(Context context, List<ProgramModel> programModels) {
        this.context = context;
        this.programModels = programModels;
    }

    public void setOnClickButtonShare(ItemOnClickListener itemOnClickListener) {
        this.itemOnClickListener = itemOnClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_past_program, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.tvTitle.setText(programModels.get(position).getName());
        String date = "Completion date : " + formatedDate(programModels.get(position).getEnd_date());
        holder.tvDate.setText(date);
        holder.bViewDetails.setTag(position);


    }

    @Override
    public int getItemCount() {
        return programModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvBadgeTitle)
        CustomTextView tvTitle;
        @BindView(R.id.tvDate)
        CustomTextView tvDate;


        @BindView(R.id.bViewDetails)
        CustomButtonView bViewDetails;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            bViewDetails.setText(context.getString(R.string.view));

            bViewDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int) view.getTag();
                    itemOnClickListener.setOnItemClickListener(pos);
                }
            });
        }
    }


    @SuppressLint("SimpleDateFormat")
    private String formatedDate(String date) {

        // 10th March 2018
        String forDate = null;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            java.util.Date date1 = formatter.parse(date);
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");

            forDate = format.format(date1);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return forDate;
    }
}
