package com.cognegix.cognegixapp.facilitor.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.cognegix.cognegixapp.LoginActivity;
import com.cognegix.cognegixapp.R;
import com.cognegix.cognegixapp.apihelper.ApiClient;
import com.cognegix.cognegixapp.apihelper.ApiInterface;
import com.cognegix.cognegixapp.customview.CustomButtonView;
import com.cognegix.cognegixapp.customview.CustomEditTextView;
import com.cognegix.cognegixapp.customview.CustomTextView;
import com.cognegix.cognegixapp.model.ForgotResponse;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HelpDeskMessageActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = HelpDeskMessageActivity.class.getSimpleName();

    @BindView(R.id.rlNotification)
    RelativeLayout rlNotification;
    @BindView(R.id.ivUserProfile)
    CircleImageView ivUserProfile;
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.tvToolbarTitle)
    CustomTextView tvToolbarTitle;
    @BindView(R.id.bSubmit)
    CustomButtonView bSubmit;

    @BindView(R.id.etFirstName)
    CustomEditTextView etFirstName;
    @BindView(R.id.etLastName)
    CustomEditTextView etLastName;
    @BindView(R.id.etEmail)
    CustomEditTextView etEmail;
    @BindView(R.id.etOrganization)
    CustomEditTextView etOrganization;
    @BindView(R.id.etMessage)
    CustomEditTextView etMessage;

    String help_type;
    String firstName;
    String lastName;
    String email;
    String organization;
    String message;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messsage_helpdesk);

        init();
    }

    //Todo init all ui
    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this);
        ibBack.setOnClickListener(this);
        bSubmit.setOnClickListener(this);
        tvToolbarTitle.setText(getString(R.string.HelpDesk));
        ivUserProfile.setVisibility(View.GONE);
        rlNotification.setVisibility(View.GONE);
        help_type = getIntent().getStringExtra("help_type");
        Log.d(TAG, "init: " + help_type);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
    }


    //Todo handle all click event
    @Override
    public void onClick(View view) {


        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;
            case R.id.bSubmit:
                if (isValidate()) {
                    doDataSendServer();
                }
                break;
        }


    }

    //todo call send api
    private void doDataSendServer() {
        showDialog(this);

        // String accept = "application/json";
//        String authorization = "Bearer " + SharePref.getLoginResponse(this).getUserDetail().getApi_token();

        // Call<ForgotResponse> responseCall = apiInterface.doSendDataAsGeast(accept, authorization, help_type, message, firstName, lastName, organization, email);
        Call<ForgotResponse> responseCall = apiInterface.doSendDataAsGeast(help_type, message, firstName, lastName, organization, email);


        responseCall.enqueue(new Callback<ForgotResponse>() {

            @Override
            public void onResponse(@NonNull Call<ForgotResponse> call, @NonNull Response<ForgotResponse> response) {
                hideDialog();

                if (response.code() == 200) {
                    assert response.body() != null;
                    if (response.body().getStatus().equals("true")) {
                        showAlertDialog(HelpDeskMessageActivity.this, "Login", response.body().getMessage());
                        etFirstName.setText("");
                        etLastName.setText("");
                        etEmail.setText("");
                        etOrganization.setText("");
                        etMessage.setText("");
                    } else {
                        showAlertDialog(HelpDeskMessageActivity.this, "Login", response.body().getMessage());
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<ForgotResponse> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t);
                hideDialog();
            }
        });


    }


    //Todo Handle back press
    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


    //todo for store variable for api
    void initVariable() {
        firstName = etFirstName.getText().toString().trim();
        lastName = etLastName.getText().toString().trim();
        email = etEmail.getText().toString().trim();
        organization = etOrganization.getText().toString().trim();
        message = etMessage.getText().toString().trim();

    }

    //todo check validation for APi
    boolean isValidate() {
        boolean isValidate = true;
        initVariable();

        if (firstName.equals("")) {
            etFirstName.setError("Enter First Name");
            isValidate = false;
        }

        if (lastName.equals("")) {
            etLastName.setError("Enter Last Name");
            isValidate = false;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.setError("Enter Valid Email Address");
            isValidate = false;
        }
        if (organization.equals("")) {
            etOrganization.setError("Enter Organization");
            isValidate = false;
        }
        if (message.equals("")) {
            etMessage.setError("Enter Your Message");
            isValidate = false;
        }

        return isValidate;
    }

}
