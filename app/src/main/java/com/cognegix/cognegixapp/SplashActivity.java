package com.cognegix.cognegixapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.cognegix.cognegixapp.facilitor.activity.HomeActivity;
import com.cognegix.cognegixapp.utils.BaseActivity;
import com.cognegix.cognegixapp.utils.SharePref;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends BaseActivity {
    private static final String TAG = SplashActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        inti();
    }

    private void inti() {
        Log.d(TAG, "inti: ");


        if (checkConnectionIsConnected()) {

         //   loadUI();
        }


    }

    private void loadUI() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if (SharePref.getLoginResponse(getApplicationContext()) != null) {
                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                }
            }
        }, 3000);


    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        super.onNetworkConnectionChanged(isConnected);
        if (isConnected) {
            loadUI();
        }

    }
}
